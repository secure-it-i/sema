package androidx.naigation.checks;

import androidx.naigation.checks.dao.FunArgs;
import androidx.naigation.checks.dao.FunDao;
import androidx.naigation.checks.dao.NavActivityDao;
import androidx.naigation.checks.dao.NavArgumentDao;
import androidx.naigation.checks.dao.NavWidgetDao;
import androidx.naigation.checks.util.Constants;
import androidx.naigation.checks.util.UsefulMethods;

import com.android.SdkConstants;
import com.android.resources.ResourceFolderType;
import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Context;
import com.android.tools.lint.detector.api.Implementation;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.ResourceXmlDetector;
import com.android.tools.lint.detector.api.Scope;
import com.android.tools.lint.detector.api.Severity;
import com.android.tools.lint.detector.api.XmlContext;
import com.android.utils.Pair;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class NavSecurityDetector extends ResourceXmlDetector {

    private Map<String,String> syn_map = null;
    private List<Pair<String,String>> influences;
    private List<Pair<String,String>> influences_tran_closure;
    private List<String> exported;
    private Map<String, List<String>> scope = null;
    private Map<String, String> graph_startDest_map = null;
    private Map<String, String> graph_layout_map = null;
    private Map<String, String> activity_layout_map = null;
    private Map<String, String> typingContext = null;
    private List<String> malicious = null;
    private List<FunDao> expOfIntrst = null;
    private List<NavWidgetDao> widgetsOfIntrst = null;
    private List<NavArgumentDao> argsOfIntrst = null;
    private List<NavActivityDao> externalActivities = null;
    private List<FunDao> potentialLeakSites = null;

    public static final Issue DATA_INJECTION_SCREEN = Issue.create("DataInjectionScreen",
            "Detects malicious arguments",
            "Detects arguments accepted by exported activities that can be used to influence" +
                    "critical resources accessed or owned by the app",
            Category.LINT, 10, Severity.ERROR,
            new Implementation(NavSecurityDetector.class, Scope.RESOURCE_FILE_SCOPE));

    public static final Issue SENSITIVE_INFO_TYPE_CHECK = Issue.create("DeclassificationError",
            "Detects inconsistent information flow",
            "Detects instances where data with high sensitivity is downgraded to low sensitivity",
            Category.LINT, 10, Severity.ERROR,
            new Implementation(NavSecurityDetector.class, Scope.RESOURCE_FILE_SCOPE));

    public static final Issue SENSITIVE_DATA_LEAK = Issue.create("SensitiveDataLeak",
            "Detects leakage of sensitive data",
            "Detects instances where highly sensitive data is leaked",
            Category.LINT, 10, Severity.ERROR,
            new Implementation(NavSecurityDetector.class, Scope.RESOURCE_FILE_SCOPE));

    public static final Issue PRIVILEGE_LEAK = Issue.create("PrivLeak",
            "Detects leakage of privilege",
            "Detects instances which could lead to a privilege escalation attack",
            Category.LINT, 10, Severity.ERROR,
            new Implementation(NavSecurityDetector.class, Scope.RESOURCE_FILE_SCOPE));

    public static final Issue KEYFORGERY = Issue.create("ConstantKey",
            "Detects instances of constant crypto key",
            "Detects if a constant key is being used for cryptograhic operations",
            Category.LINT, 10, Severity.ERROR,
            new Implementation(NavSecurityDetector.class, Scope.RESOURCE_FILE_SCOPE));

    public static final Issue REMOTE_EXECUTION = Issue.create("RemoteExecution",
            "Detects instances malicious code could be remote executed",
            "WebViews that allow the execution of malicious JavaScript are vulnerable to remote code injection attacks",
            Category.LINT, 10, Severity.ERROR,
            new Implementation(NavSecurityDetector.class, Scope.RESOURCE_FILE_SCOPE));
    public static final Issue UNGUARDED_ACTION = Issue.create("UnsafeAction"
            , "Detects actions which are not safe"
            ,"An action is unsafe if it is not guarded by a user controlled action (e.g., button click)"
            ,Category.LINT, 10, Severity.ERROR
            ,new Implementation(NavSecurityDetector.class, Scope.RESOURCE_FILE_SCOPE));


    public NavSecurityDetector() {

    }

    @Override
    public boolean appliesTo(ResourceFolderType folderType) {
        return (folderType == ResourceFolderType.NAVIGATION || folderType == ResourceFolderType.LAYOUT || folderType == ResourceFolderType.XML);
    }

    @Override
    public Collection<String> getApplicableElements() {
        return Arrays.asList(Constants.TAG_EVENT_RECVR, SdkConstants.TAG_ACTIVITY, Constants.TAG_DEEPLINK, SdkConstants.TAG_NAVIGATION,Constants.TAG_ARGUMENT, Constants.TAG_WIDGET, Constants.TAG_FRAGMENT, Constants.TAG_FUN,SdkConstants.TAG_ACTION);
    }

    @Override
    public Collection<String> getApplicableAttributes() {
        return Collections.singleton(SdkConstants.ATTR_NAV_GRAPH);
    }

    @Override
    public void afterCheckRootProject(Context context) {
        if(exported != null) {
            for (String screenId : exported) {
                if (malicious == null) malicious = new ArrayList<>();
                if (scope != null && scope.get(screenId) != null)
                    malicious.addAll(scope.get(screenId));
                if (hasNestedSynonym(screenId))
                    malicious.addAll(scope.get(syn_map.get(screenId)));
                if (hasActivitySynonym(screenId))
                    malicious
                            .addAll(scope
                                    .get(activity_layout_map
                                            .get(graph_layout_map
                                                    .get(graph_startDest_map
                                                            .get(screenId)))));
            }
        }
        if(influences != null) {
            HashMap<String,List<String>> env = new HashMap<>();
            for(Pair<String,String> pair : influences) {
                String key = pair.getFirst();
                if(env.get(key) == null) {
                    List<String> v = new ArrayList<>();
                    v.add(pair.getSecond());
                    env.put(key,v);
                }
                else {
                    List<String> v = env.get(key);
                    v.add(pair.getSecond());
                    env.put(key,v);
                }
            }

            for(Pair<String,String> pair : influences) {
                String first = pair.getFirst();
                String second = pair.getSecond();
                if(influences_tran_closure == null) influences_tran_closure = new ArrayList<>();
                influences_tran_closure.add(Pair.of(first,second));
                List<String> reachableFromSecond = getReachableVars(second,env);
                for (String s : reachableFromSecond) {
                    if(!contains(influences_tran_closure,Pair.of(first,s)))
                        influences_tran_closure.add(Pair.of(first,s));
                }

            }
        }
        if(expOfIntrst != null) {
            for (FunDao e : expOfIntrst) {
                String funName = e.getName();
                List<FunArgs> funArgs = e.getFunArgs();
                if(funArgs != null) {
                    if(e.getResourcMethod() != null
                            && (e.getResourcMethod().equals(Constants.NAV_ATTR_METHOD_LOADURL)
                                || Constants.NAV_ATTR_METHOD_LOADDATA.equals(e.getResourcMethod()))
                            && e.getAllowJS() != null && e.getAllowJS().equals(Constants.TRUE)
                            && !isJSExecSafe(e)) {
                        context.report(REMOTE_EXECUTION,e.getLocation(),"Potentially malicious URL : " + e.getFunArgs().get(0).getArgName() + " might contain malicious JS scripts. Consider providing a whitelst of trusted urls");
                        if(e.getAllowContentAccess() != null && e.getAllowContentAccess().equals(Constants.TRUE))
                            context.report(REMOTE_EXECUTION,e.getLocation(),"malicious JS should not have access to content providers. Consider disabling content access or provide a whitelist of trusted urls");
                        if(e.getAllowFileAccess() != null && e.getAllowFileAccess().equals(Constants.TRUE))
                            context.report(REMOTE_EXECUTION, e.getLocation(), "malicious JS should not have access to files in the device. Consider disabling file access or provide a whitelist of trusted urls");
                        if(e.getAcceptCookie() != null && e.getAcceptCookie().equals(Constants.TRUE))
                            context.report(REMOTE_EXECUTION, e.getLocation(), "malicious JS should not have access to cookies. Consider disabling cookies or provide a whitelist of trusted urls");
                    }

                    if(Constants.NAV_RES_NAME_KEYSTORE.equals(e.getResource())
                            && e.getResourcMethod() != null
                            && e.getResourcMethod().equals(Constants.NAV_ATTR_METHOD_POST)
                            && e.getPbe().equals(Constants.TRUE)) {
                        if(funArgs.size() != 2)
                            context.report(KEYFORGERY,e.getLocation(),"Missing Key info. 1st parm should have key info and 2nd parm should have alias info");
                        else {
                            String keyParm = funArgs.get(0).getArgName();
                            if(keyParm.startsWith(Constants.PRIM_PREFIX)
                                    && !keyParm.endsWith(Constants.USER_ENTERED_VAL))
                                context.report(KEYFORGERY, e.getLocation(), "Static Key used. Static keys can be forged and should not be used to perform crypto operations.");
                            else if(isPrimitive(influences_tran_closure, keyParm)) {
                                context.report(KEYFORGERY, e.getLocation(), "Key info depends on static value. Static keys can be forged and should not be used to perform crypto operations.");
                            }
                        }

                    }

                    for(FunArgs arg : funArgs) {
                        if(malicious != null && malicious.contains(arg.getArgName()) && arg.isConsq())
                            context.report(DATA_INJECTION_SCREEN,e.getLocation(),arg.getArgName() + " used in " +
                                    funName + " might be malicious");
                        else {
                            if(malicious != null) {
                                for (String mal : malicious) {
                                    if(contains(influences_tran_closure,Pair.of(mal,arg.getArgName())) && arg.isConsq()) {
                                        context.report(DATA_INJECTION_SCREEN,e.getLocation(),arg.getArgName() + " used in " +
                                                funName + " might be malicious");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /*
            Data Injection check
        */
        if(widgetsOfIntrst != null && malicious != null) {
            for(NavWidgetDao navWidgetDao : widgetsOfIntrst) {
                String wid = navWidgetDao.getWidgetId();
                String value = navWidgetDao.getValue();
                if((malicious.contains(wid) && navWidgetDao.isConsq()) ||
                        (value != null && malicious.contains(value)) && navWidgetDao.isConsq())
                    context.report(DATA_INJECTION_SCREEN,navWidgetDao.getLocation(),"malicious data might be injected into " + wid);
                else {
                    for(String mal : malicious) {
                        if(value != null && contains(influences_tran_closure,Pair.of(mal,value)) && navWidgetDao.isConsq()) {
                            context.report(DATA_INJECTION_SCREEN,
                                    navWidgetDao.getLocation(),
                                    value + " might be malicious");
                        }
                    }
                }
            }
        }

        /*
        Sensitive data leak to leak sites
         */
        if(potentialLeakSites != null) {
            for(FunDao e : potentialLeakSites) {
                boolean funLeaks = isFunLeakSite(e);
                String funName = e.getName();
                System.out.println(funName + ":" + funLeaks);
                List<FunArgs> funArgs = e.getFunArgs();
                if(funArgs != null) {
                    for(FunArgs arg : funArgs) {
                        /*
                        Sensitive Data Leak
                         */
                        if(funLeaks
                                && typingContext.get(arg.getArgName()) != null
                                && typingContext.get(arg.getArgName()).equals(Constants.NAV_SENS_HIGH)
                                && arg.isConsq()) {
                            context.report(SENSITIVE_DATA_LEAK, e.getLocation(), arg.getArgName() + " is sensitive and and might be leaked");
                        }

                    }
                }

                /*
                Safe Pending Intent Check
                 */
                if(!isPendingIntentSafe(e))
                    context.report(PRIVILEGE_LEAK, e.getLocation(), " An unsafe pending intent is used in " + e + " which could leak to privilege escalation");
            }
        }

        /*
        Sensitive data leak and type check due to external activity
         */
        if(externalActivities != null) {
            for(NavActivityDao navActivityDao : externalActivities) {
                if(scope != null && scope.get(navActivityDao.getActivityId()) != null) {
                    for(String var : scope.get(navActivityDao.getActivityId())) {
                        if(typingContext != null
                                && typingContext.get(var) != null
                                && typingContext.get(var).equals(Constants.NAV_SENS_HIGH))
                            context.report(SENSITIVE_DATA_LEAK, navActivityDao.getLocation(), var +
                                    " used in external activity " +
                                    navActivityDao.getActivityId() +
                                    " is highly sensitive." +
                                    "Consider adding a targetPackage");
                    }
                }
            }
        }
    }

    @Override
    public void visitAttribute(XmlContext context, Attr attr) {
        String fileNm = context.file.getName();
        String graph = attr.getValue();
        if(graph.contains("/")) {
            String key = graph.split("/")[1];
            if(graph_layout_map == null) graph_layout_map = new HashMap<>();
            graph_layout_map.put(key, fileNm.split(".xml")[0]);
        }
    }

    @Override
    public void visitElement(XmlContext context, Element element) {
        String name = element.getTagName();
        String elemId = "";
        String value = "";

        if(name.equals(SdkConstants.TAG_ACTIVITY)) {
            String id = element.getAttribute(Constants.ATTR_ID);
            String layout = element.getAttribute(Constants.ATTR_NAV_PARENT_LAYOUT);
            String actName = element.getAttribute(Constants.NAV_ATTR_NAME);
            String actPackage = element.getAttribute(Constants.NAV_ACTIVITY_ATTR_TARGETPACKAGE);

            if(id.contains("/") && layout.contains("/")) {
                if(activity_layout_map == null) activity_layout_map = new HashMap<>();
                activity_layout_map.put(layout.split("/")[1], id.split("/")[1]);
            }
            if((actPackage == null && actName == null)
                    || ("".equals(actPackage) && "".equals(actName))) {
                NodeList activityArgs = element.getElementsByTagName(Constants.TAG_ARGUMENT);
                for(int i = 0; i < activityArgs.getLength(); i++) {
                    Element activityArg = (Element) activityArgs.item(i);
                    String argName = activityArg.getAttribute(Constants.NAV_ATTR_NAME);
                    if(malicious == null) malicious = new ArrayList<>();
                    malicious.add(argName);
                }
                NavActivityDao navActivityDao = null;
                if(id.contains("/"))
                    navActivityDao = new NavActivityDao(id.split("/")[1], context.getLocation(element));
                if(externalActivities == null) externalActivities = new ArrayList<>();
                if(navActivityDao != null)
                    externalActivities.add(navActivityDao);
            }
        }

        else if(name.equals(SdkConstants.TAG_NAVIGATION)) {
            String navId = element.getAttribute(Constants.ATTR_ID);
            String startDest = element.getAttribute(Constants.NAV_ATTR_START_DEST);
            String accessPol = element.getAttribute(Constants.NAV_ATTR_ACCESSPOLICY);
            String fileNm = context.file.getName();

            if(syn_map == null) syn_map = new HashMap<>();
            if(startDest.contains("/") && navId.contains("/")){
                syn_map.put(startDest.split("/")[1],navId.split("/")[1]);
                if(graph_startDest_map == null) graph_startDest_map = new HashMap<>();
                else graph_startDest_map.put(startDest.split("/")[1],fileNm.split(".xml")[0]);
            }
            else
                throw new IndexOutOfBoundsException("startDestination or navigation ID is missing.");

            if(accessPol.equals(Constants.ALL)) {
                if(startDest.contains("/")) {
                    if(exported == null) exported = new ArrayList<>();
                    exported.add(startDest.split("/")[1]);
                }
            }
        }
        else if(name.equals(Constants.TAG_DEEPLINK)) {
            String uri = element.getAttribute(Constants.NAV_ATTR_DEEPLINK_URI);
             if(uri.contains("{") && uri.contains("}")) {
                String uriArg = uri.substring(uri.indexOf("{")+1,uri.lastIndexOf("}"));
                if(malicious == null) malicious = new ArrayList<>();
                malicious.add(uriArg);
                updateTypingContext(uriArg, Constants.NAV_SENS_HIGH);
            }

//            Element parent = (Element) element.getParentNode();
//            String parentId = parent.getAttribute(Constants.ATTR_ID);
//            if(exported == null) exported = new ArrayList<>();
//            if(parentId.contains("/"))
//                exported.add(parentId.split("/")[1]);
        }
        else if(name.equals(Constants.TAG_FUN)) {
            String funName = element.getAttribute(Constants.NAV_ATTR_NAME);
            String resource = element.getAttribute(Constants.NAV_ATTR_RESOURCE);
            String local = element.getAttribute(Constants.NAV_ATTR_LOCAL);
            NodeList funParmNodeList = element.getElementsByTagName(Constants.TAG_PARM);
            FunDao funDao = new FunDao(context.getLocation(element),funName,UsefulMethods.getFunArgs(funParmNodeList));
            funDao.setResource(resource);
            funDao.setResourcMethod(element.getAttribute(Constants.NAV_ATTR_METHOD));
            if(resource != null && resource.equals(Constants.NAV_RES_NAME_INTERNET)
                    && !Constants.NAV_ATTR_METHOD_LOADURL.equals(element.getAttribute(Constants.NAV_ATTR_METHOD))) {
                String useCertPin = element.getAttribute(Constants.NAV_ATTR_CERTPINNING);
                if(useCertPin != null && useCertPin.equals(Constants.FALSE))
                    funDao.setUseCertificatePinning(Constants.FALSE);
                else
                    funDao.setUseCertificatePinning(Constants.TRUE);
            }
            if(resource != null && resource.equals(Constants.NAV_RES_NAME_INTERNET)
                    && (Constants.NAV_ATTR_METHOD_LOADURL.equals(element.getAttribute(Constants.NAV_ATTR_METHOD))
                        || Constants.NAV_ATTR_METHOD_LOADDATA.equals(element.getAttribute(Constants.NAV_ATTR_METHOD)))) {
                String allowJS = element.getAttribute(Constants.NAV_ATTR_JSEXEC);
                if(Constants.TRUE.equals(allowJS))
                    funDao.setAllowJS(Constants.TRUE);
                else
                    funDao.setAllowJS(Constants.FALSE);
                String contentAccess = element.getAttribute(Constants.NAV_ATTR_CONTENT_ACCESS);
                if(Constants.TRUE.equals(contentAccess))
                    funDao.setAllowContentAccess(Constants.TRUE);
                else
                    funDao.setAllowContentAccess(Constants.FALSE);
                String fileAccess = element.getAttribute(Constants.NAV_ATTR_FILE_ACCESS);
                if(Constants.TRUE.equals(fileAccess))
                    funDao.setAllowFileAccess(Constants.TRUE);
                else
                    funDao.setAllowFileAccess(Constants.FALSE);
                String acceptCookie = element.getAttribute(Constants.NAV_ATTR_ACCEPT_COOKIE);
                if(Constants.TRUE.equals(acceptCookie))
                    funDao.setAcceptCookie(Constants.TRUE);
                else
                    funDao.setAcceptCookie(Constants.FALSE);
                if(element.getElementsByTagName(Constants.TAG_WHITELIST).getLength() == 1
                        && !isLoadURLWhiteListEmpty((Element) element.getElementsByTagName(Constants.TAG_WHITELIST).item(0))) {
                    funDao.setWhitelistExists(true);
                }
                if(element.getElementsByTagName(Constants.TAG_WHITELIST).getLength() == 1
                        && isLoadURLIntentUnsafe((Element) element.getElementsByTagName(Constants.TAG_WHITELIST).item(0)))
                    context.report(DATA_INJECTION_SCREEN, context.getLocation(element), "Accepting unsafe intents leads to intent hijacking. Consider providing a whitelist of safe intents.");
            }
            if(resource != null
                    && resource.equals(Constants.NAV_RES_NAME_CIPHER)
                    && (element.getAttribute(Constants.NAV_ATTR_METHOD).equals(Constants.NAV_ATTR_ENCRYPT)
                        || element.getAttribute(Constants.NAV_ATTR_METHOD).equals(Constants.NAV_ATTR_DECRYPT))) {
                Element firstParm = (Element) element.getElementsByTagName(Constants.TAG_PARM).item(0);
                NodeList nodes = firstParm.getElementsByTagName(Constants.TAG_FUN);
                if(!(nodes.getLength() == 1
                        && firstParm
                            .getAttribute(Constants.NAV_ATTR_PARM_ARG)
                            .startsWith(Constants.NAV_PREFIX_FUN)
                        && ((Element) nodes.item(0))
                                .getAttribute(Constants.NAV_ATTR_RESOURCE)
                                .equals(Constants.NAV_RES_NAME_KEYSTORE))) {
                    context.report(KEYFORGERY,context.getLocation(element),"Unsafe Cipher Operation. Consider using key from a key store");
                }
            }
            if(resource != null
                    && resource.equals(Constants.NAV_RES_NAME_KEYSTORE)
                    && element.getAttribute(Constants.NAV_ATTR_METHOD).equals(Constants.NAV_ATTR_METHOD_POST)) {
                if(element.getAttribute(Constants.NAV_ATTR_PBE).equals(Constants.TRUE))
                    funDao.setPbe(Constants.TRUE);
                else
                    funDao.setPbe(Constants.FALSE);
            }
            if(local.equals(Constants.TRUE))
                funDao.setLocal(true);
            else
                funDao.setLocal(false);
            if(expOfIntrst == null) expOfIntrst = new ArrayList<>();
            expOfIntrst.add(funDao);

            for(int i = 0; i < funParmNodeList.getLength(); i++) {
                Element elem = (Element) funParmNodeList.item(i);
                String consq = elem.getAttribute(Constants.NAV_ATTR_CONSQ);
                if(canInjectDataFun(elem.getElementsByTagName(Constants.TAG_FUN))
                        && !consq.equals("false")) {
                    context.report(DATA_INJECTION_SCREEN,context.getLocation(element),"parameter used in " +
                            funName + " might be malicious");
                }
            }

            if(canInjectDataConstraint(element)) {
                context.report(DATA_INJECTION_SCREEN,context.getLocation(element),
                        funName + " might be malicious");
            }
            //save type info
            updateTypingContext(funName, Constants.NAV_SENS_HIGH);

            /*
            Collect potential leak sites
             */
            List<FunArgs> funArgs = new ArrayList<>();
            for(int i = 0; i < funParmNodeList.getLength(); i++) {
                Element parmElem = (Element) funParmNodeList.item(i);
                String parentFunName = ((Element) parmElem.getParentNode()).getAttribute(Constants.NAV_ATTR_NAME);
                if(parentFunName != null && parentFunName.equals(funName)) {
                    FunArgs funArgs1 = new FunArgs();
                    if(UsefulMethods.isPrefix(parmElem.getAttribute(Constants.NAV_ATTR_PARM_ARG)))
                        funArgs1.setArgName(parmElem.getAttribute(Constants.NAV_ATTR_PARM_ARG).split("/")[1]);
                    else {
                        funArgs1.setArgName(Constants.PRIM_PREFIX + parmElem.getAttribute(Constants.NAV_ATTR_PARM_ARG));
                    }
                    if(parmElem.getAttribute(Constants.NAV_ATTR_CONSQ).equals(Constants.FALSE))
                        funArgs1.setConsq(false);
                    funArgs.add(funArgs1);
                }
            }
            FunDao potentialLeakFun = new FunDao(context.getLocation(element), funName, funArgs);
            potentialLeakFun.setResource(element.getAttribute(Constants.NAV_ATTR_RESOURCE));
            potentialLeakFun.setResourcMethod(element.getAttribute(Constants.NAV_ATTR_METHOD));
            if(resource != null && resource.equals(Constants.NAV_RES_NAME_INTERNET)
                    && !Constants.NAV_ATTR_METHOD_LOADURL.equals(element.getAttribute(Constants.NAV_ATTR_METHOD))) {
                String useCertPin = element.getAttribute(Constants.NAV_ATTR_CERTPINNING);
                if(useCertPin != null && useCertPin.equals(Constants.FALSE))
                    potentialLeakFun.setUseCertificatePinning(Constants.FALSE);
                else
                    potentialLeakFun.setUseCertificatePinning(Constants.TRUE);
            }
            if(resource != null && resource.equals(Constants.NAV_RES_NAME_INTERNET)
                    && Constants.NAV_ATTR_METHOD_LOADURL.equals(element.getAttribute(Constants.NAV_ATTR_METHOD))) {
                String allowJS = element.getAttribute(Constants.NAV_ATTR_JSEXEC);
                if(Constants.TRUE.equals(allowJS))
                    potentialLeakFun.setAllowJS(Constants.TRUE);
                else
                    potentialLeakFun.setAllowJS(Constants.FALSE);
                if(element.getElementsByTagName(Constants.TAG_WHITELIST).getLength() == 1
                        && !isLoadURLWhiteListEmpty((Element) element.getElementsByTagName(Constants.TAG_WHITELIST).item(0)) ) {
                    potentialLeakFun.setWhitelistExists(true);
                }
            }
            if(local.equals(Constants.TRUE))
                potentialLeakFun.setLocal(true);
            else
                potentialLeakFun.setLocal(false);
            if(potentialLeakSites == null) potentialLeakSites = new ArrayList<>();
            potentialLeakSites.add(potentialLeakFun);
        }
        else if(name.equals(Constants.TAG_FRAGMENT)) {
            String screenId = element.getAttribute(Constants.ATTR_ID).split("/")[1];
            NodeList parentArguments = element.getElementsByTagName(Constants.TAG_ARGUMENT);
            List<String> parentArgNames = UsefulMethods.getTagValues(parentArguments,Constants.NAV_ATTR_NAME,
                    Collections.singletonList(SdkConstants.TAG_ACTION));
            if(parentArgNames != null) {
                updateScope(screenId,parentArgNames);
            }
        }
        else if(name.equals(SdkConstants.TAG_ACTION)) {
            NodeList actionArgumentTags = element.getElementsByTagName(Constants.TAG_ARGUMENT);
            List<String> actionArgumentNames = UsefulMethods.getTagValues(actionArgumentTags,Constants.NAV_ATTR_NAME,null);
            if(actionArgumentNames != null && element.getAttribute(Constants.NAV_ACTION_DEST).contains("/")) {
                String key = element.getAttribute(Constants.NAV_ACTION_DEST).split("/")[1];
                updateScope(key,actionArgumentNames);
            }
            if("".equals(element.getAttribute(Constants.ATTR_NAV_ACTION_WIDGET))
                    && !(Constants.FALSE.equals(element.getAttribute(Constants.ATTR_NAV_ACTION_REQUIRESUSER))))
                context.report(UNGUARDED_ACTION,context.getLocation(element),"An action must be guarded by user constraints.");
        }
        else if(name.equals(Constants.TAG_ARGUMENT)) {
            elemId = element.getAttribute(Constants.NAV_ATTR_NAME);
            String consq = element.getAttribute(Constants.NAV_ATTR_CONSQ);
            if(canInjectDataFun(element.getElementsByTagName(Constants.TAG_FUN))) {
                if(malicious == null) malicious = new ArrayList<>();
                malicious.add(elemId);
            }
            value = element.getAttribute(Constants.NAV_ATTR_VAL_ARG);
            NavArgumentDao navArgumentDao;
            if(UsefulMethods.isPrefix(value))
                navArgumentDao = new NavArgumentDao(context.getLocation(element), elemId, value.split("/")[1]);
            else
                navArgumentDao = new NavArgumentDao(context.getLocation(element), elemId, Constants.PRIM_PREFIX + value);
            if(element.getAttribute(Constants.NAV_ATTR_SENS_TYP) != null
                    && !element.getAttribute(Constants.NAV_ATTR_SENS_TYP).equals(""))
                navArgumentDao.setSensTyp(element.getAttribute(Constants.NAV_ATTR_SENS_TYP));
            if(argsOfIntrst == null) argsOfIntrst = new ArrayList<>();
            argsOfIntrst.add(navArgumentDao);
            if(!consq.equals(Constants.FALSE)) {
                if(!updateTypingContext(elemId, Constants.NAV_SENS_HIGH))
                    context.report(SENSITIVE_INFO_TYPE_CHECK, context.getLocation(element), elemId + " cannot be declassified.");
            }
            else {
                if(!updateTypingContext(elemId, Constants.NAV_SENS_LOW))
                    context.report(SENSITIVE_INFO_TYPE_CHECK, context.getLocation(element), elemId + " cannot be declassified.");
            }

        }
        else if(name.equals(Constants.TAG_WIDGET)
                && element.getAttribute(Constants.NAV_ATTR_WID).startsWith(SdkConstants.ID_PREFIX)) {
            elemId = element.getAttribute(Constants.NAV_ATTR_WID).split("/")[1];
            if(canInjectDataFun(element.getElementsByTagName(Constants.TAG_FUN))) {
                if(malicious == null) malicious = new ArrayList<>();
                malicious.add(elemId);
            }
            value = element.getAttribute(Constants.NAV_ATTR_WIDGET_VALUE);
            if(value.isEmpty()) value = Constants.USER_ENTERED_VAL;
            NavWidgetDao navWidgetDao = new NavWidgetDao(context.getLocation(element),elemId);
            if(value.startsWith(Constants.NAV_PREFIX_VAR)
                    || value.startsWith(SdkConstants.ID_PREFIX)
                    || value.startsWith(Constants.NAV_PREFIX_FUN))
                navWidgetDao.setValue(value.split("/")[1]);
            if(element.getAttribute(Constants.NAV_ATTR_CONSQ).equals(Constants.FALSE))
                navWidgetDao.setConsq(false);
            if(element.getAttribute(Constants.NAV_ATTR_SENS_TYP) != null
                    && !element.getAttribute(Constants.NAV_ATTR_SENS_TYP).equals(""))
                navWidgetDao.setSensTyp(element.getAttribute(Constants.NAV_ATTR_SENS_TYP));
            if(widgetsOfIntrst == null) widgetsOfIntrst = new ArrayList<>();
            widgetsOfIntrst.add(navWidgetDao);
            updateTypingContext(elemId, Constants.NAV_SENS_HIGH);
        }
        else if(name.equals(Constants.TAG_EVENT_RECVR)) {
            String access = element.getAttribute(Constants.NAV_ATTR_ACCESS);
            String permNm = element.getAttribute(Constants.NAV_ATTR_PERMNM);
//            String perm = element.getAttribute(Constants.NAV_ATTR_PERM);
//            if(Constants.NAV_ACCESS_USER.equals(access) && perm.isEmpty()){
//                context.report(PRIVILEGE_LEAK, context.getLocation(element), "Required attribute permission missing. " +
//                        "Protect the receiver from unauthorized access");
//            }
            NodeList events = element.getElementsByTagName(Constants.TAG_EVENT);
            for(int i = 0; i < events.getLength(); i++) {
                Element event = (Element) events.item(i);
                String eventName = event.getAttribute(Constants.NAV_ATTR_NAME);
                String eventaccess = event.getAttribute(Constants.NAV_ATTR_ACCESS);
                String eventConsq = event.getAttribute(Constants.NAV_ATTR_CONSQ);
//                NodeList actions = event.getElementsByTagName(SdkConstants.TAG_ACTION);
//                NodeList funs = event.getElementsByTagName(Constants.TAG_FUN);
                if(Constants.NAV_ACCESS_ALL.equals(access)
                        && !isSysEvent(eventName)
                        && !Constants.NAV_ACCESS_USER.equals(eventaccess)
                        && !Constants.NAV_ACCESS_OWN.equals(eventaccess)
                        && !eventConsq.equals(Constants.FALSE)){
                    context.report(PRIVILEGE_LEAK, context.getLocation(element), "Receiver is not protected and responds to malicious events which can lead to privilege escalation. " +
                            "Consider protecting the receiver from unauthorized access or respond only to protected events");
                    break;
                }
                if((Constants.NAV_ACCESS_USER.equals(access) || Constants.NAV_ACCESS_OWN.equals(access))
                        && permNm.equals("")) {
                    context.report(PRIVILEGE_LEAK, context.getLocation(element), "Missing required attribute permission-name");
                    break;
                }

            }
        }

        /*
        building influences relation
         */
        if(!elemId.isEmpty()) {
            if(value.startsWith(Constants.NAV_PREFIX_VAR) || value.startsWith(SdkConstants.ID_PREFIX)) {
                String var = value.split("/")[1];
                if(influences == null) influences = new ArrayList<>();
                influences.add(Pair.of(var,elemId));
            }
            else if(value.startsWith(Constants.NAV_PREFIX_FUN)) {
                NodeList parmNodes = element.getElementsByTagName(Constants.TAG_PARM);
                List<FunArgs> parmVals = UsefulMethods.getFunArgs(parmNodes);
                if(parmVals != null) {
                    for(FunArgs parmVal : parmVals) {
                        if(influences == null) influences = new ArrayList<>();
                        if(parmVal.isConsq()) influences.add(Pair.of(parmVal.getArgName(),elemId));
                    }
                }
            }
            else {
                if(influences == null) influences = new ArrayList<>();
                influences.add(Pair.of(Constants.PRIM_PREFIX + value, elemId));
            }
        }
    }

    private void updateScope(String key, List<String> value) {
        if(scope == null) scope = new HashMap<>();
        if(scope.get(key) == null) scope.put(key,value);
        else {
            List<String> l = scope.get(key);
            l.addAll(value);
            scope.put(key,l);
        }
    }

    private List<String> getReachableVars(String v, HashMap<String,List<String>> env) {
        List<String> result = new ArrayList<>();
        try {
            if (env.get(v) != null) {
                List<String> tmp = env.get(v);
                for (String next : tmp) {
                    if (!result.contains(next)) {
                        result.add(next);
                        List<String> res = getReachableVars(next, env);
                        if(!res.isEmpty()) result.addAll(res);
                    }
                }
                return result;
            }
            else
                return result;
        }
        catch(NullPointerException e) {
            System.out.println(result);
            throw e;
        }
        catch(StackOverflowError e) {
            System.out.println("Argument name " + v + " is causing a cycle");
            throw e;
        }
    }

    private Boolean contains(List<Pair<String,String>> pairs, Pair<String,String> pair) {
        if(pairs != null && pair != null) {
            String first = pair.getFirst();
            String second = pair.getSecond();
            for (Pair<String,String> p : pairs) {
                if(p.getFirst().equals(first) && p.getSecond().equals(second))
                    return true;
            }
        }
        return false;
    }

    private Boolean isPrimitive(List<Pair<String,String>> pairs, String value) {
        for(Pair<String,String> p : pairs) {
            if(value.equals(p.getSecond())
                    && p.getFirst().startsWith(Constants.PRIM_PREFIX)
                    && !p.getFirst().endsWith(Constants.USER_ENTERED_VAL)) {
                return true;
            }
        }
        return false;
    }

    private Boolean hasNestedSynonym(String srcId) {
        return(syn_map != null && syn_map.get(srcId) != null
                && scope.get(syn_map.get(srcId)) != null);
    }

    private Boolean hasActivitySynonym(String srcId) {
        return(graph_startDest_map != null &&
                graph_layout_map != null &&
                activity_layout_map != null &&
                graph_startDest_map.get(srcId) != null &&
                graph_layout_map.get(graph_startDest_map.get(srcId)) != null &&
                activity_layout_map.get(graph_layout_map.get(graph_startDest_map.get(srcId))) != null);
    }

    private Boolean canInjectDataFun(NodeList funNodes) {
        if(funNodes != null && funNodes.getLength() > 0) {
            Element funNode = (Element) funNodes.item(0);
            NodeList funNodeParms = funNode.getElementsByTagName(Constants.TAG_PARM);
            String protocol = "";
            if(funNodeParms.getLength() > 0)
                protocol = ((Element) funNodeParms.item(0)).getAttribute(Constants.NAV_ATTR_PARM_ARG);
            if(funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_INTERNET)
                    && protocol.equals(Constants.NAV_URI_HTTP)) {
                return true;
            }
            else if(funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_INTERNET)
                    && !funNode.getAttribute(Constants.NAV_ATTR_METHOD).equals(Constants.NAV_ATTR_METHOD_LOADURL)
                    && funNode.getAttribute(Constants.NAV_ATTR_CERTPINNING).equals(Constants.FALSE)) {
                return true;
            }
            else if(funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_INTERNET)
                    && funNode.getAttribute(Constants.NAV_ATTR_METHOD).equals(Constants.NAV_ATTR_METHOD_GET)
                    && protocol.equals(Constants.NAV_URI_HTTP))
                return true;
            else if(funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_EXTSTORE)
                    && funNode.getAttribute(Constants.NAV_ATTR_METHOD).equals(Constants.NAV_ATTR_METHOD_GET))
                return true;
            else if (funNode.getAttribute(Constants.NAV_ATTR_RESOURCE) != null
                        && funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_ICC))
                return true;
            else if (funNode.getAttribute(Constants.NAV_ATTR_RESOURCE) != null
                    && funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_SOCKET))
                return true;
            else
                return (funNode.getAttribute(Constants.NAV_ATTR_RESOURCE) != null
                        && funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_CLIPBORAD)
                        && funNode.getAttribute(Constants.NAV_ATTR_METHOD).equals(Constants.NAV_ATTR_METHOD_GET));
        }
        return false;
    }

    private boolean canInjectDataConstraint(Element funNode){
        Element parentElem = (Element) funNode.getParentNode();
        String parentName = parentElem.getTagName();
        NodeList funNodeParms = funNode.getElementsByTagName(Constants.TAG_PARM);
        String protocol = "";
        if(funNodeParms.getLength() > 0)
            protocol = ((Element) funNodeParms.item(0)).getAttribute(Constants.NAV_ATTR_PARM_ARG);
        if(parentName.equals(Constants.TAG_CONSTRAINT) || parentName.equals(Constants.TAG_AND)
                || parentName.equals(Constants.TAG_OR) || parentName.equals(Constants.TAG_NOT)) {
            if(funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_INTERNET)
                    && protocol.equals(Constants.NAV_URI_HTTP)) {
                return true;
            }
            else if(funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_INTERNET)
                    && !funNode.getAttribute(Constants.NAV_ATTR_METHOD).equals(Constants.NAV_ATTR_METHOD_LOADURL)
                    && funNode.getAttribute(Constants.NAV_ATTR_CERTPINNING).equals(Constants.FALSE)) {
                return true;
            }
            else if(funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_EXTSTORE)
                    && funNode.getAttribute(Constants.NAV_ATTR_METHOD).equals(Constants.NAV_ATTR_METHOD_GET))
                return true;
            else if (funNode.getAttribute(Constants.NAV_ATTR_RESOURCE) != null
                        && funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_ICC)
                        && !Constants.NAV_ATTR_METHOD_SENDBROADCAST.equals(funNode.getAttribute(Constants.NAV_ATTR_METHOD))
                        && !Constants.NAV_ATTR_METHOD_SENDORDRBROADCAST.equals(funNode.getAttribute(Constants.NAV_ATTR_METHOD)))
                return true;
            else
                return (funNode.getAttribute(Constants.NAV_ATTR_RESOURCE) != null
                        && funNode.getAttribute(Constants.NAV_ATTR_RESOURCE).equals(Constants.NAV_RES_NAME_SOCKET));
        }
        return false;
    }

    private boolean isFunLeakSite(FunDao funDao) {
        String resource = funDao.getResource();
        String resourcMethod = funDao.getResourcMethod();
        if (resource != null && resource.equals(Constants.NAV_RES_NAME_INTERNET)
                && funDao.getFunArgs()
                    .stream()
                    .filter(arg -> arg.getArgName().endsWith(Constants.NAV_URI_HTTP))
                    .count() == 1)
            return true;
        else if(resource != null && resource.equals(Constants.NAV_RES_NAME_INTERNET)
                && !Constants.NAV_ATTR_METHOD_LOADURL.equals(resourcMethod)
                && funDao.getUseCertificatePinning().equals(Constants.FALSE))
            return true;
        else if (resource != null && resource.equals(Constants.NAV_RES_NAME_ICC) && !funDao.isLocal())
            return true;
        else if (resource != null && resourcMethod != null
                    && resource.equals(Constants.NAV_RES_NAME_EXTSTORE)
                    && resourcMethod.equals(Constants.NAV_ATTR_METHOD_POST))
            return true;
        else if (resource != null
                && resource.equals(Constants.NAV_RES_NAME_CLIPBORAD)
                && Constants.NAV_ATTR_METHOD_POST.equals(resourcMethod))
            return true;
        else
            return (resource != null && resourcMethod != null
                    && resource.equals(Constants.NAV_RES_NAME_SOCKET)
                    && resourcMethod.equals(Constants.NAV_ATTR_METHOD_POST));
    }

    private boolean updateTypingContext(String id, String typVal) {
        if(typingContext != null && typingContext.get(id) != null && !typVal.equals(typingContext.get(id)))
            return false;
        if(typingContext == null)
            typingContext = new HashMap<>();
        typingContext.put(id, typVal);
        return true;
    }

    private boolean isPendingIntentSafe(FunDao fundao) {
        String resource = fundao.getResource();
        String method = fundao.getResourcMethod();
        if(resource != null
                && method != null
                && resource.equals(Constants.NAV_RES_NAME_ICC)
                && method.equals(Constants.NAV_ATTR_METHOD_GETPENDINGINTENT)) {
            List<FunArgs> parms = fundao.getFunArgs();
            if(parms != null && !parms.isEmpty()) {
                String piDest = parms.get(0).getArgName();
                if(piDest.startsWith(Constants.PRIM_PREFIX)
                        && piDest.split(Constants.PRIM_PREFIX).length > 1) {
                    return (scope != null
                            && scope.keySet().contains(piDest.split(Constants.PRIM_PREFIX)[1]));
                }
                else
                    return false;
            }
            else
                return false;
        }
        else {
            return true;
        }
    }

    private boolean isJSExecSafe(FunDao funDao) {
        FunArgs funArgs = funDao.getFunArgs().get(0);
        String url = funArgs.getArgName();
        if(url.startsWith(Constants.PRIM_PREFIX))
            url = url.split("/")[1];
        if(url.startsWith(Constants.NAV_URI_HTTP + ":"))
            return false;
        else {
            if(funDao.getAllowJS() != null && funDao.getAllowJS().equals(Constants.TRUE))
                return funDao.isWhitelistExists();
            else
                return true;
        }
    }

    private boolean isLoadURLWhiteListEmpty(Element element) {
        NodeList patterns = element.getElementsByTagName(Constants.TAG_PATTERN);
        if (patterns.getLength() == 0) return true;
        else {
            for(int i = 0; i < patterns.getLength(); i++) {
                String urlPattern = ((Element) patterns.item(i)).getAttribute(Constants.NAV_ATTR_PATTERN_CASE);
                if(urlPattern == null || "".equals(urlPattern)) return true;
            }
        }
        return false;
    }
    private boolean isLoadURLIntentUnsafe(Element element) {
        NodeList patterns = element.getElementsByTagName(Constants.TAG_PATTERN);
        if (patterns.getLength() == 0) return true;
        else {
            for(int i = 0; i < patterns.getLength(); i++) {
                Element e = (Element) patterns.item(i);
                String urlPattern = e.getAttribute(Constants.NAV_ATTR_PATTERN_CASE);
                if(urlPattern.startsWith(Constants.NAV_ATTR_PATTERN_INTENT) && urlPattern.split("/").length != 3)
                    return true;
            }
        }
        return false;
    }



    private boolean isSysEvent(String name) {
        List<String> sysEvents = new ArrayList<>();
        sysEvents.add("ACTION_AIRPLANE_MODE_CHANGED");
        sysEvents.add("ACTION_APPLICATION_RESTRICTIONS_CHANGED");
        sysEvents.add("ACTION_BATTERY_CHANGED");
        sysEvents.add("ACTION_BATTERY_LOW");
        sysEvents.add("ACTION_BATTERY_OKAY");
        sysEvents.add("ACTION_BOOT_COMPLETED");
        sysEvents.add("ACTION_CONFIGURATION_CHANGED");
        sysEvents.add("ACTION_DEVICE_STORAGE_LOW");
        sysEvents.add("ACTION_DEVICE_STORAGE_OK");
        sysEvents.add("ACTION_DREAMING_STARTED");
        sysEvents.add("ACTION_DREAMING_STOPPED");
        sysEvents.add("ACTION_EXTERNAL_APPLICATIONS_AVAILABLE");
        sysEvents.add("ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE");
        sysEvents.add("ACTION_LOCALE_CHANGED");
        sysEvents.add("ACTION_LOCKED_BOOT_COMPLETED");
        sysEvents.add("ACTION_MY_PACKAGE_REPLACED");
        sysEvents.add("ACTION_MY_PACKAGE_SUSPENDED");
        sysEvents.add("ACTION_MY_PACKAGE_UNSUSPENDED");
        sysEvents.add("ACTION_NEW_OUTGOING_CALL");
        sysEvents.add("ACTION_PACKAGES_SUSPENDED");
        sysEvents.add("ACTION_PACKAGES_UNSUSPENDED");
        sysEvents.add("ACTION_PACKAGE_ADDED");
        sysEvents.add("ACTION_PACKAGE_CHANGED");
        sysEvents.add("ACTION_PACKAGE_DATA_CLEARED");
        sysEvents.add("ACTION_PACKAGE_FIRST_LAUNCH");
        sysEvents.add("ACTION_PACKAGE_FULLY_REMOVED");
        sysEvents.add("ACTION_PACKAGE_INSTALL");
        sysEvents.add("ACTION_PACKAGE_NEEDS_VERIFICATION");
        sysEvents.add("ACTION_PACKAGE_REMOVED");
        sysEvents.add("ACTION_PACKAGE_REPLACED");
        sysEvents.add("ACTION_PACKAGE_RESTARTED");
        sysEvents.add("ACTION_PACKAGE_VERIFIED");
        sysEvents.add("ACTION_POWER_CONNECTED");
        sysEvents.add("ACTION_POWER_DISCONNECTED");
        sysEvents.add("ACTION_REBOOT");
        sysEvents.add("ACTION_SCREEN_OFF");
        sysEvents.add("ACTION_SCREEN_ON");
        sysEvents.add("ACTION_SHUTDOWN");
        sysEvents.add("ACTION_TIMEZONE_CHANGED");
        sysEvents.add("ACTION_TIME_TICK");
        sysEvents.add("ACTION_UID_REMOVED");
        sysEvents.add("ACTION_USER_PRESENT");
        return sysEvents.contains(name);
    }
}
