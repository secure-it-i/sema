package androidx.naigation.checks.dao;

import com.android.tools.lint.detector.api.Location;

public class NavArgumentDao {
    private Location location;
    private String argName;
    private String argValue;
    private String sensTyp = "high";

    public NavArgumentDao(Location location, String argName, String argValue) {
        this.location = location;
        this.argName = argName;
        this.argValue = argValue;
    }

    public void setSensTyp(String sensTyp) {
        this.sensTyp = sensTyp;
    }

    public Location getLocation() {
        return location;
    }

    public String getArgName() {
        return argName;
    }

    public String getArgValue() {
        return argValue;
    }

    public String getSensTyp() {
        return sensTyp;
    }
}
