package androidx.naigation.checks.util;

public class Constants {

    public final static String PRIM_PREFIX = "prim/";
    public final static String TRUE = "true";
    public final static String FALSE = "false";
    public final static String ALL = "ALL";

    public final static String TAG_BUTTON = "Button";
    public final static String TAG_TEXTVIEW = "TextView";
    public final static String TAG_EDITTEXT = "EditText";
    public final static String TAG_LISTVIEW = "ListView";
    public final static String TAG_IMAGEVIW = "ImageView";
    public final static String TAG_WEBVIEW = "WebView";
    public final static String TAG_CHECKBOX = "CheckBox";
    public final static String TAG_WIDGET = "widget";
    public final static String TAG_ARGUMENT = "argument";
    public final static String TAG_FRAGMENT = "fragment";
    public final static String TAG_PARM = "parm";
    public final static String TAG_FUN = "fun";
    public final static String TAG_DEEPLINK = "deepLink";
    public final static String TAG_CONSTRAINT = "constraint";
    public final static String TAG_NOT = "not";
    public final static String TAG_AND = "and";
    public final static String TAG_OR = "or";
    public final static String TAG_WHITELIST = "whitelist";
    public final static String TAG_PATTERN = "pattern";
    public final static String TAG_EVENT_RECVR = "event-receiver";
    public final static String TAG_EVENT = "event";

    public static final String ATTR_NAV_ACTION_REQUIRESUSER = "requiresUser";
    public static final String ATTR_NAV_ACTION_WIDGET = "widgetOn";
    public static final String ATTR_NAV_PARENT_LAYOUT = "tools:layout";
    public static final String ATTR_ID = "android:id";
    public static final String ATTR_TASKAFF = "android:taskAffinity";
    public static final String NAV_ATTR_WID = "wid";
    public static final String NAV_ACTION_DEST = "app:destination";
    public static final String NAV_ATTR_WIDGET_VALUE = "value";
    public static final String NAV_ATTR_NAME = "android:name";
    public static final String NAV_ATTR_PARM_ARG = "arg";
    public static final String NAV_ATTR_VAL_ARG = "value";
    public static final String NAV_ATTR_START_DEST = "app:startDestination";
    public static final String NAV_ATTR_ACCESSPOLICY = "accessPolicy";
    public static final String NAV_ATTR_DEEPLINK_URI = "app:uri";
    public static final String NAV_ATTR_CONSQ = "consequence";
    public static final String NAV_ATTR_RESOURCE="resource";
    public static final String NAV_ATTR_LOCAL="local";
    public static final String NAV_ATTR_METHOD = "method";
    public static final String NAV_ATTR_METHOD_GET = "get";
    public static final String NAV_ATTR_METHOD_POST = "post";
    public static final String NAV_ATTR_METHOD_LOADURL = "loadUrl";
    public static final String NAV_ATTR_METHOD_LOADDATA = "loadDataWithBaseUrl";
    public static final String NAV_ATTR_METHOD_GETPENDINGINTENT = "getPendingIntent";
    public static final String NAV_ATTR_METHOD_SENDBROADCAST = "sendBroadcast";
    public static final String NAV_ATTR_METHOD_SENDORDRBROADCAST = "sendOrderedBroadcast";
    public static final String NAV_ATTR_SENS_TYP = "sensitive";
    public static final String NAV_ATTR_CERTPINNING = "useCertificatePinning";
    public static final String NAV_ATTR_JSEXEC = "allowJsExec";
    public static final String NAV_ATTR_CONTENT_ACCESS = "allowContentAccess";
    public static final String NAV_ATTR_FILE_ACCESS = "allowFileAccess";
    public static final String NAV_ATTR_ACCEPT_COOKIE = "acceptCookie";
    public static final String NAV_ATTR_PATTERN_CASE = "case";
    public static final String NAV_ATTR_PATTERN_INTENT = "intent";
    public static final String NAV_ATTR_PATTERN_INTENT_ACT = "intentAction";
    public static final String NAV_ATTR_PATTERN_INTENT_PACK = "intentPackage";
    public static final String NAV_ATTR_ACCESS = "access";
    public static final String NAV_ATTR_PERM = "permission";
    public static final String NAV_ATTR_PERMNM = "permission-name";
    public static final String NAV_ATTR_ENCRYPT = "encrypt";
    public static final String NAV_ATTR_DECRYPT = "decrypt";
    public static final String NAV_ATTR_PBE = "pbe";

    public static final String NAV_PREFIX_VAR = "var/";
    public static final String NAV_PREFIX_FUN = "fun/";

    public static final String NAV_ACTIVITY_ATTR_TYPE = "type";
    public static final String NAV_ACTIVITY_ATTR_ACTION = "app:action";
    public static final String NAV_ACTIVITY_ATTR_TARGETPACKAGE = "app:targetPackage";

    public static final String NAV_RES_NAME_INTERNET = "INTERNET";
    public static final String NAV_RES_NAME_EXTSTORE = "EXT_STORE";
    public static final String NAV_RES_NAME_ICC = "ICC";
    public static final String NAV_RES_NAME_KEYSTORE = "KEYSTORE";
    public static final String NAV_RES_NAME_CIPHER = "CIPHER";
    public static final String NAV_RES_NAME_SOCKET = "SOCKET";
    public static final String NAV_RES_NAME_CLIPBORAD = "CLIPBOARD_SERVICE";

    public static final String NAV_URI_HTTP = "http";
    public static final String NAV_ACTIVITY_TYPE_EXT="external";

    public static final String NAV_SENS_HIGH = "high";
    public static final String NAV_SENS_LOW = "low";
    public static final String NAV_ACCESS_ALL = "all";
    public static final String NAV_ACCESS_USER = "user";
    public static final String NAV_ACCESS_OWN = "own";

    public static final String USER_ENTERED_VAL = "SEMA_CONSTANT_1";
}
