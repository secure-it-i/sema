package androidx.naigation.checks.dao;

import com.android.tools.lint.detector.api.Location;

public class NavActivityDao {
    private String activityId;
    private Location location;

    public NavActivityDao(String activityId, Location location) {
        this.activityId = activityId;
        this.location = location;
    }

    public String getActivityId() {
        return activityId;
    }

    public Location getLocation() {
        return location;
    }
}
