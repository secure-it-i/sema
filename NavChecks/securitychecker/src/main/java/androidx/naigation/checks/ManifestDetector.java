package androidx.naigation.checks;

import androidx.naigation.checks.util.Constants;

import com.android.SdkConstants;
import com.android.resources.ResourceFolderType;
import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Implementation;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.ResourceXmlDetector;
import com.android.tools.lint.detector.api.Scope;
import com.android.tools.lint.detector.api.Severity;
import com.android.tools.lint.detector.api.XmlContext;

import org.w3c.dom.Element;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class ManifestDetector extends ResourceXmlDetector {
    public static final Issue MANIFEST_TASKAFFINITY = Issue.create("TaskAffinity",
            "Task Affinity must be set to empty",
            "Activities in an app must explicitly have an empty task affinity",
            Category.LINT, 10, Severity.ERROR,
            new Implementation(ManifestDetector.class, Scope.MANIFEST_SCOPE));

    public ManifestDetector() {

    }

    @Override
    public Collection<String> getApplicableElements() {
        return Collections.singletonList(SdkConstants.TAG_ACTIVITY);
    }
    @Override
    public void visitElement(XmlContext context, Element element) {
        String name = element.getTagName();
        System.out.println("Manifest tag = " + name);
        if(name.equals(SdkConstants.TAG_ACTIVITY)) {
            if(!element.hasAttribute(Constants.ATTR_TASKAFF))
                context.report(MANIFEST_TASKAFFINITY, context.getLocation(element),"task affinity of an activity must be set to \"\"");
            if(element.hasAttribute(Constants.ATTR_TASKAFF) && !"".equals(element.getAttribute(Constants.ATTR_TASKAFF)))
                context.report(MANIFEST_TASKAFFINITY, context.getLocation(element),"task affinity of an activity must be set to \"\"");
        }
    }

}
