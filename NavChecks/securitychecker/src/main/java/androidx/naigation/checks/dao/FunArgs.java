package androidx.naigation.checks.dao;

public class FunArgs {
    private String argName;
    private boolean consq = true;

    public String getArgName() {
        return argName;
    }

    public void setArgName(String argName) {
        this.argName = argName;
    }

    public boolean isConsq() {
        return consq;
    }

    public void setConsq(boolean consq) {
        this.consq = consq;
    }
}
