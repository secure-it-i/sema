package androidx.naigation.checks.dao;

import com.android.tools.lint.detector.api.Location;

import java.util.List;

public class FunDao {
    private Location location;
    private String name;
    private String sensTyp;
    private String resource;
    private String resourcMethod;
    private String resourceUri;
    private String useCertificatePinning;
    private String allowJS;
    private String allowContentAccess;
    private String allowFileAccess;
    private String acceptCookie;
    private String pbe;
    private boolean whitelistExists = false;
    private boolean isLocal;

    private List<FunArgs> funArgs;

    public FunDao(Location location, String name,List<FunArgs> funArgs) {
        this.location = location;
        this.name = name;
        this.funArgs = funArgs;
    }

    public String getAllowJS() {
        return allowJS;
    }

    public void setAllowJS(String allowJS) {
        this.allowJS = allowJS;
    }

    public String getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getResourcMethod() {
        return resourcMethod;
    }

    public void setResourcMethod(String resourcMethod) {
        this.resourcMethod = resourcMethod;
    }

    public String getSensTyp() {
        return sensTyp;
    }

    public void setSensTyp(String sensTyp) {
        this.sensTyp = sensTyp;
    }

    public List<FunArgs> getFunArgs() {
        return funArgs;
    }

    public Location getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public String getUseCertificatePinning() {
        return useCertificatePinning;
    }

    public void setUseCertificatePinning(String useCertificatePinning) {
        this.useCertificatePinning = useCertificatePinning;
    }

    public boolean isWhitelistExists() {
        return whitelistExists;
    }

    public void setWhitelistExists(boolean whitelistExists) {
        this.whitelistExists = whitelistExists;
    }

    public String getAllowContentAccess() {
        return allowContentAccess;
    }

    public void setAllowContentAccess(String allowContentAccess) {
        this.allowContentAccess = allowContentAccess;
    }

    public String getAllowFileAccess() {
        return allowFileAccess;
    }

    public void setAllowFileAccess(String allowFileAccess) {
        this.allowFileAccess = allowFileAccess;
    }

    public String getAcceptCookie() {
        return acceptCookie;
    }

    public void setAcceptCookie(String acceptCookie) {
        this.acceptCookie = acceptCookie;
    }

    public String getPbe() {
        return pbe;
    }

    public void setPbe(String pbe) {
        this.pbe = pbe;
    }

    public boolean isLocal() {
        return isLocal;
    }

    public void setLocal(boolean local) {
        isLocal = local;
    }
}
