# SeMA : Securing Mobile Apps

A methodology for designing secure Android apps from an app's storyboard.

**NavChecks** contains the lint checks for verifying pre-defined security properties.

**plugins** contains the code for translating navigation graphs to Android source code.

**test-apps** contain sample apps built with SeMA.

**ghera-apps** contain apps based on the benchmarks in the Ghera repository built with SeMA.

## Required Software

- Android studio 3.4.1 and later
- JDK 1.8

### How to build NavChecks

Use the following commands:

`$ cd /path/to/NavChecks/`

`$ ./gradlew assemble`

### How to setup NavChecks in an app
Append the following snippet to *settings.gadle* file in the app's project folder:


```
def includeProject(name, filePath) {
  settings.include(name)

  def file
  if(filePath instanceof String) {
      file = new File(filePath)
  }
  else {
      file = filePath
  }
  project(name).projectDir = file
}
includeProject(":nav-checks","/path/to/NavChecks/securitychecker/")
```


Next, append the following dependencies to *app/build.gradle*:

```
implementation 'androidx.navigation:navigation-fragment:2.1.0'
lintChecks project(":nav-checks")
```

Note that currently, NavChecks works only with `androidx.navigation:navigation-fragment:2.1.0`. So, if you have another version in your dependencies, then replace it with this version.

See sample [setting.gradle](https://bitbucket.org/secure-it-i/sema/src/master/ghera-apps/Crypto/BlockCipher-ECB-InfoExposure/Secure/settings.gradle)

See sample [app/build.gradle](https://bitbucket.org/secure-it-i/sema/src/master/ghera-apps/Crypto/BlockCipher-ECB-InfoExposure/Secure/app/build.gradle)

## How to run NavChecks in an app

### Via Android Studio:

Open the *gradle* tool window in Android Studio. Under *app/Tasks/verification*, click on the *lint* task to run *NavChecks*

### Via command line:

`$ cd /path/to/app/folder`

`$ ./gradlew lint`

## How to build Code Generation plugin

Use the following commands:

`$ cd /path/to/plugins/codegen`

`$ ./gradlew uploadArchives`

## How to set up Code Generation plugin in an app

Append the following snippet to the *repositories* block in the top level *build.gradle* file:


```
maven {
          url '/path/to/plugins/codegen/repo/'
      }
```


Next, append the following line to the *dependencies* block in the top level *build.gradle* file:

`classpath 'sema.plugins:codegen:1.0-SNAPSHOT'`

Next, append the following snippet to *app/build.gradle*:

```
task generateCodeFromNav(type: sema.codegen.NavGenTask) {
    pathToProj = "${project.projectDir}"
    pathToSrc = "${project.projectDir}"
}
```

See sample top-level [build.gradle](https://bitbucket.org/secure-it-i/sema/src/master/ghera-apps/Crypto/BlockCipher-ECB-InfoExposure/Secure/build.gradle)

See sample [app/build.gradle](https://bitbucket.org/secure-it-i/sema/src/master/ghera-apps/Crypto/BlockCipher-ECB-InfoExposure/Secure/app/build.gradle)

## How to generate code for an app

### Via Android Studio:

Open the *gradle* tool window in Android Studio. Under *app/Tasks/other*, click on the *generateCodeFromNav* task to generate code from navigation graphs.

### Via command line:

`$ cd /path/to/app/folder`

`$ ./gradlew generateCodeFromNav`


## Additional Info

- Access generated code:
The generated code will be available at *app/build/generated/source/source*. It is advisable not to modify the generated code.

- Adding business logic:
The generated code can be extended with business logic classes in *app/src/main/.../busLogic*.

## Attribution
Copyright (c) 2020, Kansas State University

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

Creator: Joydeep Mitra (joydeep@ksu.edu)
