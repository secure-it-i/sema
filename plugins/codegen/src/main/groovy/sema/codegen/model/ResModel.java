package sema.codegen.model;

/**
 * Created by Joy on 10/18/19.
 */
public class ResModel {
    private String resource;
    private String funCallStmt;

    public ResModel(String resource, String funCallStmt) {
        this.resource = resource;
        this.funCallStmt = funCallStmt;
    }

    public String getResource() {
        return resource;
    }


    public String getFunCallStmt() {
        return funCallStmt;
    }
}
