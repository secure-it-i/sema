package sema.codegen.model;

/**
 * Created by Joy on 1/27/20.
 */
public class EventModel {
    private String id;
    private String name;
    private String priority;
    private FunModel funModel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FunModel getFunModel() {
        return funModel;
    }

    public void setFunModel(FunModel funModel) {
        this.funModel = funModel;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
