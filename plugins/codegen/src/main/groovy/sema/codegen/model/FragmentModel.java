package sema.codegen.model;

import java.util.List;

/**
 * Created by Joy on 7/23/19.
 */
public class FragmentModel {

    private String fragId = null;
    private String layoutId = null;
    private List<WidgetModel> widgets = null;
    private List<ArgumentModel> inActionArgs = null;
    private List<ActionModel> outActions = null;
    private List<ArgumentModel> args = null;

    public List<WidgetModel> getWidgets() {
        return widgets;
    }

    public void setWidgets(List<WidgetModel> widgets) {
        this.widgets = widgets;
    }

    public List<ArgumentModel> getInActionArgs() {
        return inActionArgs;
    }

    public void setInActionArgs(List<ArgumentModel> inActionArgs) {
        this.inActionArgs = inActionArgs;
    }

    public String getFragId() {
        return fragId;
    }

    public void setFragId(String fragId) {
        this.fragId = fragId;
    }

    public String getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
    }

    public List<ActionModel> getOutActions() {
        return outActions;
    }

    public void setOutActions(List<ActionModel> outActions) {
        this.outActions = outActions;
    }

    public List<ArgumentModel> getArgs() {
        return args;
    }

    public void setArgs(List<ArgumentModel> args) {
        this.args = args;
    }

    public FragmentModel() {

    }
}
