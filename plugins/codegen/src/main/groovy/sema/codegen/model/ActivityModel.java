package sema.codegen.model;

/**
 * Created by Joy on 12/4/19.
 */
public class ActivityModel {
    private String activityId;
    private String layoutId;
    private String intentAction;
    private String intentPackage;

    public ActivityModel(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityId() {
        return activityId;
    }

    public String getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
    }

    public String getIntentAction() {
        return intentAction;
    }

    public void setIntentAction(String intentAction) {
        this.intentAction = intentAction;
    }

    public String getIntentPackage() {
        return intentPackage;
    }

    public void setIntentPackage(String intentPackage) {
        this.intentPackage = intentPackage;
    }
}
