package sema.codegen.model;

/**
 * Created by Joy on 7/24/19.
 */
public class ArgumentModel {
    private String name = null;
    private String primitiveValue = null;
    private String varValue = null;
    private String idValue = null;
    private FunModel funValue = null;

    public String getIdValue() {
        return idValue;
    }

    public void setIdValue(String idValue) {
        this.idValue = idValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrimitiveValue() {
        return primitiveValue;
    }

    public void setPrimitiveValue(String primitiveValue) {
        this.primitiveValue = primitiveValue;
    }

    public String getVarValue() {
        return varValue;
    }

    public void setVarValue(String varValue) {
        this.varValue = varValue;
    }

    public FunModel getFunValue() {
        return funValue;
    }

    public void setFunValue(FunModel funValue) {
        this.funValue = funValue;
    }
}
