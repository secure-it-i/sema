package sema.codegen.model;

/**
 * Created by Joy on 7/25/19.
 */
public class ConstraintModel {

    private String op = null;
    private FunModel opl = null;
    private ConstraintModel opr = null;

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public FunModel getOpl() {
        return opl;
    }

    public void setOpl(FunModel opl) {
        this.opl = opl;
    }

    public ConstraintModel getOpr() {
        return opr;
    }

    public void setOpr(ConstraintModel opr) {
        this.opr = opr;
    }
}
