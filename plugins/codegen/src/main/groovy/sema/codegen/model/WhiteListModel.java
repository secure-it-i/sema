package sema.codegen.model;

import java.util.List;

/**
 * Created by Joy on 2/24/20.
 */
public class WhiteListModel {
    private List<String> patterns;

    public List<String> getPatterns() {
        return patterns;
    }

    public void setPatterns(List<String> patterns) {
        this.patterns = patterns;
    }
}
