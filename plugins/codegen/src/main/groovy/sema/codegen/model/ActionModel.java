package sema.codegen.model;

import java.util.List;

/**
 * Created by Joy on 7/24/19.
 */
public class ActionModel {

    private String actionId = null;
    private String destId = null;
    private String widgetOn = null;
    private ConstraintModel constraintModel = null;
    private String gesture = null;
    private List<ArgumentModel> args = null;

    public ConstraintModel getConstraintModel() {
        return constraintModel;
    }

    public void setConstraintModel(ConstraintModel constraintModel) {
        this.constraintModel = constraintModel;
    }

    public ActionModel() {

    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getDestId() {
        return destId;
    }

    public void setDestId(String destId) {
        this.destId = destId;
    }

    public String getWidgetOn() {
        return widgetOn;
    }

    public void setWidgetOn(String widgetOn) {
        this.widgetOn = widgetOn;
    }

    public String getGesture() {
        return gesture;
    }

    public void setGesture(String gesture) {
        this.gesture = gesture;
    }

    public List<ArgumentModel> getArgs() {
        return args;
    }

    public void setArgs(List<ArgumentModel> args) {
        this.args = args;
    }
}
