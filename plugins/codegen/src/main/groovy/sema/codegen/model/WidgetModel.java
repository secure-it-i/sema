package sema.codegen.model;

/**
 * Created by Joy on 7/29/19.
 */
public class WidgetModel {

    private String wid = null;
    private String primitiveVal = null;
    private String variableVal = null;
    private FunModel funModel = null;

    public String getWid() {
        return wid;
    }

    public void setWid(String wid) {
        this.wid = wid;
    }

    public String getPrimitiveVal() {
        return primitiveVal;
    }

    public void setPrimitiveVal(String primitiveVal) {
        this.primitiveVal = primitiveVal;
    }

    public String getVariableVal() {
        return variableVal;
    }

    public void setVariableVal(String variableVal) {
        this.variableVal = variableVal;
    }

    public FunModel getFunModel() {
        return funModel;
    }

    public void setFunModel(FunModel funModel) {
        this.funModel = funModel;
    }
}
