package sema.codegen.model;

import sema.codegen.util.Constants;

import java.util.List;

/**
 * Created by Joy on 7/25/19.
 */
public class FunModel {

    private String name = null;
    private String resource = null;
    private String uri = null;
    private String method = null;

    private String protocol = null;
    private String host = null;
    private String filePath = null;
    private String publicKey = null;
    private String pbe = Constants.FALSE;
    private String requiredPerm;
    private String local = Constants.TRUE;

    private List<String> primitives = null;
    private List<String> variables = null;
    private List<String> idValues = null;
    private List<FunModel> funs = null;

    private boolean allowJS = false;

    private WhiteListModel whiteListModel = null;


    public List<String> getIdValues() {
        return idValues;
    }

    public void setIdValues(List<String> idValues) {
        this.idValues = idValues;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public List<String> getPrimitives() {
        return primitives;
    }

    public void setPrimitives(List<String> primitives) {
        this.primitives = primitives;
    }

    public List<String> getVariables() {
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }

    public List<FunModel> getFuns() {
        return funs;
    }

    public void setFuns(List<FunModel> funs) {
        this.funs = funs;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPbe() {
        return pbe;
    }

    public void setPbe(String pbe) {
        this.pbe = pbe;
    }

    public String getRequiredPerm() {
        return requiredPerm;
    }

    public void setRequiredPerm(String requiredPerm) {
        this.requiredPerm = requiredPerm;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public boolean isAllowJS() {
        return allowJS;
    }

    public void setAllowJS(boolean allowJS) {
        this.allowJS = allowJS;
    }

    public WhiteListModel getWhiteListModel() {
        return whiteListModel;
    }

    public void setWhiteListModel(WhiteListModel whiteListModel) {
        this.whiteListModel = whiteListModel;
    }
}
