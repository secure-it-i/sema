package sema.codegen

import com.squareup.javapoet.JavaFile
import com.squareup.javapoet.TypeSpec
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

import javax.lang.model.element.Modifier

/**
 * Created by Joy on 8/4/19.
 */
class ScratchPad extends DefaultTask {
    def outPath = ''

    @TaskAction
    def demoAction () {
        File outdir = new File("${outPath}/generated/source/")
        println("Start writing to = " + outdir)
        TypeSpec semaClass = TypeSpec.classBuilder("SemaClass")
                .addModifiers(Modifier.PUBLIC)
                .build()
        JavaFile javaFile = JavaFile.builder("sema.apps.mockuber",semaClass).build()
        javaFile.writeTo(outdir)
        File outFile = new File("${outPath}/generated/source/sema/apps/mockuber/SemaClass.java")
        outFile.setWritable(false,false) ? println("success") : println("failed")
    }
}
