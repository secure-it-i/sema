package sema.codegen

import com.squareup.javapoet.*
import groovy.xml.XmlUtil
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.w3c.dom.Node
import sema.codegen.model.*
import sema.codegen.util.Constants
import sema.codegen.util.NavParser
import sema.codegen.util.UsefulMethods

import javax.lang.model.element.Modifier
import javax.xml.namespace.QName

/**
 * Created by Joy on 7/31/19.
 */
class NavGenTask extends DefaultTask {
    def pathToProj = ''
    def pathToSrc = ''

    List<ResModel> resPermRequests = null
    List<MethodSpec> busLogicmethods = null
    List<MethodSpec> methodEnv = null
    def widgetTypeMap = [:]
    def packageName = ''

    String getClassName(String id) {
        String head = id.substring(0,id.indexOf("_"))
        String tail = id.substring(id.indexOf("_")+1)
        if(tail.contains("_"))
            return (head.substring(0,1).toUpperCase() +
                    head.substring(1) +
                    getClassName(tail))
        else
            return (head.substring(0,1).toUpperCase() +
                    head.substring(1) +
                    tail.substring(0,1).toUpperCase() +
                    tail.substring(1))
    }

    String getMethodName(String m) {
        return ("get" + m.substring(0,1).toUpperCase() + m.substring(1))
    }

    String getInitMethodName(String m) {
        return ("init" + m.substring(0,1).toUpperCase() + m.substring(1))
    }

    FragmentModel updateFragmenModel(groovy.util.Node anode, FragmentModel fragmentModel) {
        List<ArgumentModel> inArgs = new ArrayList<>()
        anode.argument.each {
            inArgs.add(NavParser.parseArgumentNode(it))
        }
        if(!inArgs.isEmpty()) fragmentModel.setInActionArgs(inArgs)
        return fragmentModel
    }

    String makeFunCall(FunModel funModel) {
        String stmt = removeIndicator(funModel.getName()) + "("
        if(funModel.getResource() == null
                || "null".equals(funModel.getResource())
                || "".equals(funModel.getResource()))
            stmt = Constants.OBJBUSLOGIC + "." + removeIndicator(funModel.getName()) + Constants.BUSLOGIC + "("
        StringBuffer funArgs = new StringBuffer()
        if(funModel.getPrimitives() != null) {
            funModel.getPrimitives().each {
                if(it.startsWith(Constants.RECV_INPUT_INDICATOR)) {
                    funArgs.append("intent.getStringExtra(\"" + it.split("/")[1] + "\")")
                    funArgs.append(",")
                }
                else if(removeIndicator(it) == "null") {
                    funArgs.append(removeIndicator(it))
                    funArgs.append(",")
                }
                else {
                    funArgs.append("\"" + removeIndicator(it) + "\"")
                    funArgs.append(",")
                }
            }
        }
        if(funModel.getIdValues() != null) {

            funModel.getIdValues().each {
                if(isWidgetTypeText(removeIndicator(it))) {
                    funArgs.append(removeIndicator(it) + "Obj.getText().toString()")
                    funArgs.append(",")
                }
                if(it.contains(Constants.ASYNC_BACKGROUND_PARAM_NAME)) {
                    funArgs.append(it)
                    funArgs.append(",")
                }
            }
        }
        if(funModel.getVariables() != null) {
            funModel.getVariables().each {
                funArgs.append(getMethodName(removeIndicator(it)) + "(getArguments(),\"" + getParmName(it) + "\")")
                funArgs.append(",")
            }
        }
        if(funModel.getFuns() != null) {
            funModel.getFuns().each {
                funArgs.append(makeFunCall(it))
                funArgs.append(",")
            }
        }
        if(funArgs.toString() != null
                && funArgs.toString().contains(","))
            return (stmt + funArgs
                    .toString()
                    .substring(0,funArgs.toString().lastIndexOf(",")) + ")")
        else return (stmt + ")")
    }

    String getVarName(String x, int i) {
        if(x.startsWith(Constants.PROTOCOL_INDICATOR))
            return Constants.PARM_PROTOCOL
        else if(x.startsWith(Constants.HOST_INDICATOR))
            return Constants.PARM_HOST
        else if(x.startsWith(Constants.FILE_PATH_INDICATOR))
            return Constants.PARM_FILEPATH
        else if(x.startsWith(Constants.DIR_INDICATOR))
            return Constants.PARM_DIR
        else if(x.startsWith(Constants.SYMMETRIC_KEY_INDICTOR))
            return Constants.PARM_KEY
        else {
            return "param" + i
        }
    }

    String getParmName(String x) {
        if(x.startsWith(Constants.PROTOCOL_INDICATOR))
            return Constants.PARM_PROTOCOL
        else if(x.startsWith(Constants.HOST_INDICATOR))
            return Constants.PARM_HOST
        else if(x.startsWith(Constants.FILE_PATH_INDICATOR))
            return Constants.PARM_FILEPATH
        else if(x.startsWith(Constants.DIR_INDICATOR))
            return Constants.PARM_DIR
        else if(x.startsWith(Constants.SYMMETRIC_KEY_INDICTOR))
            return Constants.PARM_KEY
        else
            return x
    }

    String removeIndicator(String x) {
        if(x.startsWith(Constants.PROTOCOL_INDICATOR))
            return x.split(Constants.PROTOCOL_INDICATOR)[1]
        else if(x.startsWith(Constants.HOST_INDICATOR))
            return x.split(Constants.HOST_INDICATOR)[1]
        else if(x.startsWith(Constants.FILE_PATH_INDICATOR))
            return x.split(Constants.FILE_PATH_INDICATOR)[1]
        else if(x.startsWith(Constants.DIR_INDICATOR))
            return x.split(Constants.DIR_INDICATOR)[1]
        else if(x.startsWith(Constants.SYMMETRIC_KEY_INDICTOR))
            return x.split(Constants.SYMMETRIC_KEY_INDICTOR)[1]
        else
            return x
    }

    boolean doesResourceNeedPermRequest(String resource) {
        if(resource == null) return false
        return (resource.equals(Constants.RES_INT_STORE))
    }

    List<MethodSpec> makeFun(FunModel funModel, TypeName returnType) {
        TypeName typeVariableName = TypeVariableName.get(String.class)
        TypeName rType
        if(returnType != null) {
            rType = returnType
        }
        else
            rType = typeVariableName
        List<MethodSpec> methods = new ArrayList<>()
        String funName = funModel.getName()
        if(funModel.getResource() == null
                || "null".equals(funModel.getResource())
                || "".equals(funModel.getResource())) {
            funName = funName + Constants.BUSLOGIC
        }
        MethodSpec method = MethodSpec.methodBuilder(removeIndicator(funName))
        //.addTypeVariable(typeVariableName)
                .returns(rType)
                .build()

        if(!funModel.getResource().equals("null")) {
            method = method.toBuilder().addModifiers(Modifier.PRIVATE).build()
            if(doesResourceNeedPermRequest(funModel.getResource())) {
                ResModel resModel = new ResModel(funModel.getResource(), makeFunCall(funModel))
                resPermRequests.add(resModel)
            }
        }
        else
            method = method.toBuilder().addModifiers(Modifier.PUBLIC).build()
        if(funModel.getPrimitives() != null) {
            funModel.getPrimitives().eachWithIndex { String prim, int i ->
                ParameterSpec primitive = ParameterSpec.builder(String.class,getVarName(prim, i)).build()
                method = method.toBuilder().addParameter(primitive).build()
            }
        }
        if(funModel.getVariables() != null) {
            funModel.getVariables().each {
                ParameterSpec varParam = ParameterSpec.builder(typeVariableName,getParmName(it)).build()
                method = method.toBuilder().addParameter(varParam).build()
            }
        }
        if(funModel.getIdValues() != null) {
            funModel.getIdValues().each {
                ParameterSpec idParam = ParameterSpec.builder(typeVariableName,getParmName(it)).build()
                method = method.toBuilder().addParameter(idParam).build()
            }
        }
        if(funModel.getFuns() != null) {
            funModel.getFuns().each {
                methods.addAll(makeFun(it,null))
                if(it.getName().startsWith(Constants.SYMMETRIC_KEY_INDICTOR)) {
                    ParameterSpec funParam = ParameterSpec
                            .builder(ClassName.get("javax.crypto", "SecretKey")
                            ,getParmName(it.getName())).build()
                    method = method.toBuilder().addParameter(funParam).build()
                }
                else {
                    ParameterSpec funParam = ParameterSpec.builder(typeVariableName,getParmName(it.getName())).build()
                    method = method.toBuilder().addParameter(funParam).build()
                }
            }
        }
        if(Constants.RES_INT_STORE.equals(funModel.getResource())
                && Constants.RES_METHOD_POST.equals(funModel.getMethod())) {
            String busLogicName = Constants.BUSLOGIC + funModel.getName()
            method = UsefulMethods.internalStoreWriteStmt(method,
                    busLogicName,
                    method.parameters
                            .findAll {
                        ParameterSpec p -> !Constants.PARM_FILEPATH.equals(p.name)
                    })

            MethodSpec busLogicMethod = UsefulMethods.storeWriteMethod(method.parameters
                    .findAll {
                ParameterSpec p -> !Constants.PARM_FILEPATH.equals(p.name)
            }, busLogicName)
            //methods.add(busLogicMethod)
            busLogicmethods.add(busLogicMethod)
        }
        else if(Constants.RES_INT_STORE.equals(funModel.getResource())
                && Constants.RES_METHOD_GET.equals(funModel.getMethod())) {
            String busLogicName = Constants.BUSLOGIC + funModel.getName()
            method = UsefulMethods.internalStoreReadStmt(method,
                    busLogicName,
                    method
                            .parameters
                            .findAll {
                        ParameterSpec p -> !Constants.PARM_FILEPATH.equals(p.name)
                    })
            MethodSpec busLogicMethod = UsefulMethods.internalStoreReadMethod(method
                    .parameters
                    .findAll {
                ParameterSpec p -> !Constants.PARM_FILEPATH.equals(p.name)
            }, busLogicName)
            //methods.add(busLogicMethod)
            busLogicmethods.add(busLogicMethod)
        }
        else if(Constants.RES_EXT_STORE.equals(funModel.getResource())
                && Constants.RES_METHOD_POST.equals(funModel.getMethod())) {
            String busLogicName = Constants.BUSLOGIC + funModel.getName()
            method = UsefulMethods.extStoreWriteStmt(method,
                    busLogicName,
                    method
                            .parameters
                            .findAll {ParameterSpec p ->
                        !Constants.PARM_FILEPATH.equals(p.name) && !Constants.PARM_DIR.equals(p.name)
                    })
            MethodSpec busLogicMethod = UsefulMethods.storeWriteMethod(method.parameters
                    .findAll {ParameterSpec p ->
                !Constants.PARM_FILEPATH.equals(p.name) && !Constants.PARM_DIR.equals(p.name)
            }, busLogicName)
            //methods.add(busLogicMethod)
            busLogicmethods.add(busLogicMethod)
        }
        else if(Constants.RES_EXT_STORE.equals(funModel.getResource())
                && Constants.RES_METHOD_GET.equals(funModel.getMethod())) {
            String busLogicName = Constants.BUSLOGIC + funModel.getName()
            method = UsefulMethods.extStoreReadStmt(method,
                    busLogicName,
                    method
                            .parameters
                            .findAll {ParameterSpec p ->
                        !Constants.PARM_FILEPATH.equals(p.name) && !Constants.PARM_DIR.equals(p.name)
                    })
            MethodSpec busLogicMethod = UsefulMethods
                    .internalStoreReadMethod(method.parameters
                    .findAll {ParameterSpec p ->
                !Constants.PARM_FILEPATH.equals(p.name) && !Constants.PARM_DIR.equals(p.name)
            }, busLogicName)
            //methods.add(busLogicMethod)
            busLogicmethods.add(busLogicMethod)
        }
        else if(Constants.RES_INTERNET.equals(funModel.getResource())
                && Constants.RES_METHOD_GET.equals(funModel.getMethod())) {
            def urlParamsMethodNm = "busLogicUrlParamsFor" + funModel.getName()
            def readStreamMethodNm = "busLogicReadStreamFor" + funModel.getName()
            method = UsefulMethods
                    .GethttpStmt(method,
                    urlParamsMethodNm,
                    readStreamMethodNm,
                    method.parameters.
                            findAll {ParameterSpec p ->
                                !p.name.equals(Constants.PARM_HOST) && !p.name.equals(Constants.PARM_PROTOCOL)
                            })

            MethodSpec busLogicMethod1 = UsefulMethods.urlParamsMethod(urlParamsMethodNm,
                    method.parameters
                            .findAll{ ParameterSpec p ->
                        !p.name.equals(Constants.PARM_HOST) && !p.name.equals(Constants.PARM_PROTOCOL)
                    })

            MethodSpec busLogicMethod2 = UsefulMethods.inStreamGetMethod(readStreamMethodNm)

            //methods.add(busLogicMethod1)
            //methods.add(busLogicMethod2)

            busLogicmethods.add(busLogicMethod1)
            busLogicmethods.add(busLogicMethod2)
        }
        else if(Constants.RES_INTERNET.equals(funModel.getResource())
                && Constants.RES_METHOD_POST.equals(funModel.getMethod())) {
            def postParamMethodNm = "busLogicPostParamsFor" + funModel.getName()
            def readStreamMethodNm = "busLogicReadStreamFor" + funModel.getName()

            method = UsefulMethods.PosthttpStmt(method,
                    postParamMethodNm,
                    readStreamMethodNm,
                    method.parameters.
                            findAll{ ParameterSpec p ->
                                !p.name.equals(Constants.PARM_HOST) && !p.name.equals(Constants.PARM_PROTOCOL)
                            })
            MethodSpec busLogicMethod1 = UsefulMethods
                    .postParamsMethod(method.parameters
                    .findAll{ ParameterSpec p ->
                !p.name.equals(Constants.PARM_HOST) && !p.name.equals(Constants.PARM_PROTOCOL)
            },
                    postParamMethodNm)
            MethodSpec busLogicMethod2 = UsefulMethods.inStreamPostMethod(readStreamMethodNm, method.returnType)

            //methods.add(busLogicMethod1)
            //methods.add(busLogicMethod2)

            busLogicmethods.add(busLogicMethod1)
            busLogicmethods.add(busLogicMethod2)
        }
        else if(Constants.RES_CIPHER == funModel.getResource()
                && Constants.RES_METHOD_ENCRYPT == funModel.getMethod()) {
            if(method.parameters.size() == 2)
                method = UsefulMethods.cipherOpMethod(method, method.parameters, Constants.ENCRYPT_MODE)
            else
                throw new RuntimeException("cipher encrypt method does not have the required arguments. Code gen failed!")
            methods.add(UsefulMethods.saveIVMethod())
            methods.add(UsefulMethods.getIVMethod())
        }
        else if(Constants.RES_CIPHER == funModel.getResource()
                && Constants.RES_METHOD_DECRYPT == funModel.getMethod()) {
            if(method.parameters.size() == 2)
                method = UsefulMethods.cipherOpMethod(method, method.parameters, Constants.DECRYPT_MODE)
            else
                throw new RuntimeException("cipher decrypt method does not have the required arguments.Code gen failed!")
            methods.add(UsefulMethods.saveIVMethod())
            methods.add(UsefulMethods.getIVMethod())
        }
        else if(Constants.RES_KEYSTORE == funModel.getResource()
                && Constants.RES_METHOD_GET == funModel.getMethod()) {
            if(method.parameters.size() == 1)
                method = UsefulMethods.readKeyStore(method, method.parameters.get(0).name)
            else
                throw new RuntimeException("Exception occured while generating code for retrieving key from keystore. Code gen failed!")
        }
        else if(Constants.RES_KEYSTORE == funModel.getResource()
                && Constants.RES_METHOD_POST == funModel.getMethod()
                && Constants.TRUE == funModel.getPbe()) {
            if(method.parameters.size() == 2)
                method = UsefulMethods
                        .writePBEKeyStore(method
                        , method.parameters.find {it.name != Constants.PARM_KEY}.name
                        , method.parameters.find {it.name == Constants.PARM_KEY}.name)
            else
                throw new RuntimeException("Exception occurred while gen code for writing pbe key to keystore")
        }
        else if(Constants.RES_KEYSTORE == funModel.getResource()
                && Constants.RES_METHOD_POST == funModel.getMethod()
                && Constants.FALSE == funModel.getPbe()) {
            if (method.parameters.size() == 1)
                method = UsefulMethods.writeKeyStore(method, method.parameters.get(0).name)
            else
                throw new RuntimeException("Exception occurred while gen code for writing key to keystore")
        }
        else if(Constants.RES_INT_STORE == funModel.getResource() && Constants.RES_METHOD_EXIST == funModel.getMethod()) {
            method = method.toBuilder().addStatement("throw new UnsupportedOperationException()").build()
        }
        else if(Constants.RES_INTERNET == funModel.getResource() && Constants.RES_METHOD_LOADURL == funModel.getMethod()) {
            method = method.toBuilder().addStatement("return \$L", method.parameters[0].name).build()
        }
        else if(Constants.RES_INTERNET == funModel.getResource() && Constants.RES_METHOD_LOADDATA == funModel.getMethod()) {
            method = method.toBuilder().addStatement("throw new UnsupportedOperationException()").build()
        }
        else if(Constants.RES_ICC == funModel.getResource() && Constants.RES_METHOD_SENDBRDCST == funModel.getMethod()) {
            int reqCode = new Random().nextInt(1000)
            method = UsefulMethods.sendBroadcastMethod(method, funModel.getRequiredPerm(), funModel.getLocal(), packageName, "setBrdcastIntent", "sendBroadcast", reqCode)
            int mIndx = methods.collect {it.name}.indexOf(Constants.METHOD_ONREQPERM)
            if(mIndx > 0) {
                methods.add(UsefulMethods.permRequestSendBrdcst(methods.get(mIndx), makeFunCall(funModel), reqCode))
            }
            else {
                methods.add(UsefulMethods.permRequestSendBrdcst(UsefulMethods.permRequestMethod(), makeFunCall(funModel), reqCode))
            }
            if(method.parameters.size() > 1) {
                busLogicmethods.add(UsefulMethods.setBrdcastIntentMethod("setBrdcastIntent", method.parameters.tail()))
            }
        }
        else if(Constants.RES_ICC == funModel.getResource() && Constants.RES_METHOD_ORD_SENDBRDCST == funModel.getMethod()) {
            int reqCode = new Random().nextInt(1000)
            method = UsefulMethods.sendBroadcastMethod(method, funModel.getRequiredPerm(), funModel.getLocal(), packageName, "setBrdcastIntent", "sendOrderedBroadcast", reqCode)
            int mIndx = methods.collect {it.name}.indexOf(Constants.METHOD_ONREQPERM)
            if(mIndx > 0) {
                methods.add(UsefulMethods.permRequestSendBrdcst(methods.get(mIndx), makeFunCall(funModel), reqCode))
            }
            else {
                methods.add(UsefulMethods.permRequestSendBrdcst(UsefulMethods.permRequestMethod(), makeFunCall(funModel), reqCode))
            }
            if(method.parameters.size() > 1) {
                busLogicmethods.add(UsefulMethods.setBrdcastIntentMethod("setBrdcastIntent", method.parameters.tail()))
            }
        }
        else if(Constants.RES_ICC == funModel.getResource() && "getResultData" == funModel.getMethod()) {
            method = method.toBuilder().addStatement("return getResultData()").build()
        }
        else if(Constants.RES_ICC == funModel.getResource() && "setResultData" == funModel.getMethod()) {
            method = method.toBuilder().addStatement("setResultData(\$L)", method.parameters[0].name).build()
        }
        else if(Constants.RES_ICC == funModel.getResource() && "getIntentAction" == funModel.getMethod()) {
            method = method.toBuilder().addStatement("return getActivity().getIntent().getAction()").build()
        }
        else if(Constants.RES_VIBRATOR == funModel.getResource()) {
            method = method.toBuilder().addStatement("throw new UnsupportedOperationException()").build()
        }
        else if(Constants.RES_SSLSOCKET == funModel.getResource()) {
            method = method.toBuilder().addStatement("throw new UnsupportedOperationException()").build()
        }
        else if(Constants.RES_CLIPBRD == funModel.getResource()) {
            method = method.toBuilder().addStatement("throw new UnsupportedOperationException()").build()
        }
        else if(Constants.RES_TELPHNY == funModel.getResource()) {
            method = method.toBuilder().addStatement("throw new UnsupportedOperationException()").build()
        }
        else if(Constants.RES_DYNCODELOAD == funModel.getResource()) {
            method = method.toBuilder().addStatement("throw new UnsupportedOperationException()").build()
        }
        else {
            MethodSpec m = method
                    .toBuilder()
                    .addStatement("throw new UnsupportedOperationException()")
                    .build()
            //methods.add(m)
            busLogicmethods.add(m)
        }
        if(!funName.endsWith(Constants.BUSLOGIC))
            methods.add(method)
        return methods
    }

    boolean methodExists(MethodSpec methodSpec) {
        if(methodEnv == null) return false
        else {
            return (methodEnv.find {
                it.name == methodSpec.name &&
                        it.parameters.collect{it.name}
                                .containsAll(methodSpec.parameters.collect {it.name})
            } != null)
        }
    }

    CodeBlock makeListener(String wid, String gesture, Map<String,String> constraintCallMap, Map<String, List<ArgumentModel>> argCallMap, List<String> actionIds, Boolean isPredLongRunning, List<String> constraintWidgetRefs, ActivityModel activityModel) {
        if(gesture == Constants.GESTURE_CLICK) {
            MethodSpec onClickMethod = MethodSpec.methodBuilder("onClick")
                    .addModifiers(Modifier.PUBLIC)
                    .returns(void.class)
                    .addParameter(ClassName.get("android.view","View"),"v")
                    .addAnnotation(Override.class)
                    .build()

            ClassName navigationClass = ClassName.get("androidx.navigation","Navigation")
            ClassName bundle = ClassName.get("android.os","Bundle")
            actionIds.each { String actionId ->
                String constraintCall = constraintCallMap.get(actionId)
                if(constraintCall != null) {
                    if(isPredLongRunning) {
                        if(constraintWidgetRefs != null && !constraintWidgetRefs.isEmpty()) {
                            String taskParms = constraintWidgetRefs
                                    .findAll {
                                isWidgetTypeText(it)}
                            .collect {it + "Obj.getText().toString()"}
                                    .join(",")
                            onClickMethod = onClickMethod
                                    .toBuilder()
                                    .addStatement("new " + actionId + "Task().execute(" + taskParms + ")")
                                    .build()
                        }
                        else {
                            onClickMethod = onClickMethod
                                    .toBuilder()
                                    .addStatement("new " + actionId + "Task().execute()")
                                    .build()
                        }
                    }
                    else {
                        if(argCallMap.get(actionId) != null) {
                            onClickMethod = onClickMethod.toBuilder()
                                    .beginControlFlow("if (" + constraintCall + ")")
                                    .addStatement("\$T destArgs = new \$T()",bundle,bundle)
                                    .build()

                            argCallMap.get(actionId).each { ArgumentModel argumentModel ->
                                onClickMethod = onClickMethod
                                        .toBuilder()
                                        .addStatement(makeActionArgCall(argumentModel))
                                        .build()
                            }
                            if(getIntentCode(activityModel) != null) {
                                onClickMethod = onClickMethod
                                        .toBuilder()
                                        .addCode(getIntentCode(activityModel))
                                        .addStatement("intent.putExtras(destArgs)")
                                        .addStatement("startActivity(intent)")
                                        .endControlFlow()
                                        .build()
                            }
                            else {
                                onClickMethod = onClickMethod
                                        .toBuilder()
                                        .addStatement("\$T.findNavController(getView()).navigate(R.id." + actionId + ", destArgs, null)", navigationClass)
                                        .endControlFlow()
                                        .build()
                            }
                        }
                        else {
                            if(getIntentCode(activityModel) != null) {
                                onClickMethod = onClickMethod
                                        .toBuilder()
                                        .addCode(getIntentCode(activityModel))
                                        .addStatement("startActivity(intent)")
                                        .endControlFlow()
                                        .build()
                            }
                            else {
                                onClickMethod = onClickMethod.toBuilder()
                                        .beginControlFlow("if (" + constraintCall + ")")
                                        .addStatement("\$T.findNavController(getView()).navigate(R.id." + actionId + ", null, null)", navigationClass)
                                        .endControlFlow()
                                        .build()
                            }
                        }
                    }
                }
                else {
                    if(argCallMap.get(actionId) != null) {
                        onClickMethod = onClickMethod.toBuilder()
                                .addStatement("\$T destArgs = new \$T()",bundle,bundle)
                                .build()

                        argCallMap.get(actionId).each { ArgumentModel argumentModel ->
                            onClickMethod = onClickMethod
                                    .toBuilder()
                                    .addStatement(makeActionArgCall(argumentModel))
                                    .build()
                        }
                        if(getIntentCode(activityModel) != null) {
                            onClickMethod = onClickMethod
                                    .toBuilder()
                                    .addCode(getIntentCode(activityModel))
                                    .addStatement("intent.putExtras(destArgs)")
                                    .addStatement("startActivity(intent)")
                                    .build()
                        }
                        else {
                            onClickMethod = onClickMethod
                                    .toBuilder()
                                    .addStatement("\$T.findNavController(getView()).navigate(R.id." + actionId + ", destArgs, null)", navigationClass)
                                    .build()
                        }
                    }
                    else {
                        if(getIntentCode(activityModel) != null) {
                            onClickMethod = onClickMethod
                                    .toBuilder()
                                    .addCode(getIntentCode(activityModel))
                                    .addStatement("startActivity(intent)")
                                    .build()
                        }
                        else {
                            onClickMethod = onClickMethod.toBuilder()
                                    .addStatement("\$T.findNavController(getView()).navigate(R.id." + actionId + ", null, null)", navigationClass)
                                    .build()
                        }
                    }
                }
            }
            return CodeBlock.of("view.findViewById(R.id.\$L).setOnClickListener(\$L)"
                    ,wid
                    ,TypeSpec.anonymousClassBuilder("")
                    .addSuperinterface(ClassName.get("android.view","View.OnClickListener"))
                    .addMethod(onClickMethod)
                    .build())
        }
        return null
    }

    List<MethodSpec> makePredicate(ConstraintModel constraintModel) {
        def methods = new ArrayList()
        if(constraintModel != null && constraintModel.getOpl() != null) {
            methods.addAll(makeFun(constraintModel.getOpl(),TypeName.get(boolean.class)))
        }
        if(constraintModel != null && constraintModel.getOpr() != null) {
            methods.addAll(makePredicate(constraintModel.getOpr()))
        }
        return methods
    }

    boolean isPredicateLongRunning(ConstraintModel constraintModel) {
        if(constraintModel != null
                && constraintModel.getOpl() != null
                && constraintModel.getOpl().getResource().equals(Constants.RES_INTERNET)) {
            return true
        }
        else if(constraintModel != null
                && constraintModel.getOpr() != null)
            return isPredicateLongRunning(constraintModel.getOpr())
        else
            return false
    }

    List<String> funWidgetRefs(FunModel funModel) {
        List<String> widgetRefs = new ArrayList<>()
        if(funModel != null) {
            if(funModel.getIdValues() != null) widgetRefs.addAll(funModel.getIdValues())
            if(funModel.getFuns() != null )
                funModel.getFuns()
                        .collect {funWidgetRefs(it)}
                        .each {widgetRefs.addAll(it)}
        }
        return widgetRefs
    }

    List<String> predicateWidgetRefs(ConstraintModel constraintModel) {
        List<String> refs = new ArrayList<>()
        if(constraintModel != null) {
            ConstraintModel oprR = constraintModel.getOpr()
            FunModel oprL = constraintModel.getOpl()
            if(oprL.getIdValues() != null) refs.addAll(oprL.getIdValues())
            if(oprR != null) {
                refs.addAll(predicateWidgetRefs(oprR))
            }
        }
        return refs
    }

    FunModel longRunningFunModel(FunModel funModel) {
        if(funModel == null) return funModel
        else if(funModel.getIdValues() == null || funModel.getIdValues().isEmpty()) return funModel
        else {
            List<String> newIds = new ArrayList<>()
            funModel.getIdValues().eachWithIndex { String entry, int i ->
                newIds.add(Constants.ASYNC_BACKGROUND_PARAM_NAME + "[" + i + "]")
            }
            funModel.setIdValues(newIds)
            if(funModel.getFuns() != null && !funModel.getFuns().isEmpty())
                funModel.setFuns(funModel.getFuns().collect{longRunningFunModel(it)})
            return funModel
        }
    }

    ConstraintModel longRunningConstraint(ConstraintModel constraintModel) {
        ConstraintModel oprR = constraintModel.getOpr()
        FunModel oprL = constraintModel.getOpl()
        constraintModel.setOpl(longRunningFunModel(oprL))
        if(oprR != null) constraintModel.setOpr(longRunningConstraint(oprR))
        return constraintModel
    }

    String resolveFunCall(String funCall) {
        String funName = funCall.take(funCall.indexOf("("))
        if(busLogicmethods.collect {it.name}.contains(funName)) {
            return (funName + " is a busLogic")
        }
        else
            return (funName + " is not a busLogic")
    }

    String makePredCall(ConstraintModel constraintModel) {
        if(constraintModel.getOp() == null) {
            return makeFunCall(constraintModel.getOpl())
        }
        else if(constraintModel.getOp() == Constants.TAG_NOT
                && constraintModel.getOpl() != null) {
            return "!(" + makeFunCall(constraintModel.getOpl()) + ")"
        }
        else if(constraintModel.getOp() == Constants.TAG_NOT
                && constraintModel.getOpr() != null) {
            return "!(" + makePredCall(constraintModel.getOpr()) + ")"
        }
        else if(constraintModel.getOp() == Constants.TAG_AND) {
            return "(" + makeFunCall(constraintModel.getOpl()) + " && " +
                    makePredCall(constraintModel.getOpr()) + ")"
        }
        else if(constraintModel.getOp() == Constants.TAG_OR) {
            return "(" + makeFunCall(constraintModel.getOpl()) + " || " +
                    makePredCall(constraintModel.getOpr()) + ")"
        }
        else return ""
    }

    List<MethodSpec> makeActionArgFuns(ArgumentModel argumentModel) {
        List<MethodSpec> methods = new ArrayList<>()
//        TypeVariableName typeVariableName = TypeVariableName.get("T")
//        if(argumentModel.getIdValue() != null) {
//            String mName = getMethodName(argumentModel.getIdValue())
//            MethodSpec method = MethodSpec.methodBuilder(mName)
//                    .addModifiers(Modifier.PRIVATE)
//                    .addTypeVariable(typeVariableName)
//                    .returns(typeVariableName)
//                    .addParameter(int.class,"widgetId")
//                    .addStatement("throw new UnsupportedOperationException()")
//                    .build()
//            methods.add(method)
//        }
        if(argumentModel.getFunValue() != null) {
            methods.addAll(makeFun(argumentModel.getFunValue(),null))
        }
        return methods
    }

    String makeActionArgCall(ArgumentModel arg) {
        def key = arg.getName()
        return "destArgs.putString(\"" + key + "\"," + makeActionArgParam(arg) + ")"
    }

    String makeActionArgParam(ArgumentModel argumentModel) {
        if(argumentModel.getPrimitiveValue() != null) {
            return "\"" + argumentModel.getPrimitiveValue() + "\""
        }
        if(argumentModel.getIdValue() != null) {
            if(isWidgetTypeText(argumentModel.getIdValue()))
                return argumentModel.getIdValue() + "Obj.getText().toString()"
        }
        if(argumentModel.getVarValue() != null)
            return getMethodName(argumentModel.getVarValue()) +
                    "(getArguments(),\"" + argumentModel.getVarValue() + "\")"
        if(argumentModel.getFunValue() != null) {
            return makeFunCall(argumentModel.getFunValue())
        }
        else
            return null
    }

    TypeSpec makeAsyncTask(String className, TypeName parameterType, TypeName resultType, CodeBlock bkgroundCode, CodeBlock postExecCode) {
        ClassName asyncTaskClass = ClassName.get("android.os","AsyncTask")
        TypeName asyncTaskType = ParameterizedTypeName.get(asyncTaskClass,TypeName.get(String.class),TypeName.get(Void.class),resultType)

        ParameterSpec parameterSpec = ParameterSpec.builder(Void.class,"voids").build()
        def params = new ArrayList<ParameterSpec>()
        params.add(parameterSpec)

        MethodSpec backGroundMethod = MethodSpec
                .methodBuilder("doInBackground")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PROTECTED)
                .addParameter(parameterType, Constants.ASYNC_BACKGROUND_PARAM_NAME)
                .varargs(true)
                .returns(resultType)
                .addCode(bkgroundCode)
                .build()

        TypeSpec asyncTask = TypeSpec.classBuilder(className)
                .superclass(asyncTaskType)
                .addModifiers(Modifier.PRIVATE)
                .addMethod(backGroundMethod)
                .build()

        if(postExecCode != null) {
            MethodSpec onPostExecuteMethod = MethodSpec
                    .methodBuilder("onPostExecute")
                    .addAnnotation(Override.class)
                    .addModifiers(Modifier.PROTECTED)
                    .addParameter(resultType, "result")
                    .returns(void)
                    .addCode(postExecCode)
                    .build()
            asyncTask = asyncTask
                    .toBuilder()
                    .addMethod(onPostExecuteMethod)
                    .build()
        }

        return asyncTask
    }

    boolean isWidgetTypeText(String wid) {
        return (widgetTypeMap.get(wid).equals(Constants.WIDGET_NAME_TV)
                || widgetTypeMap.get(wid).equals(Constants.WIDGET_NAME_ET))
    }

    boolean isWidgetWeb(String wid) {
        return (widgetTypeMap.get(wid).equals(Constants.WIDGET_NAME_WV))
    }

    CodeBlock getIntentCode(ActivityModel activityModel) {
        if(activityModel != null) {
            CodeBlock intentCode = CodeBlock
                    .builder()
                    .addStatement("\$T intent = new \$T(\"\$L\")"
                    , ClassName.get("android.content", "Intent")
                    , ClassName.get("android.content", "Intent")
                    , activityModel.getIntentAction())
                    .build()
            if(activityModel.getIntentPackage() != null
                    && activityModel.getIntentPackage() != "") {
                intentCode = intentCode
                        .toBuilder()
                        .addStatement("intent.setPackage(\"\$L\")", activityModel.getIntentPackage())
                        .build()
            }
            return intentCode
        }
        return null
    }

    @TaskAction
    def generateCode() {
        File outdir = new File("${pathToProj}/build/generated/source/buildConfig/debug")
        File outdirbusLogic = new File("${pathToProj}/src/main/java/")
        File navfiles = new File("${pathToSrc}/src/main/res/navigation/")
        File layoutFiles = new File("${pathToSrc}/src/main/res/layout/")
        File manifestFile = new File("${pathToProj}/src/main/AndroidManifest.xml")
        File customRes = new File("${pathToSrc}/src/main/res/xml/custom_res.xml")

        List<String> layoutFilesToParse = new ArrayList<>()
        List<ActivityModel> activities = new ArrayList<>()
        List<RecvModel> receivers = new ArrayList<>()
        Map<String, String> NavGraphStartDestMap = new HashMap<>()
        Map<String, String> activityIdLayoutMap = new HashMap<>()
        Map<String,String> layoutNavGraphMap = new HashMap<>()
        Map<String, String> NavIdStartDestMap = new HashMap<>()

        Map<String, FragmentModel> fragmentModelMap = new HashMap<>()

        ClassName fragment = ClassName.get("androidx.fragment.app","Fragment")
        ClassName bundle = ClassName.get("android.os","Bundle")
        ClassName layoutInflater = ClassName.get("android.view","LayoutInflater")
        ClassName view = ClassName.get("android.view","View")
        ClassName viewGroup = ClassName.get("android.view","ViewGroup")
        ClassName parentRecvClass = ClassName.get("android.content", "BroadcastReceiver")
        ClassName contextClass = ClassName.get("android.content", "Context")
        ClassName intentClass = ClassName.get("android.content", "Intent")
        TypeVariableName genericType = TypeVariableName.get("T")

        def manifest = new XmlParser().parse(manifestFile)
        packageName = manifest.attribute("package")
        navfiles.eachFile {
            def root = new XmlParser().parse(it)
            NavGraphStartDestMap.put(it.getName().split(".xml")[0]
                    ,root.attribute(NavParser.appNS.startDestination)
                    .toString().split("/")[1])
            root.activity.each { groovy.util.Node activityNode ->
                def layoutId = activityNode.attribute(NavParser.toolsNS.layout)
                if(layoutId.toString().contains("/"))
                    layoutFilesToParse.add(layoutId.toString().split("/")[1] + ".xml")
                def activityId = activityNode.attribute(NavParser.androidNS.id)
                if(activityId.toString().contains("/")) {
                    String intentAction = activityNode.attribute(NavParser.appNS.action)
                    String intentPackage = activityNode.attribute(NavParser.appNS.targetPackage)
                    ActivityModel activityModel = new ActivityModel(activityId.toString().split("/")[1])
                    if(layoutId.toString().contains("/")) activityModel.setLayoutId(layoutId.toString().split("/")[1])
                    activityModel.setIntentAction(intentAction)
                    activityModel.setIntentPackage(intentPackage)
                    activities.add(activityModel)
                }
                if(activityId.toString().contains("/") && layoutId.toString().contains("/"))
                    activityIdLayoutMap.put(activityId.toString().split("/")[1]
                            ,layoutId.toString().split("/")[1])
            }

            root.navigation.each { groovy.util.Node navNode ->
                def navId = navNode.attribute(NavParser.androidNS.id)
                def startDestId = navNode.attribute(NavParser.appNS.startDestination)
                if(navId.toString().contains("/")
                        && startDestId.toString().contains("/")) {
                    NavIdStartDestMap.put(navId.toString().split("/")[1]
                            ,startDestId.toString().split("/")[1])
                }
            }
        }

        layoutFiles.eachFile {
            def root = new XmlParser().parse(it)
            root.children().each {
                if(it.attribute(NavParser.androidNS.id) != null) {
                    widgetTypeMap.put(it.attribute(NavParser.androidNS.id).split("/")[1],it.name())
                }
            }
            if(layoutFilesToParse.contains(it.getName())) {
                def navGraph = root.depthFirst().find {
                    it.name() == Constants.TAG_FRAGMENT
                }.attribute(NavParser.appNS.navGraph)

                if(navGraph.toString().contains("/"))
                    layoutNavGraphMap.put(it.getName().split(".xml")[0]
                            ,navGraph.toString().split("/")[1])
            }

        }

        navfiles.eachFile {
            def root = new XmlParser().parse(it)

            def fragNodes = root.depthFirst().findAll {
                it.name() == Constants.TAG_FRAGMENT
            }
            fragNodes.each { groovy.util.Node node ->
                def id = (node.attribute(NavParser.androidNS.id) != null
                        && node.attribute(NavParser.androidNS.id).toString().contains("/")) ?
                        node.attribute(NavParser.androidNS.id).toString().split('/')[1] : ""
                def parsedModel = NavParser.parseFragmentNode(node)
                if(parsedModel != null)
                    fragmentModelMap.put(id,parsedModel)
            }

            fragNodes.each { groovy.util.Node node ->
                node.action.each { groovy.util.Node anode ->
                    def destId = (anode.attribute(NavParser.appNS.destination) != null
                            && anode.attribute(NavParser.appNS.destination).toString().contains("/")) ?
                            anode.attribute(NavParser.appNS.destination).toString().split('/')[1] : ""
                    if(fragmentModelMap.get(destId) != null) {
                        fragmentModelMap.put(destId
                                ,updateFragmenModel(anode,fragmentModelMap.get(destId)))
                    }
                    else if(NavIdStartDestMap.get(destId) != null) {
                        fragmentModelMap.put(NavIdStartDestMap.get(destId)
                                ,updateFragmenModel(anode,fragmentModelMap.get(NavIdStartDestMap.get(destId))))
                    }
                    else if(activityIdLayoutMap.get(destId) != null) {
                        def key = NavGraphStartDestMap
                                .get(layoutNavGraphMap
                                .get(activityIdLayoutMap.get(destId)))
                        fragmentModelMap.put(key
                                ,updateFragmenModel(anode,fragmentModelMap.get(key)))
                    }
                }
            }
        }
        if(customRes != null && customRes.exists()) {
            new XmlParser().parse(customRes)
                    .depthFirst()
                    .findAll() {
                it.name() == Constants.TAG_RECV }
            .each { groovy.util.Node node -> receivers.add(NavParser.parseRecvNode(node)) }

            manifest.depthFirst().findAll {
                it.name() == Constants.MANIFEST_TAG_PERM
            }.each {
                manifest.remove(it)
            }
        }

        receivers.each { RecvModel recvModel ->
            busLogicmethods = new ArrayList<>()
            List<MethodSpec> methods = new ArrayList<>()
            String classNm = recvModel.getId().substring(0,1).toUpperCase() +
                    recvModel.getId().substring(1) + Constants.BRDCST_RCVR
            String protLevel = recvModel.getProtectionLevel()
            String permTyp = recvModel.getPermType()
            String permNm = recvModel.getPermName()
            List<EventModel> events = recvModel.getEvents()
            CodeBlock.Builder eventStmtbuilder = CodeBlock.builder()
            events.each {EventModel eventModel ->
                methods.addAll(makeFun(eventModel.getFunModel(),TypeName.get(void.class)))
                String eventNm = eventModel.getName()
                eventStmtbuilder.beginControlFlow("if(intent.getAction().equals(\"\$L\"))", eventNm)
                eventStmtbuilder.addStatement(makeFunCall(eventModel.getFunModel()))
                eventStmtbuilder.endControlFlow()
            }

            MethodSpec onRecvMethod = MethodSpec.methodBuilder("onReceive")
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(contextClass, "context")
                    .addParameter(intentClass, "intent")
                    .addStatement("this.context = context")
                    .addCode(eventStmtbuilder.build())
                    .returns(void.class)
                    .addAnnotation(Override.class)
                    .build()
            MethodSpec getContexMethod = MethodSpec.methodBuilder("getContext")
                    .addModifiers(Modifier.PUBLIC)
                    .returns(contextClass)
                    .addStatement("return this.context")
                    .build()
            TypeSpec recvClass = TypeSpec.classBuilder(classNm).addModifiers(Modifier.PUBLIC)
                    .superclass(parentRecvClass)
                    .addField(contextClass, "context", Modifier.PRIVATE)
                    .addMethod(getContexMethod)
                    .addMethod(onRecvMethod)
                    .build()
            if(!methods.isEmpty()) {
                methodEnv = new ArrayList<>()
                methods.each {
                    if(!methodExists(it)) {
                        methodEnv.add(it)
                        recvClass = recvClass.toBuilder().addMethod(it).build()
                    }
                }
            }
            if(!busLogicmethods.isEmpty()) {
                methodEnv = new ArrayList<>()
                String busLogicPackage = packageName + Constants.BUSLOGIC_DIR
                TypeSpec recvClassBusLogic = TypeSpec.classBuilder(classNm + Constants.BUSLOGIC)
                        .addModifiers(Modifier.PUBLIC)
                        .build()

                busLogicmethods.each {
                    if(!methodExists(it)) {
                        methodEnv.add(it)
                        recvClassBusLogic = recvClassBusLogic.toBuilder().addMethod(it).build()
                    }
                }

                FieldSpec busLogicObj = FieldSpec
                        .builder(ClassName.get(busLogicPackage, classNm + Constants.BUSLOGIC), Constants.OBJBUSLOGIC, Modifier.PRIVATE)
                        .initializer("new \$T()", ClassName.get(busLogicPackage, classNm + Constants.BUSLOGIC))
                        .build()
                recvClass = recvClass.toBuilder().addField(busLogicObj).build()

                JavaFile busLogicFile = JavaFile.builder(busLogicPackage,recvClassBusLogic).build()
                busLogicFile.writeTo(outdirbusLogic)

                JavaFile recvFile = JavaFile.builder(packageName.toString(),recvClass).build()
                recvFile.writeTo(outdir)

            }
            JavaFile recvFile = JavaFile.builder(packageName.toString(),recvClass).build()
            recvFile.writeTo(outdir)

            /*
            Add receiver to manifest
             */

            if(permTyp == Constants.PERM_TYPE_CUSTOM
                    && permNm != null && protLevel != null
                    && manifest.permission.find {it.attribute(NavParser.androidNS.name) == permNm} == null) {
                Map attr = new HashMap()
                attr.put(new groovy.xml.QName("http://schemas.android.com/apk/res/android","name","android"),permNm)
                if(protLevel == Constants.PERM_PROTECT_ALL)
                    attr.put(new groovy.xml.QName("http://schemas.android.com/apk/res/android","protectionLevel","android"),"normal")
                else if(protLevel == Constants.PERM_PROTECT_USER)
                    attr.put(new groovy.xml.QName("http://schemas.android.com/apk/res/android","protectionLevel","android"),"dangerous")
                else if(protLevel == Constants.PERM_PROTECT_OWN)
                    attr.put(new groovy.xml.QName("http://schemas.android.com/apk/res/android","protectionLevel","android"),"signature")
                manifest.appendNode("permission", attr,"")
            }

            def appNodes = manifest.depthFirst().findAll {
                it.name() == Constants.MANIFEST_TAG_APP
            }

            appNodes.each {
                manifest.remove(it)
            }

            appNodes.each { groovy.util.Node n ->
                n.depthFirst().findAll {
                    it.name() == Constants.MANIFEST_TAG_RECV && it.attribute(NavParser.androidNS.name) == packageName + "." + classNm
                }.each {n.remove(it)}
                String export = "true"
                if(protLevel == "" || protLevel == null) {
                    export = "false"
                }
                groovy.util.Node recvNode = new groovy.util.Node(null,"receiver", ["android:name":packageName + "." + classNm,
                                                                                   "android:exported":export])
                if(permNm != null) {
                    recvNode = new groovy.util.Node(null,"receiver", ["android:name":packageName + "." + classNm,
                                                                      "android:permission":(permNm),
                                                                      "android:exported":export])
                }
                if(recvModel.getEvents() != null) {
                    recvModel.getEvents().each {
                        groovy.util.Node ifNode = new groovy.util.Node(null,"intent-filter")
                        if(it.getPriority() != null) {
                            ifNode = new groovy.util.Node(null,"intent-filter", ["android:priority":it.getPriority()])
                        }
                        ifNode.appendNode("action",["android:name":it.getName()])
                        recvNode.append(ifNode)
                    }
                }
                n.append(recvNode)
                manifest.append(n)
            }

            manifestFile.withPrintWriter {outWriter ->
                outWriter.println(XmlUtil.serialize(manifest))
            }
//            def manifestRecv = manifest.depthFirst().findAll {
//                it.name() == Constants.MANIFEST_TAG_RECV && it.attribute(NavParser.androidNS.name) == packageName + "." + classNm
//            }
//            manifest.appendNode("permission", ["name":packageName + ".perm1"],"")
//            if(manifestRecv.size() == 0) {
//                if(protLevel == null || protLevel == "")
//                    manifest.application[0].appendNode("receiver",["android:name":packageName + "." + classNm, "android:exported":"false"],"")
//                else
//                    manifest.application[0].appendNode("receiver",["android:name":packageName + "." + classNm, "android:exported":"true"],"")
//                println manifest
//                manifestFile.withPrintWriter {outWriter ->
//                    outWriter.println(XmlUtil.serialize(manifest))
//                }
//            }

        }

        fragmentModelMap.keySet().each { String fragId ->
            methodEnv = new ArrayList<>()
            busLogicmethods = new ArrayList<>()
            resPermRequests = new ArrayList<>()
            FragmentModel fragmentModel = fragmentModelMap.get(fragId)
            def layoutId = fragmentModel.getLayoutId()
            def fragClassName = getClassName(layoutId)

            MethodSpec onCreateView = MethodSpec.methodBuilder("onCreateView")
                    .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                    .addParameter(layoutInflater,"inflater")
                    .addParameter(viewGroup,"container")
                    .addParameter(bundle,"savedInstanceState")
                    .returns(view)
                    .addStatement("return inflater.inflate(R.layout." + layoutId + ", container, false)")
                    .addAnnotation(Override.class)
                    .build()

            MethodSpec onViewCreated = MethodSpec.methodBuilder("onViewCreated")
                    .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                    .addParameter(view,"view")
                    .addParameter(bundle,"savedInstanceState")
                    .returns(void.class)
                    .addAnnotation(Override.class)
                    .addStatement("super.onViewCreated(view, savedInstanceState)")
                    .build()

            TypeSpec fragClass = TypeSpec.classBuilder(fragClassName)
                    .addModifiers(Modifier.PUBLIC)
                    .superclass(fragment)
                    .addMethod(onCreateView)
                    .build()

            if(fragmentModel.getInActionArgs() != null) {
                fragmentModel.getInActionArgs().each { ArgumentModel argumentModel ->
                    String argName = argumentModel.getName()
                    MethodSpec methodSpec = MethodSpec.methodBuilder(getMethodName(argName))
                            .addModifiers(Modifier.PRIVATE)
                            .returns(String.class)
                            .addParameter(bundle,"bundle")
                            .addParameter(String.class,argName)
                            .beginControlFlow("if (bundle != null)")
                            .addStatement("return bundle.getString(" + argName + ")")
                            .endControlFlow()
                            .addStatement("return null")
                            .build()
                    if(!methodExists(methodSpec)) {
                        methodEnv.add(methodSpec)
                        fragClass = fragClass.toBuilder().addMethod(methodSpec).build()
                    }
                }
            }

            if(fragmentModel.getArgs() != null) {
                fragmentModel.getArgs().each { ArgumentModel argumentModel ->
                    String argName = argumentModel.getName()
                    MethodSpec methodSpec = MethodSpec.methodBuilder(getMethodName(argName))
                            .addModifiers(Modifier.PRIVATE)
                            .returns(String.class)
                            .addParameter(bundle,"bundle")
                            .addParameter(String.class,argName)
                            .beginControlFlow("if (bundle != null)")
                            .addStatement("return bundle.getString(" + argName + ")")
                            .endControlFlow()
                            .addStatement("return null")
                            .build()
                    if(!methodExists(methodSpec)) {
                        methodEnv.add(methodSpec)
                        fragClass = fragClass.toBuilder().addMethod(methodSpec).build()
                    }
                }
            }
            if(fragmentModel.getWidgets() != null) {
                fragmentModel.getWidgets().each { WidgetModel widgetModel ->

                    ClassName widgetType = ClassName
                            .get("android.widget"
                            ,widgetTypeMap.get(widgetModel.getWid()).toString())

                    if(isWidgetWeb(widgetModel.getWid())) {
                        widgetType = ClassName
                                .get("android.webkit"
                                ,widgetTypeMap.get(widgetModel.getWid()).toString())
                    }
                    String widgetVar = widgetModel.getWid() + "Obj"
                    fragClass = fragClass
                            .toBuilder()
                            .addField(widgetType,widgetVar,Modifier.PRIVATE)
                            .build()
                    onViewCreated = onViewCreated.toBuilder()
                            .addStatement(widgetVar + " = (\$T) view.findViewById(R.id." + widgetModel.getWid() + ")",widgetType)
                            .build()
                    if(isWidgetWeb(widgetModel.getWid()) && widgetModel.getFunModel().isAllowJS()) {
                        onViewCreated = onViewCreated.toBuilder()
                                .addStatement(widgetVar + ".getSettings().setJavaScriptEnabled(true)")
                                .build()
                    }

                    if(widgetModel.getFunModel() != null && widgetModel.getFunModel().getWhiteListModel() != null) {
                        List<String> patterns = widgetModel.getFunModel().getWhiteListModel().getPatterns()
                        CodeBlock.Builder codeBlock = CodeBlock.builder()
                        patterns.each {
                            if(it.startsWith(Constants.PREFIX_INTENT)) {
                                MethodSpec urlIntentHandleMethod = MethodSpec
                                        .methodBuilder(Constants.URL_INTENT_HANDLE_METHOD)
                                        .addParameter(ClassName.get("android.content", "Intent"), "intent")
                                        .addModifiers(Modifier.PUBLIC)
                                        .returns(void.class)
                                        .addStatement("throw new UnsupportedOperationException()")
                                        .build()
                                busLogicmethods.add(urlIntentHandleMethod)
                                try {
                                    String action = it.split("/")[1]
                                    String app = it.split("/")[2]
                                    codeBlock.beginControlFlow("if(url.contains(\"intent\"))")
                                            .beginControlFlow("try")
                                            .addStatement("\$T intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME)", ClassName.get("android.content", "Intent"))
                                            .beginControlFlow("if(intent != null && intent.getAction().equals(\"\$L\") && intent.getPackage().equals(\"\$L\"))", action, app)
                                            .addStatement("\$T \$L = new \$T()", ClassName.get(packageName + Constants.BUSLOGIC_DIR, fragClassName + Constants.BUSLOGIC), Constants.OBJBUSLOGIC_INNER, ClassName.get(packageName + Constants.BUSLOGIC_DIR, fragClassName + Constants.BUSLOGIC))
                                            .addStatement(Constants.OBJBUSLOGIC_INNER + "." + Constants.URL_INTENT_HANDLE_METHOD + "(intent)")
                                            .addStatement("return true")
                                            .endControlFlow()
                                            .endControlFlow()
                                            .beginControlFlow("catch(\$T e)", URISyntaxException.class)
                                            .addStatement("e.printStackTrace()")
                                            .addStatement("return true")
                                            .endControlFlow()
                                            .endControlFlow()
                                }
                                catch(IndexOutOfBoundsException e) {
                                    e.printStackTrace()
                                    println("malformed intent pattern case in whitelist found")
                                }
                            }
                            else {
                                codeBlock.beginControlFlow("if(url.matches(\"\$L\"))", it)
                                        .addStatement("return false")
                                        .endControlFlow()

                            }
                        }
                        MethodSpec overrideUrlMethod1 = MethodSpec
                                .methodBuilder("shouldOverrideUrlLoading")
                                .addAnnotation(Override.class)
                                .addModifiers(Modifier.PUBLIC)
                                .addParameter(ClassName.get("android.webkit", "WebView"), "view")
                                .addParameter(String.class, "url")
                                .returns(boolean.class)
                                .addCode(codeBlock.build())
                                .addStatement("view.loadData(\"intent in URL is not trusted\", \"text/css\", \"UTF-8\")")
                                .addStatement("return true")
                                .build()
                        MethodSpec overrideUrlMethod2 = MethodSpec
                                .methodBuilder("shouldOverrideUrlLoading")
                                .addAnnotation(Override.class)
                                .addModifiers(Modifier.PUBLIC)
                                .addParameter(ClassName.get("android.webkit", "WebView"), "view")
                                .addParameter(ClassName.get("android.webkit", "WebResourceRequest"), "request")
                                .returns(boolean.class)
                                .addStatement("\$T url = request.getUrl().toString()", String.class)
                                .addCode(codeBlock.build())
                                .addStatement("view.loadData(\"intent in URL is not trusted\", \"text/css\", \"UTF-8\")")
                                .addStatement("return true")
                                .build()

                        if(patterns.findAll{!it.startsWith(Constants.PREFIX_INTENT)}.size() > 0) {
                            CodeBlock.Builder codeBlock1 = CodeBlock.builder()
                            patterns.findAll{!it.startsWith(Constants.PREFIX_INTENT)}.each {
                                codeBlock1.beginControlFlow("if(url.matches(\"\$L\"))", it)
                                        .addStatement("return null")
                                        .endControlFlow()
                            }

                            MethodSpec interceptReqMethod = MethodSpec
                                    .methodBuilder("shouldInterceptRequest")
                                    .addAnnotation(Override.class)
                                    .addModifiers(Modifier.PUBLIC)
                                    .addParameter(ClassName.get("android.webkit", "WebView"), "view")
                                    .addParameter(ClassName.get("android.webkit", "WebResourceRequest"), "request")
                                    .returns(ClassName.get("android.webkit", "WebResourceResponse"))
                                    .addStatement("\$T url = request.getUrl().toString()", String.class)
                                    .addCode(codeBlock1.build())
                                    .addStatement("return new WebResourceResponse(\"text/css\", \"UTF-8\", new \$T(\"alert(\'Abort! The webpage contains untrusted content\')\".getBytes()))", ByteArrayInputStream.class)
                                    .build()

                            onViewCreated = onViewCreated.toBuilder()
                                    .addStatement(widgetVar + ".setWebViewClient(\$L)"
                                    , TypeSpec.anonymousClassBuilder("")
                                    .addSuperinterface(ClassName.get("android.webkit", "WebViewClient"))
                                    .addMethod(overrideUrlMethod1)
                                    .addMethod(overrideUrlMethod2)
                                    .addMethod(interceptReqMethod)
                                    .build())
                                    .build()
                        }
                        else {
                            onViewCreated = onViewCreated.toBuilder()
                                    .addStatement(widgetVar + ".setWebViewClient(\$L)"
                                    , TypeSpec.anonymousClassBuilder("")
                                    .addSuperinterface(ClassName.get("android.webkit", "WebViewClient"))
                                    .addMethod(overrideUrlMethod1)
                                    .addMethod(overrideUrlMethod2)
                                    .build())
                                    .build()
                        }
                    }

                    if (widgetModel.getFunModel() != null
                            || widgetModel.getVariableVal() != null
                            || widgetModel.getPrimitiveVal() != null) {

                        if (widgetModel.getFunModel() != null) {
                            makeFun(widgetModel.getFunModel(), null).each {
                                if (!methodExists(it)) {
                                    methodEnv.add(it)
                                    fragClass = fragClass.toBuilder()
                                            .addMethod(it).build()
                                }
                            }
                        }

                        String initMethod = widgetVar + ".setText("
                        if(isWidgetWeb(widgetModel.getWid())) {
                            initMethod = widgetVar + ".loadUrl("
                        }
                        if (widgetModel.getPrimitiveVal() != null) {
                            StringBuffer initMethodbf = new StringBuffer(initMethod)
                            initMethodbf.append("\"" + widgetModel.getPrimitiveVal() + "\"")
                            initMethodbf.append(")")
                            onViewCreated = onViewCreated
                                    .toBuilder()
                                    .addStatement(initMethodbf.toString())
                                    .build()
                        }
                        if (widgetModel.getVariableVal() != null) {
                            String tmp = getMethodName(widgetModel.getVariableVal()) +
                                    "(getArguments(),\"" +
                                    widgetModel.getVariableVal() + "\")"
                            String varName = widgetModel.getVariableVal() + "Var"
                            onViewCreated = onViewCreated
                                    .toBuilder()
                                    .addStatement("\$T \$L = \$L",String.class,varName,tmp)
                                    .beginControlFlow("if (\$L != null)",varName)
                                    .addStatement(initMethod + varName + ")")
                                    .endControlFlow()
                                    .build()
                        }
                        if (widgetModel.getFunModel() != null) {
                            String varName = widgetModel.getFunModel().getName() + "Var"
                            String classNm = widgetModel.getFunModel().getName() + "Task"
                            if(UsefulMethods.isFunRequiresAysnc(widgetModel.getFunModel())) {
                                List<String> widgetRefs = funWidgetRefs(widgetModel.getFunModel())
                                if(widgetRefs != null && !widgetRefs.isEmpty()) {
                                    onViewCreated = onViewCreated
                                            .toBuilder()
                                            .addStatement("new \$L().execute(\$L)"
                                            , classNm
                                            , widgetRefs.findAll { isWidgetTypeText(it)}
                                            .collect {it + "Obj.getText().toString()"}
                                            .join(","))
                                            .build()
                                }
                                else {
                                    onViewCreated = onViewCreated
                                            .toBuilder()
                                            .addStatement("new \$L().execute()", classNm)
                                            .build()
                                }
                                CodeBlock bkgrndCodeBlock = CodeBlock
                                        .builder()
//                                            .addStatement("\$T \$L = \$L", String.class
//                                                , varName
//                                                , makeFunCall(widgetModel.getFunModel()))
                                        .addStatement("return \$L", makeFunCall(longRunningFunModel(widgetModel.getFunModel())))
                                        .build()

                                CodeBlock resultCodeBlock = CodeBlock
                                        .builder()
                                        .beginControlFlow("if (result != null)")
                                        .addStatement(initMethod + "result)")
                                        .endControlFlow()
                                        .build()
                                fragClass = fragClass.toBuilder()
                                        .addType(makeAsyncTask(classNm
                                        , TypeName.get(String[].class)
                                        , TypeName.get(String.class)
                                        , bkgrndCodeBlock
                                        , resultCodeBlock))
                                        .build()
                            }
                            else {
                                onViewCreated = onViewCreated
                                        .toBuilder()
                                        .addStatement("\$T \$L = \$L"
                                        ,String.class
                                        ,varName
                                        ,makeFunCall(widgetModel.getFunModel()))
                                        .beginControlFlow("if (\$L != null)", varName)
                                        .addStatement(initMethod + varName + ")")
                                        .endControlFlow()
                                        .build()
                            }
                        }
                    }
                }
            }

            if(fragmentModel.getOutActions() != null) {
                List<String> existingListeners = new ArrayList<>()
                Map<String,List<String>> gestureActionIdMap = new HashMap<>()
                Map<String, String> constraintCallMap = new HashMap<>()
                Map<String, List<ArgumentModel>> argCallMap = new HashMap<>()
                fragmentModel.getOutActions().each { ActionModel action ->
                    if(action.getWidgetOn() != null && action.getGesture() != null) {
                        def key = action.getWidgetOn() + "#" + action.getGesture()
                        if(gestureActionIdMap.get(key) != null) {
                            def tmpList = gestureActionIdMap.get(key)
                            tmpList.add(action.getActionId())
                            gestureActionIdMap.put(key,tmpList)
                        }
                        else {
                            def tmpList = new ArrayList()
                            tmpList.add(action.getActionId())
                            gestureActionIdMap.put(key,tmpList)
                        }
                    }
                    if(action.getConstraintModel() != null) {
                        constraintCallMap.put(action.getActionId()
                                ,makePredCall(action.getConstraintModel()))
                    }
                    if(action.getArgs() != null) {
                        argCallMap.put(action.getActionId()
                                ,action.getArgs())
                    }
                }
                fragmentModel.getOutActions().each { ActionModel action ->
                    ActivityModel destActivityModel = activities.find {it.activityId == action.getDestId() && it.layoutId == null}
                    if(action.getWidgetOn() != null && action.getGesture() != null) {
                        def widgetGesture = action.getWidgetOn() + "#" + action.getGesture()
                        if(!existingListeners
                                .contains(widgetGesture)) {
                            onViewCreated = onViewCreated.toBuilder()
                                    .addStatement(makeListener(action.getWidgetOn()
                                    ,action.getGesture()
                                    ,constraintCallMap
                                    ,argCallMap
                                    ,gestureActionIdMap.get(widgetGesture)
                                    ,isPredicateLongRunning(action.getConstraintModel())
                                    ,predicateWidgetRefs(action.getConstraintModel())
                                    ,destActivityModel))
                                    .build()
                            existingListeners.add(widgetGesture)
                        }
                    }

                    if(action.getConstraintModel() != null) {
                        makePredicate(action.getConstraintModel()).each {
                            if(!methodExists(it)) {
                                methodEnv.add(it)
                                fragClass = fragClass.toBuilder().addMethod(it).build()
                            }
                        }
                        if(isPredicateLongRunning(action.getConstraintModel())) {
                            if(action.getDestId() != fragId) {
                                CodeBlock postExecBlock = CodeBlock.builder()
                                        .beginControlFlow("if (result)")
                                        .build()

                                if(action.getArgs() != null) {
                                    postExecBlock = postExecBlock
                                            .toBuilder()
                                            .addStatement("\$T destArgs = new \$T()",bundle,bundle)
                                            .build()
                                    action.getArgs().each { ArgumentModel argumentModel ->
                                        postExecBlock = postExecBlock
                                                .toBuilder()
                                                .addStatement(makeActionArgCall(argumentModel))
                                                .build()
                                    }
                                    if(getIntentCode(destActivityModel) != null) {
                                        postExecBlock = postExecBlock
                                                .toBuilder()
                                                .addStatement(getIntentCode(destActivityModel))
                                                .addStatement("intent.putExtras(destArgs)")
                                                .addStatement("startActivity(intent)")
                                                .endControlFlow()
                                                .build()
                                    }
                                    else {
                                        postExecBlock = postExecBlock
                                                .toBuilder()
                                                .addStatement("\$T.findNavController(getView()).navigate(R.id."
                                                + action.getActionId() + ", destArgs, null)"
                                                , ClassName.get("androidx.navigation","Navigation"))
                                                .endControlFlow()
                                                .build()
                                    }


                                }
                                else {
                                    if(getIntentCode(destActivityModel) != null) {
                                        postExecBlock = postExecBlock
                                                .toBuilder()
                                                .addStatement(getIntentCode(destActivityModel))
                                                .addStatement("destArgs.putExtras(destArgs)")
                                                .addStatement("startActivity(intent)")
                                                .endControlFlow()
                                                .build()
                                    }
                                    else {
                                        postExecBlock = postExecBlock
                                                .toBuilder()
                                                .addStatement("\$T.findNavController(getView()).navigate(R.id."
                                                + action.getActionId() + ", null, null)"
                                                , ClassName.get("androidx.navigation" ,"Navigation"))
                                                .endControlFlow()
                                                .build()
                                    }
                                }
                                fragClass = fragClass
                                        .toBuilder()
                                        .addType(makeAsyncTask(action.getActionId() + "Task"
                                        , TypeName.get(String[].class)
                                        , TypeName.get(Boolean.class)
                                        , CodeBlock
                                        .builder()
                                        .addStatement("return (" + makePredCall(longRunningConstraint(action.getConstraintModel())) + ")")
                                        .build()
                                        , postExecBlock))
                                        .build()
                            }
                            else {
                                if(action.getArgs() != null && !action.getArgs().isEmpty()) {
                                    CodeBlock postExecBlock = CodeBlock.builder()
                                            .beginControlFlow("if (result)")
                                            .build()
                                    postExecBlock = postExecBlock
                                            .builder()
                                            .addStatement("\$T destArgs = new \$T()",bundle,bundle)
                                            .build()
                                    action.getArgs().each { ArgumentModel argumentModel ->
                                        postExecBlock = postExecBlock
                                                .toBuilder()
                                                .addStatement(makeActionArgCall(argumentModel))
                                                .build()
                                    }
                                    postExecBlock = postExecBlock
                                            .toBuilder()
                                            .addStatement("setArguments(destArgs)")
                                            .endControlFlow()
                                            .build()
                                    fragClass = fragClass
                                            .toBuilder()
                                            .addType(makeAsyncTask(action.getActionId() + "Task"
                                            , TypeName.get(String[].class)
                                            , TypeName.get(Boolean.class)
                                            , CodeBlock
                                            .builder()
                                            .addStatement("return (" + makePredCall(longRunningConstraint(action.getConstraintModel())) + ")")
                                            .build()
                                            , postExecBlock))
                                            .build()
                                }
                                else {
                                    fragClass = fragClass
                                            .toBuilder()
                                            .addType(makeAsyncTask(action.getActionId() + "Task"
                                            , TypeName.get(String[].class)
                                            , TypeName.get(Boolean.class)
                                            , CodeBlock
                                            .builder()
                                            .addStatement("return (" + makePredCall(longRunningConstraint(action.getConstraintModel())) + ")")
                                            .build()
                                            , null))
                                            .build()
                                }
                            }
                        }
                        else if(action.getWidgetOn() == null || action.getGesture() == null) {
                            if(fragId != action.getDestId()) {
                                onViewCreated = onViewCreated
                                        .toBuilder()
                                        .beginControlFlow("if (\$L)",makePredCall(action.getConstraintModel()))
                                        .build()
                                if(action.getArgs() != null && !action.getArgs().isEmpty()) {
                                    onViewCreated = onViewCreated
                                            .toBuilder()
                                            .addStatement("\$T destArgs = new \$T()",bundle,bundle)
                                            .build()
                                    action.getArgs().each { ArgumentModel argumentModel ->
                                        onViewCreated = onViewCreated
                                                .toBuilder()
                                                .addStatement(makeActionArgCall(argumentModel))
                                                .build()
                                    }
                                    if(getIntentCode(destActivityModel) != null) {
                                        onViewCreated = onViewCreated
                                                .toBuilder()
                                                .addStatement(getIntentCode(destActivityModel))
                                                .addStatement("intent.putExtras(destArgs)")
                                                .addStatement("startActivity(intent)")
                                                .endControlFlow()
                                                .build()
                                    }
                                    else {
                                        onViewCreated = onViewCreated
                                                .toBuilder()
                                                .addStatement("\$T.findNavController(getView()).navigate(R.id."
                                                + action.getActionId() + ", destArgs, null)"
                                                , ClassName.get("androidx.navigation","Navigation"))
                                                .endControlFlow()
                                                .build()
                                    }
                                }
                                else {
                                    if(getIntentCode(destActivityModel) != null) {
                                        onViewCreated = onViewCreated
                                                .toBuilder()
                                                .addStatement(getIntentCode(destActivityModel))
                                                .addStatement("startActivity(intent)")
                                                .endControlFlow()
                                                .build()
                                    }
                                    else {
                                        onViewCreated = onViewCreated
                                                .toBuilder()
                                                .addStatement("\$T.findNavController(getView()).navigate(R.id."
                                                + action.getActionId() + ", null, null)"
                                                , ClassName.get("androidx.navigation" ,"Navigation"))
                                                .endControlFlow()
                                                .build()
                                    }
                                }
                            }
                            else {
                                if(action.getArgs() != null && !action.getArgs().isEmpty()) {
                                    onViewCreated = onViewCreated
                                            .toBuilder()
                                            .beginControlFlow("if (\$L)",makePredCall(action.getConstraintModel()))
                                            .build()
                                    onViewCreated = onViewCreated
                                            .toBuilder()
                                            .addStatement("\$T destArgs = new \$T()",bundle,bundle)
                                            .build()
                                    action.getArgs().each { ArgumentModel argumentModel ->
                                        onViewCreated = onViewCreated
                                                .toBuilder()
                                                .addStatement(makeActionArgCall(argumentModel))
                                                .build()
                                    }
                                    onViewCreated = onViewCreated
                                            .toBuilder()
                                            .addStatement("setArguments(destArgs)")
                                            .endControlFlow()
                                            .build()
                                }
                                else {
                                    onViewCreated = onViewCreated
                                            .toBuilder()
                                            .addStatement(makePredCall(action.getConstraintModel()))
                                            .build()
                                }
                            }
                        }
                    }

                    if(action.getArgs() != null) {
                        action.getArgs().each {
                            makeActionArgFuns(it).each {
                                if(!methodExists(it)) {
                                    methodEnv.add(it)
                                    fragClass = fragClass.toBuilder().addMethod(it).build()
                                }
                            }
                        }
                    }
                }
            }

            /*
            Chaney :
                needs to write the logic for permission request handler in the empty if block
             */
            if(!resPermRequests.isEmpty()) {

            }

            if(!busLogicmethods.isEmpty()) {
                methodEnv = new ArrayList<>()
                String busLogicPackage = packageName + Constants.BUSLOGIC_DIR
                TypeSpec fragClassBusLogic = TypeSpec.classBuilder(fragClassName + Constants.BUSLOGIC)
                        .addModifiers(Modifier.PUBLIC)
                        .build()

                busLogicmethods.each {
                    if(!methodExists(it)) {
                        methodEnv.add(it)
                        fragClassBusLogic = fragClassBusLogic.toBuilder().addMethod(it).build()
                    }
                }

                FieldSpec busLogicObj = FieldSpec
                        .builder(ClassName.get(busLogicPackage, fragClassName + Constants.BUSLOGIC), Constants.OBJBUSLOGIC, Modifier.PRIVATE)
                        .initializer("new \$T()", ClassName.get(busLogicPackage, fragClassName + Constants.BUSLOGIC))
                        .build()
                fragClass = fragClass
                        .toBuilder()
                        .addField(busLogicObj)
                        .addMethod(onViewCreated)
                        .build()

//                fragClassBusLogic = fragClassBusLogic
//                        .toBuilder()
//                        .superclass(ClassName.get(packageName.toString(), fragClassName))
//                        .build()

                JavaFile busLogicFile = JavaFile.builder(busLogicPackage,fragClassBusLogic).build()
                busLogicFile.writeTo(outdirbusLogic)

                JavaFile fragFile = JavaFile.builder(packageName.toString(),fragClass).build()
                fragFile.writeTo(outdir)

                println("Add " + packageName + "." + fragClassName + " to navigation file")
            }
            else {
                fragClass = fragClass
                        .toBuilder()
                        .addMethod(onViewCreated)
                        .build()

                JavaFile fragFile = JavaFile.builder(packageName.toString(),fragClass).build()
                fragFile.writeTo(outdir)

                println("Add " + packageName + "." + fragClassName + " to navigation file")
            }
//            File outFile = new File("${pathToProj}/src/main/java/sema/apps/mockuber/" + fragClassName + ".java")
//            outFile.setWritable(false,false) ? println("success") : println("failed")
        }
    }
}
