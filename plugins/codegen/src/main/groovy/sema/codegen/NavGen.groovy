package sema.codegen

import com.squareup.javapoet.*
import org.gradle.api.Plugin
import org.gradle.api.Project
import sema.codegen.model.ActionModel
import sema.codegen.model.ArgumentModel
import sema.codegen.model.ConstraintModel
import sema.codegen.model.FragmentModel
import sema.codegen.model.FunModel
import sema.codegen.model.WidgetModel
import sema.codegen.util.Constants
import sema.codegen.util.NavParser

import javax.lang.model.element.Modifier

/**
 * Created by Joy on 7/23/19.
 */
class NavGen implements Plugin<Project> {

    private List<MethodSpec> methodEnv = null
    @Override
    void apply(Project project) {
        File outdir = new File("${project.projectDir}/src/main/java/")
        File navfiles = new File("${project.projectDir}/src/main/res/navigation/")
        File layoutFiles = new File("${project.projectDir}/src/main/res/layout/")

        List<String> layoutFilesToParse = new ArrayList<>()
        Map<String, String> NavGraphStartDestMap = new HashMap<>()
        Map<String, String> activityIdLayoutMap = new HashMap<>()
        Map<String,String> layoutNavGraphMap = new HashMap<>()
        Map<String, String> NavIdStartDestMap = new HashMap<>()

        Map<String, FragmentModel> fragmentModelMap = new HashMap<>()

        ClassName fragment = ClassName.get("androidx.fragment.app","Fragment")
        ClassName bundle = ClassName.get("android.os","Bundle")
        ClassName layoutInflater = ClassName.get("android.view","LayoutInflater")
        ClassName view = ClassName.get("android.view","View")
        ClassName viewGroup = ClassName.get("android.view","ViewGroup")
        TypeVariableName genericType = TypeVariableName.get("T")

        def navGenTask = project.tasks.create("navgen") {
            navfiles.eachFile {
                def root = new XmlParser().parse(it)
                NavGraphStartDestMap.put(it.getName().split(".xml")[0]
                        ,root.attribute(NavParser.appNS.startDestination)
                        .toString().split("/")[1])

                root.activity.each { Node activityNode ->
                    def layoutId = activityNode.attribute(NavParser.toolsNS.layout)
                    if(layoutId.toString().contains("/"))
                        layoutFilesToParse.add(layoutId.toString().split("/")[1] + ".xml")
                    def activityId = activityNode.attribute(NavParser.androidNS.id)
                    if(activityId.toString().contains("/") && layoutId.toString().contains("/"))
                        activityIdLayoutMap.put(activityId.toString().split("/")[1]
                                ,layoutId.toString().split("/")[1])
                }

                root.navigation.each { Node navNode ->
                    def navId = navNode.attribute(NavParser.androidNS.id)
                    def startDestId = navNode.attribute(NavParser.appNS.startDestination)
                    if(navId.toString().contains("/")
                            && startDestId.toString().contains("/")) {
                        NavIdStartDestMap.put(navId.toString().split("/")[1]
                                ,startDestId.toString().split("/")[1])
                    }
                }
            }

            layoutFiles.eachFile {
                if(layoutFilesToParse.contains(it.getName())) {
                    def root = new XmlParser().parse(it)
                    def navGraph = root.depthFirst().find {
                        it.name() == Constants.TAG_FRAGMENT
                    }.attribute(NavParser.appNS.navGraph)

                    if(navGraph.toString().contains("/"))
                        layoutNavGraphMap.put(it.getName().split(".xml")[0]
                                ,navGraph.toString().split("/")[1])
                }
            }

            navfiles.eachFile {
                def root = new XmlParser().parse(it)

                def fragNodes = root.depthFirst().findAll {
                    it.name() == Constants.TAG_FRAGMENT
                }
                fragNodes.each { Node node ->
                    def id = (node.attribute(NavParser.androidNS.id) != null
                            && node.attribute(NavParser.androidNS.id).toString().contains("/")) ?
                            node.attribute(NavParser.androidNS.id).toString().split('/')[1] : ""
                    if(NavParser.parseFragmentNode(node) != null)
                        fragmentModelMap.put(id,NavParser.parseFragmentNode(node))
                }

                fragNodes.each {Node node ->
                    node.action.each { Node anode ->
                        def destId = (anode.attribute(NavParser.appNS.destination) != null
                                && anode.attribute(NavParser.appNS.destination).toString().contains("/")) ?
                                anode.attribute(NavParser.appNS.destination).toString().split('/')[1] : ""
                        if(fragmentModelMap.get(destId) != null) {
                            fragmentModelMap.put(destId
                                    ,updateFragmenModel(anode,fragmentModelMap.get(destId)))
                        }
                        else if(NavIdStartDestMap.get(destId) != null) {
                            fragmentModelMap.put(NavIdStartDestMap.get(destId)
                                    ,updateFragmenModel(anode
                                    ,fragmentModelMap.get(NavIdStartDestMap.get(destId))))
                        }
                        else if(activityIdLayoutMap.get(destId) != null) {
                            def key = NavGraphStartDestMap
                                    .get(layoutNavGraphMap
                                    .get(activityIdLayoutMap.get(destId)))
                            fragmentModelMap.put(key
                                    ,updateFragmenModel(anode,fragmentModelMap.get(key)))
                        }
                    }
                }
            }

            fragmentModelMap.keySet().each { String fragId ->
                methodEnv = new ArrayList<>()
                FragmentModel fragmentModel = fragmentModelMap.get(fragId)
                def layoutId = fragmentModel.getLayoutId()
                def fragClassName = getClassName(layoutId)


                MethodSpec onCreateView = MethodSpec.methodBuilder("onCreateView")
                        .addModifiers(Modifier.PUBLIC)
                        .addParameter(layoutInflater,"inflater")
                        .addParameter(viewGroup,"container")
                        .addParameter(bundle,"savedInstanceState")
                        .returns(view)
                        .addStatement("return inflater.inflate(R.layout." + layoutId + ", container, false)")
                        .addAnnotation(Override.class)
                        .build()

                MethodSpec onViewCreated = MethodSpec.methodBuilder("onViewCreated")
                        .addModifiers(Modifier.PUBLIC)
                        .addParameter(view,"view")
                        .addParameter(bundle,"savedInstanceState")
                        .returns(void.class)
                        .addAnnotation(Override.class)
                        .addStatement("super.onViewCreated(view, savedInstanceState)")
                        .build()

                TypeSpec fragClass = TypeSpec.classBuilder(fragClassName)
                        .addModifiers(Modifier.PUBLIC)
                        .superclass(fragment)
                        .addMethod(onCreateView)
                        .build()

                if(fragmentModel.getInActionArgs() != null) {
                    fragmentModel.getInActionArgs().each { ArgumentModel argumentModel ->
                        String argName = argumentModel.getName()
                        MethodSpec methodSpec = MethodSpec.methodBuilder(getMethodName(argName))
                                .addModifiers(Modifier.PRIVATE)
                                .addTypeVariable(genericType)
                                .returns(genericType)
                                .addParameter(bundle,"bundle")
                                .addParameter(String.class,argName)
                                .addStatement("throw new UnsupportedOperationException()")
                                .build()
                        fragClass = fragClass.toBuilder().addMethod(methodSpec).build()
                    }
                }

                if(fragmentModel.getArgs() != null) {
                    fragmentModel.getArgs().each { ArgumentModel argumentModel ->
                        String argName = argumentModel.getName()
                        MethodSpec methodSpec = MethodSpec.methodBuilder(getMethodName(argName))
                                .addModifiers(Modifier.PRIVATE)
                                .addTypeVariable(genericType)
                                .returns(genericType)
                                .addParameter(bundle,"bundle")
                                .addParameter(String.class,argName)
                                .addStatement("throw new UnsupportedOperationException()")
                                .build()
                        fragClass = fragClass.toBuilder().addMethod(methodSpec).build()
                    }
                }
                if(fragmentModel.getWidgets() != null) {
                    fragmentModel.getWidgets().each { WidgetModel widgetModel ->
                        if(widgetModel.getFunModel() != null
                                || widgetModel.getVariableVal() != null
                                || widgetModel.getPrimitiveVal() != null) {

                            MethodSpec initWidgetMethod = MethodSpec
                                    .methodBuilder(getInitMethodName(widgetModel.getWid()))
                                    .addModifiers(Modifier.PRIVATE)
                                    .returns(void.class)
                                    .addParameter(int.class,"id")
                                    .addStatement("throw new UnsupportedOperationException()")
                                    .build()

                            if(widgetModel.getPrimitiveVal() != null) {
                                initWidgetMethod = initWidgetMethod.toBuilder()
                                        .addParameter(String.class
                                            ,widgetModel.getPrimitiveVal() + "Arg")
                                        .build()
                            }
                            if(widgetModel.getVariableVal() != null) {
                                initWidgetMethod = initWidgetMethod.toBuilder()
                                        .addTypeVariable(genericType)
                                        .addParameter(genericType
                                            ,widgetModel.getVariableVal() + "Var")
                                        .build()
                            }
                            if(widgetModel.getFunModel() != null) {
                                makeFun(widgetModel.getFunModel()).each {
                                    if(!methodExists(it)) {
                                        methodEnv.add(it)
                                        fragClass = fragClass.toBuilder()
                                                .addMethod(it).build()
                                    }
                                }
                                initWidgetMethod = initWidgetMethod.toBuilder()
                                        .addTypeVariable(genericType)
                                        .addParameter(genericType,widgetModel.getFunModel().getName())
                                        .build()

                            }

                            if(!methodExists(initWidgetMethod)) {
                                methodEnv.add(initWidgetMethod)
                                fragClass = fragClass.toBuilder()
                                        .addMethod(initWidgetMethod)
                                        .build()
                            }

                            String initWidgetCallStmt = getInitMethodName(widgetModel.getWid())+ "(R.id." +
                                    widgetModel.getWid() + ","
                            StringBuffer argBuffer = new StringBuffer()
                            if(widgetModel.getPrimitiveVal() != null) {
                                argBuffer.append("\"" + widgetModel.getPrimitiveVal() + "\"")
                                argBuffer.append(",")
                            }
                            if(widgetModel.getVariableVal() != null) {
                                argBuffer.append(getMethodName(widgetModel.getVariableVal()) +
                                        "(getArguments(),\"" +
                                        widgetModel.getVariableVal() + "\")")
                                argBuffer.append(",")
                            }
                            if(widgetModel.getFunModel() != null) {
                                argBuffer.append(makeFunCall(widgetModel.getFunModel()))
                                argBuffer.append(",")
                            }
                            if(argBuffer.toString() != null
                                    && argBuffer.toString().contains(","))
                                initWidgetCallStmt = initWidgetCallStmt + argBuffer.toString()
                                        .substring(0,argBuffer.toString().lastIndexOf(",")) + ")"
                            else initWidgetCallStmt = initWidgetCallStmt + ")"
                            onViewCreated = onViewCreated.toBuilder()
                                    .addStatement(initWidgetCallStmt)
                                    .build()
                        }
                    }

                    if(fragmentModel.getOutActions() != null) {
                        Map<String,ConstraintModel> gestureConstraintMap = new HashMap<>()
                        fragmentModel.getOutActions().each { ActionModel action ->
                            String key = action.getWidgetOn() + "#" + action.getGesture()

                        }
                    }
                }

                fragClass = fragClass.toBuilder().addMethod(onViewCreated).build()
                JavaFile javaFile = JavaFile.builder("sema.apps.mockuber",fragClass).build()
                javaFile.writeTo(outdir)
            }
        }
        navGenTask.description = 'Generates source code from navigation graphs'
        navGenTask.group = "codegen"
//        navGenTask.mustRunAfter("lint")

    }

    private String getClassName(String id) {
        String head = id.substring(0,id.indexOf("_"))
        String tail = id.substring(id.indexOf("_")+1)
        if(tail.contains("_"))
            return (head.substring(0,1).toUpperCase() +
                    head.substring(1) +
                    getClassName(tail))
        else
            return (head.substring(0,1).toUpperCase() +
                    head.substring(1) +
                    tail.substring(0,1).toUpperCase() +
                    tail.substring(1))
    }

    private String getMethodName(String m) {
        return ("get" + m.substring(0,1).toUpperCase() + m.substring(1))
    }

    private String getInitMethodName(String m) {
        return ("init" + m.substring(0,1).toUpperCase() + m.substring(1))
    }

    private FragmentModel updateFragmenModel(Node anode, FragmentModel fragmentModel) {
        List<ArgumentModel> inArgs = new ArrayList<>()
        anode.argument.each {
            inArgs.add(NavParser.parseArgumentNode(it))
        }
        if(!inArgs.isEmpty()) fragmentModel.setInActionArgs(inArgs)
        return fragmentModel
    }

    private String makeFunCall(FunModel funModel) {
        String stmt = funModel.getName() + "("
        StringBuffer funArgs = new StringBuffer()
        if(funModel.getPrimitives() != null) {
            funModel.getPrimitives().each {
                funArgs.append("\"" + it + "\"")
                funArgs.append(",")
            }
        }
        if(funModel.getIdValues() != null) {
            funModel.getIdValues().each {
                funArgs.append(getMethodName(it) + "(R.id." + it + ")")
                funArgs.append(",")
            }
        }
        if(funModel.getVariables() != null) {
            funModel.getVariables().each {
                funArgs.append(getMethodName(it) + "(getArguments(),\"" + it + "\")")
                funArgs.append(",")
            }
        }
        if(funModel.getFuns() != null) {
            funModel.getFuns().each {
                funArgs.append(makeFunCall(it))
                funArgs.append(",")
            }
        }
        if(funArgs.toString() != null
                && funArgs.toString().contains(","))
            return (stmt + funArgs
                    .toString()
                    .substring(0,funArgs.toString().lastIndexOf(",")) + ")")
        else return (stmt + ")")
    }

    private List<MethodSpec> makeFun(FunModel funModel) {
        TypeVariableName genericType = TypeVariableName.get("T")
        List<MethodSpec> methods = new ArrayList<>()
        String funName = funModel.getName()
        MethodSpec method = MethodSpec.methodBuilder(funName)
                .addModifiers(Modifier.PRIVATE)
                .addTypeVariable(genericType)
                .returns(genericType)
                .addStatement("throw new UnsupportedOperationException()")
                .build()
        if(funModel.getPrimitives() != null) {
            funModel.getPrimitives().each {
                ParameterSpec primitive = ParameterSpec.builder(String.class,it).build()
                method = method.toBuilder().addParameter(primitive).build()
            }
        }
        if(funModel.getVariables() != null) {
            funModel.getVariables().each {
                ParameterSpec varParam = ParameterSpec.builder(genericType,it).build()
                method = method.toBuilder().addParameter(varParam).build()
            }
        }
        if(funModel.getIdValues() != null) {
            funModel.getIdValues().each {
                String mName = getMethodName(it)
                MethodSpec m = MethodSpec.methodBuilder(mName)
                        .addTypeVariable(genericType)
                        .returns(genericType)
                        .addParameter(int.class,"widgetId")
                        .addStatement("throw new UnsupportedOperationException()")
                        .build()
                methods.add(m)
                ParameterSpec idParam = ParameterSpec.builder(genericType,it).build()
                method = method.toBuilder().addParameter(idParam).build()
            }
        }
        if(funModel.getFuns() != null) {
            funModel.getFuns().each {
                methods.addAll(makeFun(it))
                ParameterSpec funParam = ParameterSpec.builder(genericType,it.getName()).build()
                method = method.toBuilder().addParameter(funParam).build()
            }
        }
        methods.add(method)
        return methods
    }

    private boolean methodExists(MethodSpec methodSpec) {
        if(methodEnv == null) return false
        else {
            return (methodEnv.find {
                it.name == methodSpec.name &&
                        it.parameters.collect{it.name}
                                .containsAll(methodSpec.parameters.collect {it.name})
            } != null)
        }
    }

    private void displayFun(FunModel funModel) {
        System.out.println("fun name = " + funModel.getName())
        System.out.println("fun parm primitives = " + funModel.getPrimitives())
        System.out.println("fun parm ids = " + funModel.getIdValues())
        System.out.println("fun parm vars = " + funModel.getVariables())
        if(funModel.getFuns() != null && funModel.getFuns().isEmpty())
            funModel.getFuns().each {
                displayFun(it)
            }
    }
}

//        def attrEnumTask = project.tasks.create("navgen") {
//            def attr = new XmlParser().parse(attrsfile)
//            def styleable = attr.depthFirst().find {
//                it.name() == "declare-styleable" && it.@name == "MyEnumStyle"
//            }
//            def enumlist = styleable.depthFirst().find { it.name() == "attr" && it.@name == "fubar" }
//            def vals = enumlist.depthFirst().findAll { it.name() == "enum" }
//
//            TypeSpec.Builder attrEnumBuilder = TypeSpec.enumBuilder("AttrEnum").addModifiers(Modifier.PUBLIC)
//            vals.each {
//                System.out.print("name = " + it.@name)
//                attrEnumBuilder.addEnumConstant(it.@name, TypeSpec.anonymousClassBuilder('$L', it.@value as int).build())
//            }
//
//            attrEnumBuilder
//                    .addField(TypeName.INT, "index", Modifier.PUBLIC, Modifier.FINAL)
//                    .addMethod(MethodSpec.constructorBuilder()
//                    .addParameter(TypeName.INT, "index")
//                    .addStatement('this.$N = $N', "index", "index")
//                    .build())
//
//            TypeSpec attrEnum = attrEnumBuilder.build();
//            JavaFile javaFile = JavaFile.builder("sema.apps.mockuber", attrEnum).build();
//            javaFile.writeTo(outdir)
//        }
