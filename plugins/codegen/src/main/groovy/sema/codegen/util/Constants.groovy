package sema.codegen.util

import groovy.xml.Namespace

/**
 * Created by Joy on 7/24/19.
 */
class Constants {

    public static String BUSLOGIC_DIR = ".busLogic"
    public static String BRDCST_RCVR = "BroadcastReceiver"
    public static String MANIFEST_TAG_RECV = "receiver"
    public static String MANIFEST_TAG_APP = "application"
    public static String MANIFEST_TAG_PERM = "permission"
    public static String METHOD_ONREQPERM = "onRequestPermissionsResult"

    public static String TAG_FRAGMENT = "fragment"
    public static String TAG_RECV = "event-receiver"
    public static String TAG_FUN = "fun"
    public static String TAG_AND = "and"
    public static String TAG_OR = "or"
    public static String TAG_NOT = "not"
    public static String PREFIX_ID = "@id/"
    public static String PREFIX_VAR = "var/"
    public static String PREFIX_FUN = "fun/"
    public static String PREFIX_INTENT = "intent/"
    public static String GESTURE_CLICK = "click"
    public static String MAKE_BUNDLE_FUN_NAME = "makeBundle"

    public static String WIDGET_NAME_TV = "TextView"
    public static String WIDGET_NAME_ET = "EditText"
    public static String WIDGET_NAME_WV = "WebView"

    public static String RES_INT_STORE = "INT_STORE"
    public static String RES_EXT_STORE = "EXT_STORE"
    public static String RES_INTERNET = "INTERNET"
    public static String RES_LOCATION = "LOCATION"
    public static String RES_KEYSTORE = "KEYSTORE"
    public static String RES_CIPHER = "CIPHER"
    public static String RES_ICC = "ICC"
    public static String RES_VIBRATOR = "VIBRATOR"
    public static String RES_SSLSOCKET = "SSLSocket"
    public static String RES_CLIPBRD = "CLIPBOARD_SERVICE"
    public static String RES_TELPHNY = "TELEPHONE_SERVICE"
    public static String RES_DYNCODELOAD = "DYNAMIC_CODE_LOAD"

    public static String ATTR_CERT_PIN = "useCertificatePinning"
    public static String ATTR_FUN_REQ_PERM = "requiresPerm"
    public static String ATTR_FUN_LOCAL = "local"
    public static String ATTR_PBE = "pbe"
    public static String ATTR_RECV_ACCESS = "access"
    public static String ATTR_RECV_PERMTYP = "permission"
    public static String ATTR_RECV_PERMNM = "permission-name"

    public static String RES_METHOD_POST = "post"
    public static String RES_METHOD_GET = "get"
    public static String RES_METHOD_EXIST = "exists"
    public static String RES_METHOD_LOADURL = "loadUrl"
    public static String RES_METHOD_LOADDATA = "loadDataWithBaseUrl"
    public static String RES_METHOD_ENCRYPT = "encrypt"
    public static String RES_METHOD_DECRYPT = "decrypt"
    public static String RES_METHOD_SENDBRDCST = "sendBroadcast"
    public static String RES_METHOD_ORD_SENDBRDCST = "sendOrderedBroadcast"

    public static String URI_PROTOCOL_FILE = "file://"
    public static String URI_PROTOCOL_HTTP = "http://"
    public static String URI_PROTOCOL_HTTPS = "https://"

    public static String SEPARATOR_SLASH = "/"

    public static String TRUE = "true"
    public static String FALSE = "false"

    public static String HOST_INDICATOR = "host!"
    public static String PROTOCOL_INDICATOR = "protocol!"
    public static String FILE_PATH_INDICATOR = "filePath!"
    public static String DIR_INDICATOR = "dir!"
    public static String SYMMETRIC_KEY_INDICTOR = "key!"
    public static String RECV_INPUT_INDICATOR = "input/"

    public static String PARM_HOST = "host"
    public static String PARM_PROTOCOL = "protocol"
    public static String PARM_FILEPATH = "filePath"
    public static String PARM_DIR = "dir"
    public static String PARM_KEY = "key"
    public static String KEYSTORE_FILE = "keyStore.txt"
    public static String IV_FILE = "initVector.txt"

    public static String ASYNC_BACKGROUND_PARAM_NAME = "params"

    public static String BUSLOGIC = "busLogic"
    public static String OBJBUSLOGIC = "objBusLogic"
    public static String OBJBUSLOGIC_INNER = "objBusLogic1"

    public static SET_IV_METHOD = "saveIV"
    public static GET_IV_METHOD = "getIV"
    public static ENCRYPT_MODE = "ENCRYPT_MODE"
    public static DECRYPT_MODE = "DECRYPT_MODE"
    public static URL_INTENT_HANDLE_METHOD = "handleUrlIntent"

    public static RECV_ACCESS_1 = "all"
    public static RECV_ACCESS_2 = "user"
    public static RECV_ACCESS_3 = "own"

    public static PERM_TYPE_CUSTOM = "custom"
    public static PERM_PROTECT_USER = "user"
    public static PERM_PROTECT_ALL = "all"
    public static PERM_PROTECT_OWN = "own"

    public static int PERM_REQ_CODE_101 = 2922

}
