package sema.codegen.util

import com.squareup.javapoet.ClassName
import com.squareup.javapoet.CodeBlock
import com.squareup.javapoet.MethodSpec
import com.squareup.javapoet.ParameterSpec
import com.squareup.javapoet.TypeName
import sema.codegen.model.FunModel

import javax.crypto.BadPaddingException
import javax.crypto.IllegalBlockSizeException
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec
import javax.lang.model.element.Modifier
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.UnrecoverableEntryException
import java.security.cert.CertificateException
import java.security.spec.InvalidKeySpecException
import java.security.spec.KeySpec

/**
 * Created by Joy on 8/25/19.
 */
class UsefulMethods {
    static MethodSpec internalStoreWriteStmt(MethodSpec methodSpec, String userMethodNm, List<ParameterSpec> parameterSpecs) {
        ClassName context = ClassName.get("android.content","Context")
        String params = null
        if(parameterSpecs != null) {
            params = parameterSpecs.collect { it.name }.join(",")
        }
        return methodSpec.toBuilder()
                .beginControlFlow("try(\$T outputStream " +
                    "= getContext().openFileOutput(\$L, \$T.MODE_PRIVATE))"
                    , FileOutputStream.class
                    , Constants.PARM_FILEPATH
                    , context)
                .addStatement(Constants.OBJBUSLOGIC + "." + userMethodNm + "(outputStream,\$L)", params)
                .endControlFlow()
                .beginControlFlow("catch(\$T e)", IOException.class)
                .addStatement("e.printStackTrace()")
                .addStatement("return false")
                .endControlFlow()
                .addStatement("return true")
                .build()
    }

    static MethodSpec storeWriteMethod(List<ParameterSpec> parameterSpecs, String name) {
        MethodSpec m = MethodSpec
                .methodBuilder(name)
                .addException(IOException.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(void.class)
                .addParameter(FileOutputStream.class, "outputStream")
                .addStatement("throw new \$T()", UnsupportedOperationException.class)
                .build()
        if(parameterSpecs != null) {
            parameterSpecs.each { ParameterSpec parameterSpec ->
                m = m.toBuilder().addParameter(String.class, parameterSpec.name).build()
            }
        }
        return m
    }

    static MethodSpec internalStoreReadStmt(MethodSpec methodSpec, String userMethodNm, List<ParameterSpec> parameterSpecs) {
        def paramList = parameterSpecs.collect { it.name }
        if(!paramList.isEmpty()) {
            def params = paramList.join(",")
            return methodSpec.toBuilder()
                    .beginControlFlow("try(\$T inputStream " +
                        "= getContext().openFileInput(\$L))", FileInputStream.class, Constants.PARM_FILEPATH)
                    .addStatement("return " + Constants.OBJBUSLOGIC + "." + userMethodNm + "(inputStream,\$L)", params)
                    .endControlFlow()
                    .beginControlFlow("catch(\$T e)", IOException.class)
                    .addStatement("e.printStackTrace()")
                    .endControlFlow()
                    .addStatement("return null")
                    .build()
        }
        else
            return methodSpec.toBuilder()
                    .beginControlFlow("try(\$T inputStream " +
                    "= getContext().openFileInput(\$L))", FileInputStream.class, Constants.PARM_FILEPATH)
                    .addStatement("return " + Constants.OBJBUSLOGIC + "." + userMethodNm + "(inputStream)")
                    .endControlFlow()
                    .beginControlFlow("catch(\$T e)", IOException.class)
                    .addStatement("e.printStackTrace()")
                    .endControlFlow()
                    .addStatement("return null")
                    .build()
    }

    static MethodSpec internalStoreReadMethod(List<ParameterSpec> parameterSpecs, String name) {
        MethodSpec m = MethodSpec
                .methodBuilder(name)
                .addException(IOException.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(String.class)
                .addParameter(FileInputStream.class, "inputStream")
                .addStatement("throw new \$T()", UnsupportedOperationException.class)
                .build()
        if(parameterSpecs != null) {
            parameterSpecs.each { ParameterSpec parameterSpec ->
                m = m.toBuilder().addParameter(String.class, parameterSpec.name).build()
            }
        }
        return m
    }

    static MethodSpec extStoreWriteStmt(MethodSpec methodSpec, String userMethodNm, List<ParameterSpec> parameterSpecs) {
        String params = null
        if(parameterSpecs != null) {
            params = parameterSpecs.collect { it.name }.join(",")
        }
        return methodSpec.toBuilder()
                .addStatement("\$T file " +
                "= new \$T(getContext().getExternalFilesDir(\$L),\$L)"
                , File.class
                , File.class
                , Constants.PARM_DIR
                , Constants.PARM_FILEPATH)
                .beginControlFlow("try(\$T outputStream = new \$T(file))", FileOutputStream.class, FileOutputStream.class)
                .addStatement(Constants.OBJBUSLOGIC + "." + userMethodNm + "(outputStream,\$L)", params)
                .endControlFlow()
                .beginControlFlow("catch(\$T e)", IOException.class)
                .addStatement("e.printStackTrace()")
                .addStatement("return false")
                .endControlFlow()
                .addStatement("return true")
                .build()
    }

    static MethodSpec extStoreReadStmt(MethodSpec methodSpec, String userMethodNm, List<ParameterSpec> parameterSpecs) {
        def paramList = parameterSpecs.collect { it.name }
        if(!paramList.isEmpty()) {
            def params = paramList.join(",")
            return methodSpec.toBuilder()
                    .addStatement("\$T file " +
                    "= new \$T(getContext().getExternalFilesDir(\$L),\$L)"
                    , File.class
                    , File.class
                    , Constants.PARM_DIR
                    , Constants.PARM_FILEPATH)
                    .beginControlFlow("try(\$T inputStream = new \$T(file))", FileInputStream.class, FileInputStream.class)
                    .addStatement("return " + Constants.OBJBUSLOGIC + "." + userMethodNm + "(inputStream,\$L)", params)
                    .endControlFlow()
                    .beginControlFlow("catch(\$T e)", IOException.class)
                    .addStatement("e.printStackTrace()")
                    .endControlFlow()
                    .addStatement("return null")
                    .build()
        }
        else
            return methodSpec.toBuilder()
                    .addStatement("\$T file " +
                    "= new \$T(getContext().getExternalFilesDir(\$L),\$L)"
                    , File.class
                    , File.class
                    , Constants.PARM_DIR
                    , Constants.PARM_FILEPATH)
                    .beginControlFlow("try(\$T inputStream = new \$T(file))", FileInputStream.class, FileInputStream.class)
                    .addStatement("return " + Constants.OBJBUSLOGIC + "." + userMethodNm + "(inputStream)")
                    .endControlFlow()
                    .beginControlFlow("catch(\$T e)", IOException.class)
                    .addStatement("e.printStackTrace()")
                    .endControlFlow()
                    .addStatement("return null")
                    .build()
    }

    static boolean isFunRequiresAysnc(FunModel funModel) {
        if(funModel.getResource() != null && funModel.getResource().equals(Constants.RES_INTERNET)
                && funModel.getMethod() != Constants.RES_METHOD_LOADURL)
            return true
        else if(funModel.getFuns() != null) {
            return funModel.getFuns().any {
                it.getResource() != null && it.getResource().equals(Constants.RES_INTERNET) && it.getMethod() != Constants.RES_METHOD_LOADURL
            }
        }
        else
            return false
    }

    static MethodSpec GethttpStmt(MethodSpec methodSpec, String getUrlParamsMethodNm, String inputStreamMethodNm, List<ParameterSpec> parameterSpecs) {
        def paramList = parameterSpecs.collect { it.name }
        ClassName classURL = ClassName.get("java.net", "URL")
        ClassName classHttpConn = ClassName.get("java.net", "HttpURLConnection")
        ClassName classHttpsConn = ClassName.get("javax.net.ssl", "HttpsURLConnection")
        CodeBlock finallyBlock = CodeBlock.builder()
                .beginControlFlow("finally")
                .beginControlFlow("if(urlConnection != null)")
                .addStatement("urlConnection.disconnect()")
                .endControlFlow()
                .endControlFlow()
                .build()
        if(!paramList.isEmpty()) {
            def params = paramList.join(",")
            return methodSpec.toBuilder()
                    .beginControlFlow("if (protocol.equals(\"http\"))")
                    .addStatement("\$T urlConnection = null", classHttpConn)
                    .addStatement("\$T urlString = protocol + \"://\" + host + \$L(\$L)", String.class, Constants.OBJBUSLOGIC + "." + getUrlParamsMethodNm, params)
                    .beginControlFlow("try")
                    .addStatement("\$T url = new \$T(urlString)", classURL, classURL)
                    .addStatement("urlConnection = (\$T) url.openConnection()", classHttpConn)
                    .addStatement("return " + Constants.OBJBUSLOGIC + "." + inputStreamMethodNm + "(urlConnection.getInputStream())")
                    .endControlFlow()
                    .beginControlFlow("catch(\$T e)", IOException.class)
                    .addStatement("e.printStackTrace()")
                    .endControlFlow()
                    .addCode(finallyBlock)
                    .addStatement("return null")
                    .nextControlFlow("else if (protocol.equals(\"https\"))")
                    .addStatement("\$T urlConnection = null", classHttpsConn)
                    .addStatement("\$T urlString = protocol + \"://\" + host + \$L(\$L)", String.class, Constants.OBJBUSLOGIC + "." + getUrlParamsMethodNm, params)
                    .beginControlFlow("try")
                    .addStatement("\$T url = new \$T(urlString)", classURL, classURL)
                    .addStatement("urlConnection = (\$T) url.openConnection()", classHttpsConn)
                    .addStatement("return " + Constants.OBJBUSLOGIC + "." + inputStreamMethodNm + "(urlConnection.getInputStream())")
                    .endControlFlow()
                    .beginControlFlow("catch(\$T e)", IOException.class)
                    .addStatement("e.printStackTrace()")
                    .endControlFlow()
                    .addCode(finallyBlock)
                    .addStatement("return null")
                    .nextControlFlow("else")
                    .addStatement("return null")
                    .endControlFlow()
                    .build()
        }
        else
            return methodSpec.toBuilder()
                    .beginControlFlow("if (protocol.equals(\"http\"))")
                    .addStatement("\$T urlConnection = null", classHttpConn)
                    .addStatement("\$T urlString = protocol + \"://\" + host", String.class)
                    .beginControlFlow("try")
                    .addStatement("\$T url = new \$T(urlString)", classURL, classURL)
                    .addStatement("urlConnection = (\$T) url.openConnection()", classHttpConn)
                    .addStatement("return " + Constants.OBJBUSLOGIC + "." + inputStreamMethodNm + "(urlConnection.getInputStream())")
                    .endControlFlow()
                    .beginControlFlow("catch(\$T e)", IOException.class)
                    .addStatement("e.printStackTrace()")
                    .endControlFlow()
                    .addCode(finallyBlock)
                    .addStatement("return null")
                    .nextControlFlow("else if (protocol.equals(\"https\"))")
                    .addStatement("\$T urlConnection = null", classHttpsConn)
                    .addStatement("\$T urlString = protocol + \"://\" + host", String.class)
                    .beginControlFlow("try")
                    .addStatement("\$T url = new \$T(urlString)", classURL, classURL)
                    .addStatement("urlConnection = (\$T) url.openConnection()", classHttpsConn)
                    .addStatement("return " + Constants.OBJBUSLOGIC + "." + inputStreamMethodNm + "(urlConnection.getInputStream())")
                    .endControlFlow()
                    .beginControlFlow("catch(\$T e)", IOException.class)
                    .addStatement("e.printStackTrace()")
                    .endControlFlow()
                    .addCode(finallyBlock)
                    .addStatement("return null")
                    .nextControlFlow("else")
                    .addStatement("return null")
                    .endControlFlow()
                    .build()
    }

    static MethodSpec urlParamsMethod(String methodNm, List<ParameterSpec> parameterSpecs) {
        MethodSpec m = MethodSpec
                .methodBuilder(methodNm)
                .addModifiers(Modifier.PUBLIC)
                .returns(String.class)
                .addStatement("throw new \$T()", UnsupportedOperationException.class)
                .build()
        if(parameterSpecs != null) {
            parameterSpecs.each { ParameterSpec parameterSpec ->
                m = m.toBuilder().addParameter(parameterSpec).build()
            }
        }
        return m
    }

    static MethodSpec inStreamGetMethod(String name) {
        MethodSpec m = MethodSpec
                .methodBuilder(name)
                .addException(IOException.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(String.class)
                .addParameter(InputStream.class, "inputStream")
                .addStatement("throw new \$T()", UnsupportedOperationException.class)
                .build()
        return m
    }

    static MethodSpec PosthttpStmt(MethodSpec methodSpec, String postParamsMethodNm, String inputStreamMethodNm, List<ParameterSpec> parameterSpecs) {
        def paramList = parameterSpecs.collect { it.name }
        ClassName classURL = ClassName.get("java.net", "URL")
        ClassName classHttpConn = ClassName.get("java.net", "HttpURLConnection")
        ClassName classHttpsConn = ClassName.get("javax.net.ssl", "HttpsURLConnection")
        CodeBlock finallyBlock = CodeBlock.builder()
                .beginControlFlow("finally")
                .beginControlFlow("if(urlConnection != null)")
                .addStatement("urlConnection.disconnect()")
                .endControlFlow()
                .endControlFlow()
                .build()
        def params = paramList.join(",")
        MethodSpec m = methodSpec.toBuilder()
                .beginControlFlow("if (protocol.equals(\"http\"))")
                .addStatement("\$T urlConnection = null", classHttpConn)
                .addStatement("\$T urlString = protocol + \"://\" + host", String.class)
                .beginControlFlow("try")
                .addStatement("\$T url = new \$T(urlString)", classURL, classURL)
                .addStatement("urlConnection = (\$T) url.openConnection()", classHttpConn)
                .addStatement("urlConnection.setDoOutput(true)")
                .addStatement("urlConnection.setChunkedStreamingMode(0)")
                .addStatement(Constants.OBJBUSLOGIC + "." + postParamsMethodNm + "(urlConnection.getOutputStream(), \$L)", params)
                .addStatement("return " + Constants.OBJBUSLOGIC + "." + inputStreamMethodNm + "(urlConnection.getInputStream())")
                .endControlFlow()
                .beginControlFlow("catch(\$T e)", IOException.class)
                .addStatement("e.printStackTrace()")
                .endControlFlow()
                .addCode(finallyBlock)
                .nextControlFlow("else if (protocol.equals(\"https\"))")
                .addStatement("\$T urlConnection = null", classHttpsConn)
                .addStatement("\$T urlString = protocol + \"://\" + host", String.class)
                .beginControlFlow("try")
                .addStatement("\$T url = new \$T(urlString)", classURL, classURL)
                .addStatement("urlConnection = (\$T) url.openConnection()", classHttpsConn)
                .addStatement("urlConnection.setDoOutput(true)")
                .addStatement("urlConnection.setChunkedStreamingMode(0)")
                .addStatement(Constants.OBJBUSLOGIC + "." + postParamsMethodNm + "(urlConnection.getOutputStream(), \$L)", params)
                .addStatement("return " + Constants.OBJBUSLOGIC + "." + inputStreamMethodNm + "(urlConnection.getInputStream())")
                .endControlFlow()
                .beginControlFlow("catch(\$T e)", IOException.class)
                .addStatement("e.printStackTrace()")
                .endControlFlow()
                .addCode(finallyBlock)
                .endControlFlow()
                .build()
        if(methodSpec.returnType.equals(TypeName.BOOLEAN)) {
            return m.toBuilder().addStatement("return false").build()
        }
        else if(methodSpec.returnType.equals(TypeName.get(String.class))) {
            return m.toBuilder().addStatement("return null").build()
        }
        else return m
    }

    static MethodSpec inStreamPostMethod(String name, TypeName type) {
        MethodSpec m = MethodSpec
                .methodBuilder(name)
                .addException(IOException.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(type)
                .addParameter(InputStream.class, "inputStream")
                .addStatement("throw new \$T()", UnsupportedOperationException.class)
                .build()
        return m
    }

    static MethodSpec postParamsMethod(List<ParameterSpec> parameterSpecs, String name) {
        MethodSpec m = MethodSpec
                .methodBuilder(name)
                .addException(IOException.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(void.class)
                .addParameter(OutputStream.class, "outputStream")
                .addStatement("throw new \$T()", UnsupportedOperationException.class)
                .build()
        if(parameterSpecs != null) {
            parameterSpecs.each { ParameterSpec parameterSpec ->
                m = m.toBuilder().addParameter(String.class, parameterSpec.name).build()
            }
        }
        return m
    }

    static MethodSpec cipherOpMethod(MethodSpec methodSpec, List<ParameterSpec> parameterSpecs, String opMode) {
        ClassName cipherClass = ClassName.get("javax.crypto", "Cipher")
        ClassName base64Class = ClassName.get("android.util","Base64")

        CodeBlock codeBlock = CodeBlock.builder()
                .addStatement("cipher.init(\$T.\$L, key)", cipherClass, opMode)
                .addStatement(Constants.SET_IV_METHOD + "(cipher.getIV())")
                .addStatement("return \$T.encodeToString(cipher.doFinal(\$L.getBytes()), \$T.DEFAULT)"
                , base64Class
                , parameterSpecs.find {it.name != Constants.PARM_KEY}.name
                , base64Class)
                .endControlFlow()
                .beginControlFlow("catch (\$T | \$T | \$T | \$T | \$T e)", NoSuchAlgorithmException.class, NoSuchPaddingException.class, InvalidKeyException.class, IllegalBlockSizeException.class, BadPaddingException.class)
                .addStatement("e.printStackTrace()")
                .addStatement("return \"\"")
                .endControlFlow()
                .build()
        if(opMode == Constants.DECRYPT_MODE) {
            codeBlock = CodeBlock.builder()
                    .addStatement("\$T ivPramSpec = new \$T(\$L())", IvParameterSpec.class, IvParameterSpec.class, Constants.GET_IV_METHOD)
                    .addStatement("cipher.init(\$T.\$L, key, ivPramSpec)", cipherClass, opMode)
                    .addStatement("return \$T.encodeToString(cipher.doFinal(\$L.getBytes()), \$T.DEFAULT)"
                    , base64Class
                    , parameterSpecs.find {it.name != Constants.PARM_KEY}.name
                    , base64Class)
                    .endControlFlow()
                    .beginControlFlow("catch (\$T | \$T | \$T | \$T | \$T | \$T e)", NoSuchAlgorithmException.class, NoSuchPaddingException.class, InvalidKeyException.class, IllegalBlockSizeException.class, BadPaddingException.class, InvalidAlgorithmParameterException.class)
                    .addStatement("e.printStackTrace()")
                    .addStatement("return \"\"")
                    .endControlFlow()
                    .build()
        }


        methodSpec = methodSpec
                .toBuilder()
                .addStatement("\$T cipher", cipherClass)
                .beginControlFlow("try")
                .addStatement("cipher = \$T.getInstance(\"AES/GCM/NoPadding\")", cipherClass)
                .addCode(codeBlock)
                .build()
        return methodSpec
    }

    static MethodSpec readKeyStore(MethodSpec methodSpec, String alias) {
        ClassName keyStoreClass = ClassName.get("java.security", "KeyStore")
        methodSpec = methodSpec
                .toBuilder()
                .returns(ClassName.get("javax.crypto", "SecretKey"))
                .beginControlFlow("try(\$T inputStream " + "= getContext().openFileInput(\"\$L\"))", FileInputStream.class, Constants.KEYSTORE_FILE)
                .addStatement("\$T keyStore = \$T.getInstance(\$T.getDefaultType())", keyStoreClass, keyStoreClass, keyStoreClass)
                .addStatement("keyStore.load(inputStream, null)")
                .addStatement("\$T.SecretKeyEntry secretKeyEntry = (\$T.SecretKeyEntry) keyStore.getEntry(\$L, null)", keyStoreClass, keyStoreClass, alias)
                .addStatement("return secretKeyEntry.getSecretKey()")
                .endControlFlow()
                .beginControlFlow("catch(\$T e)", IOException.class)
                .addStatement("e.printStackTrace()")
                .endControlFlow()
                .beginControlFlow("catch(\$T | \$T | \$T | \$T e)", UnrecoverableEntryException.class, KeyStoreException.class, CertificateException.class, NoSuchAlgorithmException.class)
                .addStatement("e.printStackTrace()")
                .endControlFlow()
                .addStatement("return null")
                .build()
        return methodSpec
    }

    static MethodSpec writePBEKeyStore(MethodSpec methodSpec, String alias, String key) {
        ClassName keyStoreClass = ClassName.get("java.security", "KeyStore")
        ClassName ctxClass = ClassName.get("android.content", "Context")
        methodSpec = methodSpec
                .toBuilder()
                .returns(Boolean.class)
                .beginControlFlow("try(\$T outputStream " + "= getContext().openFileOutput(\"\$L\", \$T.MODE_PRIVATE))", FileOutputStream.class, Constants.KEYSTORE_FILE, ctxClass)
                .addStatement("\$T secureRandom = new \$T()", SecureRandom.class, SecureRandom.class)
                .addStatement("byte[] salt = new byte[16]")
                .addStatement("new SecureRandom().nextBytes(salt)")
                .addStatement("\$T keySpec = new \$T(\$L.toCharArray(), salt, 1000, 256)", KeySpec.class, PBEKeySpec.class, key)
                .addStatement("\$T secretKeyFactory = SecretKeyFactory.getInstance(\"PBKDF2WithHmacSHA1\")", SecretKeyFactory.class)
                .addStatement("byte[] keyBytes = secretKeyFactory.generateSecret(keySpec).getEncoded()")
                .addStatement("\$T secretKey = new \$T(keyBytes, \"AES\")", SecretKey.class, SecretKeySpec.class)
                .addStatement("\$T keyStore = \$T.getInstance(\$T.getDefaultType())", keyStoreClass, keyStoreClass, keyStoreClass)
                .addStatement("keyStore.load(null, null)")
                .addStatement("keyStore.setEntry(\$L, new \$T.SecretKeyEntry(secretKey), null)", alias, keyStoreClass)
                .addStatement("keyStore.store(outputStream, null)")
                .addStatement("return true")
                .endControlFlow()
                .beginControlFlow("catch(\$T e)", IOException.class)
                .addStatement("e.printStackTrace()")
                .addStatement("return false")
                .endControlFlow()
                .beginControlFlow("catch(\$T | \$T | \$T | \$T e)", KeyStoreException.class, CertificateException.class, NoSuchAlgorithmException.class, InvalidKeySpecException.class)
                .addStatement("e.printStackTrace()")
                .addStatement("return false")
                .endControlFlow()
                .build()
        return methodSpec
    }

    static MethodSpec writeKeyStore(MethodSpec methodSpec, String alias) {
        ClassName keyStoreClass = ClassName.get("java.security", "KeyStore")
        ClassName ctxClass = ClassName.get("android.content", "Context")
        methodSpec = methodSpec
                .toBuilder()
                .returns(Boolean.class)
                .beginControlFlow("try(\$T outputStream " + "= getContext().openFileOutput(\"\$L\", \$T.MODE_PRIVATE))", FileOutputStream.class, Constants.KEYSTORE_FILE, ctxClass)
                .addStatement("byte[] key = new byte[16]")
                .addStatement("new \$T().nextBytes(key)", SecureRandom)
                .addStatement("\$T secretKeySpec = new \$T(key,\"AES\")", SecretKeySpec.class, SecretKeySpec.class)
                .addStatement("\$T keyStore = \$T.getInstance(\$T.getDefaultType())", keyStoreClass, keyStoreClass, keyStoreClass)
                .addStatement("keyStore.load(null, null)")
                .addStatement("keyStore.setEntry(\$L, new \$T.SecretKeyEntry(secretKeySpec), null)", alias, keyStoreClass)
                .addStatement("keyStore.store(outputStream, null)")
                .addStatement("return true")
                .endControlFlow()
                .beginControlFlow("catch(\$T e)", IOException.class)
                .addStatement("e.printStackTrace()")
                .addStatement("return false")
                .endControlFlow()
                .beginControlFlow("catch(\$T | \$T | \$T e)", KeyStoreException.class, CertificateException.class, NoSuchAlgorithmException.class)
                .addStatement("e.printStackTrace()")
                .addStatement("return false")
                .endControlFlow()
                .build()
        return methodSpec
    }

    static MethodSpec saveIVMethod() {
        ClassName ctxClass = ClassName.get("android.content", "Context")
        return MethodSpec
                .methodBuilder(Constants.SET_IV_METHOD)
                .addParameter(byte[].class, "iv")
                .beginControlFlow("try(\$T outputStream " + "= getContext().openFileOutput(\"\$L\", \$T.MODE_PRIVATE))", FileOutputStream.class, Constants.IV_FILE, ctxClass)
                .addStatement("outputStream.write(iv)")
                .endControlFlow()
                .beginControlFlow("catch (\$T e)", IOException.class)
                .addStatement("e.printStackTrace()")
                .endControlFlow()
                .build()
    }

    static MethodSpec getIVMethod() {
        return MethodSpec
                .methodBuilder(Constants.GET_IV_METHOD)
                .returns(byte[].class)
                .beginControlFlow("try(\$T inputStream " + "= getContext().openFileInput(\"\$L\"))", FileInputStream.class, Constants.IV_FILE)
                .addStatement("byte[] iv = new byte[16]")
                .addStatement("inputStream.read(iv)")
                .addStatement("return iv")
                .endControlFlow()
                .beginControlFlow("catch (\$T e)", IOException.class)
                .addStatement("e.printStackTrace()")
                .addStatement("return null")
                .endControlFlow()
                .build()
    }

    static MethodSpec sendBroadcastMethod(MethodSpec methodSpec, String reqPerm, String local, String packageName, String busLogicMethodNm, String sendBrcstMethodNm, int reqCode) {
        ClassName packageMgr = ClassName.get("android.content.pm","PackageManager")
        ClassName intentCls = ClassName.get("android.content","Intent")
        CodeBlock.Builder intentSetter = CodeBlock.builder()
        if(!methodSpec.parameters.tail().isEmpty())
            intentSetter.addStatement("intent = " + Constants.OBJBUSLOGIC + "." + busLogicMethodNm + "(intent," + methodSpec.parameters.tail().collect{it.name}.join(",") + ")")
        CodeBlock.Builder sendBrdcastMethod = CodeBlock.builder()
        if(sendBrcstMethodNm == "sendOrderedBroadcast")
            sendBrdcastMethod.addStatement("getContext().\$L(intent, null)", sendBrcstMethodNm)
        else
            sendBrdcastMethod.addStatement("getContext().\$L(intent)", sendBrcstMethodNm)
        if(local == Constants.FALSE)
            return methodSpec
                    .toBuilder()
                    .beginControlFlow("if(getContext().checkSelfPermission(\"\$L\") == \$T.PERMISSION_GRANTED)", reqPerm, packageMgr)
                    .addStatement("\$T intent = new Intent(\$L)", intentCls, methodSpec.parameters[0].name)
                    .addCode(intentSetter.build())
                    .addCode(sendBrdcastMethod.build())
                    .nextControlFlow("else")
                    .addStatement("getActivity().requestPermissions(new String[]{\"\$L\"},\$L)", reqPerm, reqCode)
                    .endControlFlow()
                    .addStatement("return true")
                    .build()
        else
            return methodSpec
                    .toBuilder()
                    .beginControlFlow("if(getContext().checkSelfPermission(\"\$L\") == \$T.PERMISSION_GRANTED)", reqPerm, packageMgr)
                    .addStatement("\$T intent = new Intent(\$L)", intentCls, methodSpec.parameters[0].name)
                    .addStatement("intent.setPackage(\"\$L\")", packageName)
                    .addCode(intentSetter.build())
                    .addCode(sendBrdcastMethod.build())
                    .nextControlFlow("else")
                    .addStatement("getActivity().requestPermissions(new String[]{\"\$L\"},\$L)", reqPerm, reqCode)
                    .endControlFlow()
                    .addStatement("return true")
                    .build()
    }

    static MethodSpec setBrdcastIntentMethod(String methodNm, List<ParameterSpec> params) {
        MethodSpec.Builder mSpecbuilder = MethodSpec.methodBuilder(methodNm)
        ClassName intentCls = ClassName.get("android.content","Intent")
        mSpecbuilder.addModifiers(Modifier.PUBLIC)
        mSpecbuilder.returns(intentCls)
        mSpecbuilder.addParameter(intentCls, "intent")
        params.each {
            mSpecbuilder.addParameter(it)
        }
        mSpecbuilder.addStatement("throw new \$T()", UnsupportedOperationException.class)
        return mSpecbuilder.build()
    }

    static MethodSpec permRequestSendBrdcst(MethodSpec methodSpec, String callStmt, int reqCode) {
        return methodSpec
                .toBuilder()
                .beginControlFlow("if(requestCode == \$L)", reqCode)
                .addStatement(callStmt)
                .endControlFlow()
                .build()
    }

    static MethodSpec permRequestMethod() {
        return MethodSpec
                .methodBuilder(Constants.METHOD_ONREQPERM)
                .returns(void.class)
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class)
                .addParameter(int.class, "requestCode")
                .addParameter(String[].class, "permissions")
                .addParameter(int[].class, "grantResult")
                .build()
    }
}
