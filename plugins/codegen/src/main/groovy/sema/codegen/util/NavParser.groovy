package sema.codegen.util

import groovy.xml.Namespace
import sema.codegen.model.ActionModel
import sema.codegen.model.ArgumentModel
import sema.codegen.model.ConstraintModel
import sema.codegen.model.EventModel
import sema.codegen.model.FragmentModel
import sema.codegen.model.FunModel
import sema.codegen.model.RecvModel
import sema.codegen.model.WhiteListModel
import sema.codegen.model.WidgetModel

/**
 * Created by Joy on 7/29/19.
 */
class NavParser {

    static def androidNS = new Namespace("http://schemas.android.com/apk/res/android",'android')
    static def toolsNS = new Namespace("http://schemas.android.com/tools", 'tools')
    static def appNS = new Namespace("http://schemas.android.com/apk/res-auto", 'app')

    static ConstraintModel parseFunConstraint(Node funNode) {
        ConstraintModel constraintModel = new ConstraintModel()
        constraintModel.setOpl(parseFunNode(funNode))
        return constraintModel
    }

    static ConstraintModel parseNotNode(Node notNode) {
        ConstraintModel constraintModel = new ConstraintModel()
        constraintModel.setOp("not")
        if(notNode.children().size() == 1) {
            Node child = (Node) notNode.children().get(0)
            if(child.name().equals(Constants.TAG_FUN))
                constraintModel.setOpl(parseFunNode(child))
            else if(child.name().equals(Constants.TAG_AND))
                constraintModel.setOpr(parseBinaryNode(child,Constants.TAG_AND))
            else if(child.name().equals(Constants.TAG_OR))
                constraintModel.setOpr(parseBinaryNode(child,Constants.TAG_OR))
            return constraintModel
        }
        else throw new Exception("<not> can have at most 1 child")
    }

    static ConstraintModel parseBinaryNode(Node andNode, String op) {
        ConstraintModel constraintModel = new ConstraintModel()
        if(op.equals(Constants.TAG_AND)) constraintModel.setOp("and")
        if(op.equals(Constants.TAG_OR)) constraintModel.setOp("or")
        if(andNode.children().size() == 2) {
            andNode.children().each { Node child ->
                if(child.name().equals(Constants.TAG_FUN)) {
                    if(constraintModel.getOpl() == null)
                        constraintModel.setOpl(parseFunNode(child))
                    else
                        constraintModel.setOpr(parseFunConstraint(child))
                }
                else if(child.name().equals(Constants.TAG_AND))
                    constraintModel.setOpr(parseBinaryNode(child,Constants.TAG_AND))
                else if(child.name().equals(Constants.TAG_NOT))
                    constraintModel.setOpr(parseNotNode(child))
                else if(child.name().equals(Constants.TAG_OR))
                    constraintModel.setOpr(parseBinaryNode(child,  Constants.TAG_OR))

            }
            return constraintModel
        }
        else throw new Exception("<" + op + "> must have at most 2 children")
    }

    static FunModel parseFunNode(Node funNode) {
        def fname = funNode.attribute(androidNS.name)
        def res = funNode.attribute("resource")
        def method = funNode.attribute("method")
        def jsExec = funNode.attribute("allowJsExec")
        def pbe = funNode.attribute(Constants.ATTR_PBE)
        def perm = funNode.attribute(Constants.ATTR_FUN_REQ_PERM)
        def local = funNode.attribute(Constants.ATTR_FUN_LOCAL)
        def certPin = funNode.attribute(Constants.ATTR_CERT_PIN);
        List<String> primimitiveParms = new ArrayList<>()
        List<String> idParms = new ArrayList<>()
        List<String> varParms = new ArrayList<>()
        List<FunModel> funParms = new ArrayList<>()
        FunModel funModel = new FunModel()

        funNode.parm.eachWithIndex { Node node, index ->
            def appArg = node.attribute("arg")
            if(appArg != null && appArg.toString().startsWith(Constants.PREFIX_ID)) {
                if(Constants.RES_INTERNET.equals(res)
                        && Constants.RES_METHOD_LOADURL != method
                        && Constants.RES_METHOD_LOADDATA != method
                        && index == 0)
                    throw new RuntimeException("Protocol needs to be a constant")
                else if(Constants.RES_INTERNET.equals(res) && Constants.RES_METHOD_LOADURL != method && index == 1)
                    idParms.add(Constants.HOST_INDICATOR + appArg.toString().split("/")[1])
                else if((Constants.RES_EXT_STORE.equals(res) || Constants.RES_INT_STORE.equals(res)) && index == 0)
                    idParms.add(Constants.FILE_PATH_INDICATOR + appArg.toString().split("/")[1])
                else if(Constants.RES_EXT_STORE.equals(res) && index == 1)
                    idParms.add(Constants.DIR_INDICATOR + appArg.toString().split("/")[1])
                else if(Constants.RES_CIPHER.equals(res) && index == 0)
                    throw new RuntimeException("key must be retrieved from keystore")
                else if(Constants.RES_KEYSTORE == res && Constants.RES_METHOD_POST == method && index == 0 && pbe == Constants.TRUE) {
                    funModel.setPbe(Constants.TRUE)
                    idParms.add(Constants.SYMMETRIC_KEY_INDICTOR + appArg.toString().split("/")[1])
                }
                else
                    idParms.add(appArg.toString().split("/")[1])
            }
            else if(appArg != null && appArg.toString().startsWith(Constants.PREFIX_VAR)) {
                if(Constants.RES_INTERNET.equals(res)
                        && Constants.RES_METHOD_LOADURL != method
                        && Constants.RES_METHOD_LOADDATA != method
                        && index == 0)
                    throw new RuntimeException("Protocol needs to be a constant")
                else if(Constants.RES_INTERNET.equals(res) && Constants.RES_METHOD_LOADURL != method && index == 1)
                    varParms.add(Constants.HOST_INDICATOR + appArg.toString().split("/")[1])
                else if((Constants.RES_EXT_STORE.equals(res) || Constants.RES_INT_STORE.equals(res)) && index == 0)
                    varParms.add(Constants.FILE_PATH_INDICATOR + appArg.toString().split("/")[1])
                else if(Constants.RES_EXT_STORE.equals(res) && index == 1)
                    varParms.add(Constants.DIR_INDICATOR + appArg.toString().split("/")[1])
                else if(Constants.RES_CIPHER.equals(res) && index == 0)
                    throw new RuntimeException("key must be retrieved from keystore")
                else if(Constants.RES_KEYSTORE == res && Constants.RES_METHOD_POST == method && index == 0 && pbe == Constants.TRUE) {
                    funModel.setPbe(Constants.TRUE)
                    varParms.add(Constants.SYMMETRIC_KEY_INDICTOR + appArg.toString().split("/")[1])
                }
                else
                    varParms.add(appArg.toString().split("/")[1])
            }
            else if(appArg != null && appArg.toString().startsWith(Constants.PREFIX_FUN)
                    && node.fun.size() == 1) {
                if(Constants.RES_INTERNET.equals(res)
                        && Constants.RES_METHOD_LOADURL != method
                        && Constants.RES_METHOD_LOADDATA != method
                        && index == 0)
                    throw new RuntimeException("Protocol needs to be a constant")
                else if(Constants.RES_INTERNET.equals(res) && Constants.RES_METHOD_LOADURL != method && index == 1) {
                    FunModel funModel1 = parseFunNode(node.fun.get(0))
                    funModel1.setName(Constants.HOST_INDICATOR + funModel1.getName())
                    funParms.add(funModel1)
                }
                else if((Constants.RES_EXT_STORE.equals(res) || Constants.RES_INT_STORE.equals(res)) && index == 0) {
                    FunModel funModel1 = parseFunNode(node.fun.get(0))
                    funModel1.setName(Constants.FILE_PATH_INDICATOR + funModel1.getName())
                    funParms.add(funModel1)
                }
                else if(Constants.RES_EXT_STORE.equals(res) && index == 1) {
                    FunModel funModel1 = parseFunNode(node.fun.get(0))
                    funModel1.setName(Constants.DIR_INDICATOR + funModel1.getName())
                    funParms.add(funModel1)
                }
                else if(Constants.RES_CIPHER.equals(res) && index == 0) {
                    FunModel funModel1 = parseFunNode(node.fun.get(0))
                    funModel1.setName(Constants.SYMMETRIC_KEY_INDICTOR + funModel1.getName())
                    funParms.add(funModel1)
                }
                else if(Constants.RES_KEYSTORE == res && Constants.RES_METHOD_POST == method && index == 0 && pbe == Constants.TRUE)
                    throw new RuntimeException("unsafe usage of key")
                else
                    funParms.add(parseFunNode(node.fun.get(0)))
            }

            else {
                if(Constants.RES_INTERNET.equals(res)
                        && Constants.RES_METHOD_LOADURL != method
                        && Constants.RES_METHOD_LOADDATA != method
                        && index == 0)
                    primimitiveParms.add(Constants.PROTOCOL_INDICATOR + appArg.toString())
                else if(Constants.RES_INTERNET.equals(res) && Constants.RES_METHOD_LOADURL != method && index == 1)
                    primimitiveParms.add(Constants.HOST_INDICATOR + appArg.toString())
                else if((Constants.RES_EXT_STORE.equals(res) || Constants.RES_INT_STORE.equals(res)) && index == 0)
                    primimitiveParms.add(Constants.FILE_PATH_INDICATOR + appArg.toString())
                else if(Constants.RES_EXT_STORE.equals(res) && index == 1)
                    primimitiveParms.add(Constants.DIR_INDICATOR + appArg.toString())
                else if(Constants.RES_CIPHER.equals(res) && index == 0)
                    throw new RuntimeException("unsafe usage of key")
                else if(Constants.RES_KEYSTORE == res && Constants.RES_METHOD_POST == method && index == 0 && pbe == Constants.TRUE)
                    throw new RuntimeException("unsafe usage of key")
                else
                    primimitiveParms.add(appArg.toString())
            }
        }

        List<String> whiteListPatterns = new ArrayList<>()
        funNode.whitelist.pattern.each {
            String casePattern = it.attribute("case")
            whiteListPatterns.add(casePattern)
        }

        funModel.setName(fname.toString())
        funModel.setResource(res.toString())
        funModel.setMethod(method.toString())
        funModel.setRequiredPerm(perm.toString())
        if(local == Constants.TRUE) funModel.setLocal(Constants.TRUE)
        else funModel.setLocal(Constants.FALSE)
        if(jsExec == Constants.TRUE) funModel.setAllowJS(true)
        else funModel.setAllowJS(false)
        if(!whiteListPatterns.isEmpty()) {
            WhiteListModel whiteListModel = new WhiteListModel()
            whiteListModel.setPatterns(whiteListPatterns)
            funModel.setWhiteListModel(whiteListModel)
        }
        if(!primimitiveParms.isEmpty()) funModel.setPrimitives(primimitiveParms)
        if(!idParms.isEmpty()) funModel.setIdValues(idParms)
        if(!varParms.isEmpty()) funModel.setVariables(varParms)
        if(!funParms.isEmpty()) funModel.setFuns(funParms)
        return funModel
    }

    static ArgumentModel parseArgumentNode(Node argNode) {
        ArgumentModel argumentModel = new ArgumentModel()
        def name = argNode.attribute(androidNS.name) != null ?
                argNode.attribute(androidNS.name).toString() : ""
        argumentModel.setName(name)
        def val = argNode.attribute("value") != null ?
                argNode.attribute("value").toString() : ""
        if(val.startsWith(Constants.PREFIX_ID))
            argumentModel.setIdValue(val.split("/")[1])
        else if(val.startsWith(Constants.PREFIX_VAR))
            argumentModel.setVarValue(val.split("/")[1])
        else if(val.startsWith(Constants.PREFIX_FUN)
                && argNode.fun.size() == 1)
            argumentModel.setFunValue(parseFunNode(argNode.fun.get(0)))
        else
            argumentModel.setPrimitiveValue(val)
        return argumentModel
    }

    static WidgetModel parseWidgetNode(Node widgetNode) {
        WidgetModel widgetModel = new WidgetModel()
        def wid = (widgetNode.attribute("wid") != null
                && widgetNode.attribute("wid").toString().startsWith(Constants.PREFIX_ID))?
                widgetNode.attribute("wid").toString().split("/")[1] : ""
        widgetModel.setWid(wid)
        def val = widgetNode.attribute("value") != null ?
                widgetNode.attribute("value").toString() : ""
        if(val.startsWith(Constants.PREFIX_VAR))
            widgetModel.setVariableVal(val.split("/")[1])
        else if(val.startsWith(Constants.PREFIX_FUN)
                && widgetNode.fun.size() == 1)
            widgetModel.setFunModel(parseFunNode(widgetNode.fun.get(0)))
        else if(!val.trim().equals(""))
            widgetModel.setPrimitiveVal(val)
        return widgetModel
    }

    static ActionModel parseActionNode(Node anode) {
        ActionModel actionModel = new ActionModel()
        def actionId = (anode.attribute(androidNS.id) != null
                && anode.attribute(androidNS.id).toString().contains("/")) ?
                anode.attribute(androidNS.id).toString().split('/')[1] : ""
        actionModel.setActionId(actionId)
        def destId = (anode.attribute(appNS.destination) != null
                && anode.attribute(appNS.destination).toString().contains("/")) ?
                anode.attribute(appNS.destination).toString().split('/')[1] : ""
        actionModel.setDestId(destId)
        if(anode.attribute("gesture") != null)
            actionModel.setGesture(anode.attribute("gesture").toString())
        if(anode.attribute("widgetOn") != null
                && anode.attribute("widgetOn").toString().contains("/"))
            actionModel.setWidgetOn(anode.attribute("widgetOn").toString().split("/")[1])
        if(anode.constraint.fun.size() == 1) {
            Node funNode = anode.constraint.fun.get(0)
            ConstraintModel constraintModel = new ConstraintModel()
            constraintModel.setOpl(parseFunNode(funNode))
            actionModel.setConstraintModel(constraintModel)
        }
        if(anode.constraint.not.size() == 1) {
            Node notNode = anode.constraint.not.get(0)
            actionModel.setConstraintModel(parseNotNode(notNode))
        }
        if(anode.constraint.and.size() == 1) {
            Node notNode = anode.constraint.and.get(0)
            actionModel.setConstraintModel(parseBinaryNode(notNode,Constants.TAG_AND))
        }
        if(anode.constraint.or.size() == 1) {
            Node notNode = anode.constraint.and.get(0)
            actionModel.setConstraintModel(parseBinaryNode(notNode,Constants.TAG_OR))
        }
        List<ArgumentModel> actionArgs = new ArrayList<>()
        anode.argument.each { Node actionArgNode ->
            actionArgs.add(parseArgumentNode(actionArgNode))
        }
        if(!actionArgs.isEmpty()) actionModel.setArgs(actionArgs)
        return actionModel
    }

    static FragmentModel parseFragmentNode(Node node) {
        FragmentModel fragmentModel = new FragmentModel();
        def id = (node.attribute(androidNS.id) != null
                && node.attribute(androidNS.id).contains("/")) ?
                node.attribute(androidNS.id).toString().split('/')[1] : ""
        if(id.equals("")) throw new Exception("fragment id is mandatory")
        fragmentModel.setFragId(id)
        def layout = (node.attribute(toolsNS.layout) != null
                && node.attribute(toolsNS.layout).contains("/")) ?
                node.attribute(toolsNS.layout).toString().split('/')[1] : ""
        if(layout.equals("")) throw new Exception("layout id is mandatory")
        fragmentModel.setLayoutId(layout)
        List<ArgumentModel> screenArgs = new ArrayList<>()
        node.argument.each { Node argNode ->
            screenArgs.add(parseArgumentNode(argNode))
        }

        node.deepLink.each { Node deepLinkNode ->
            def uri = deepLinkNode.attribute(appNS.get("uri"))
            if(uri != null
                    && uri.toString().contains("{")
                    && uri.toString().contains("}")) {
                def uriArg = uri.toString()
                        .substring(uri.toString().indexOf("{")+1
                        , uri.toString().lastIndexOf("}"))
                ArgumentModel argumentModel = new ArgumentModel();
                argumentModel.setName(uriArg)
                screenArgs.add(argumentModel);
            }
        }

        if(!screenArgs.isEmpty()) fragmentModel.setArgs(screenArgs)

        List<WidgetModel> widgets = new ArrayList<>()
        node.widget.each { Node widgetNode ->
            widgets.add(parseWidgetNode(widgetNode))
        }
        if(!widgets.isEmpty()) fragmentModel.setWidgets(widgets)
        List<ActionModel> actions = new ArrayList<>()
        node.action.each { Node anode ->
            actions.add(parseActionNode(anode))
        }
        if(!actions.isEmpty()) fragmentModel.setOutActions(actions)
        return fragmentModel
    }

    static EventModel parseEvent(Node node) {
        def id = (node.attribute(androidNS.id) != null
                && node.attribute(androidNS.id).contains("/")) ?
                node.attribute(androidNS.id).toString().split('/')[1] : ""
        String name = node.attribute(androidNS.name)
        String priority = node.attribute("priority")
        EventModel eventModel = new EventModel()
        eventModel.setId(id)
        eventModel.setName(name)
        if(priority != "") eventModel.setPriority(priority)
        node.fun.each { Node funNode ->
            eventModel.setFunModel(parseFunNode(funNode))
        }
        return eventModel
    }

    static RecvModel parseRecvNode(Node node) {
        def id = (node.attribute(androidNS.id) != null
                && node.attribute(androidNS.id).contains("/")) ?
                node.attribute(androidNS.id).toString().split('/')[1] : ""
        String access = node.attribute(Constants.ATTR_RECV_ACCESS)
        String permTyp = node.attribute(Constants.ATTR_RECV_PERMTYP)
        String permNm = node.attribute(Constants.ATTR_RECV_PERMNM)
        RecvModel recvModel = new RecvModel()
        recvModel.setId(id)
        recvModel.setProtectionLevel(access)
        recvModel.setPermType(permTyp)
        recvModel.setPermName(permNm)
        List<EventModel> events = new ArrayList<>()
        node.event.each { Node eventNode ->
            events.add(parseEvent(eventNode))
        }
        if(!events.isEmpty()) recvModel.setEvents(events)
        return recvModel
    }
}
