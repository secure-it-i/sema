## Summary
Apps that use a Block Cipher algorithm in ECB mode for encrypting sensitive information are vulnerable to exposing leaking information.  *Corresponding Ghera Benchmark: [BlockCipher-ECB-InformationExposure](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Crypto/BlockCipher-ECB-InformationExposure-Lean/)*

## Description of the vulnerability
*Context*: Android apps can use block cipher algorithms to encrypt sensitive information. The `Cipher` API in Android enable developers to specify the block cipher algorithm to use along with its mode.

*Issue*: If the app uses the block cipher algorithm AES in ECB mode to encrypt sensitive information, then an attacker can break the encryption to get access to the confidential information. When an app uses AES algorithm, the app can select ECB mode implicitly by accepting the default mode (which is ECB) or explicitly specifying ECB mode.

*Resolution*: SeMA does not allow users to specify which block cipher algorithm/transformation to use for cipher operations. Instead, SeMA automatically selects a more secure block cipher algorithm. Therefore, an app with this vulnerability cannot be created with SeMA.

## Building Secure with SeMA--
The storyboard of *Secure (see main_nav.xml)* app is as follows:

- When the app is started, *form* screen is displayed to the user. Before loading the *form* screen, the app generates a random *secret key* and stores it in a Keystore, which can be accessed only by the app.
- In the *form* screen, a user enters a student's name and her grade. Upon clicking the *Submit* button, the student's name and her grade encrypted using *secret key* are stored in a file in external storage.
- The user navigates to the *msg* screen if the student name and grade is saved successfully.
- The app has a fragment named *share* that can be triggered by another app via a *deep link*.
- In the *share* fragment, a user enters a student's name and clicks on the *Get Grade* button to retrieve the student's grade from external storage, decrypt it, and send it to the requesting app.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags no errors or warnings because
1. the *Secure* app encrypts sensitive information before saving it to external storage, and
2. the confidential information collected by *Secure* can be extracted by another app only by explicity user action, i.e., a click of a button.

Click on the gradle plugin *generateCodeFromNav* or use the following command to generate code from the navigation graph:

`$ ./gradlew generateCodeFromNav`

The generated source code for selecting the Block Cipher algorithm is in the *encryptGrade* method in *app/build/generated/source/../FormFragment.java*.

## Discussion
SeMA prevents the vulnerability because the generated code ensures that the transformation used in cipher operations is *AES/GCM*, a more secure block cipher transformation, instead of *AES/ECB*. SeMA uses *AES/GCM* as the default block cipher algorithm because it is known to be secure [1].  However, SeMA can be altered to offer other safe choices of block cipher algorithms.

*Secure* app uses a more secure block cipher algorithm/transformation to perform cipher operations. Therefore, the secret key used in these operations cannot be inferred by an attacker observing the cipher operations.

## References
1. [How to choose an Authenticated Encryption mode? Mathew Green](https://blog.cryptographyengineering.com/2012/05/19/how-to-choose-authenticated-encryption/)
