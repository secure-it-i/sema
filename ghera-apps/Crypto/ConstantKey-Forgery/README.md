## Summary
Apps that store encryption keys in the source code are vulnerable to forgery attacks and information leaks. *Corresponding Ghers benchmark: [ConstantKey-ForgeryAttack-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Crypto/ConstantKey-ForgeryAttack-Lean/)*

## Description of the vulnerability
*Context*: Android apps can use block cipher algorithms to encrypt sensitive information using the `Cipher` API.

*Issue*: The `Cipher` API expects a key. If such keys are saved in the source code, then an attacker can access it and abuse it.

*Example*: The *Benign* app demonstrates the vulnerability. *Benign* allows the user to enter a student's name and grade in a form. This grade is encrypted using a constant key. The encrypted grade and the name are stored in a file in the external storage of the device. A malicious app with access to external storage can use the key, after discovering it from *Benign* app's source code, to extract the grade.

## Building Benign app with SeMA
The storyboard (see *main_nav.xml*) of *Benign* is as follows:

- When the app is started, the user is shown the *form* screen, where she enters a student's name and her grade. When the user clicks on the *Submit* button, the student's name and her grade are stored in a file in external storage. The grade is encrypted with a constant key before storing it.
- The user navigates to the *msg* screen if the student name and grade is saved successfully.
- The app has a fragment *share*, which can be triggered from outside the app via a *deep link*.
- In *share*, a user enters a student's name and clicks on the *Get Grade* button, which retrieves the student's grade, decrypts it and sends it to the requesting app.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows:

`$ ./gradlew lintDebug`

The analysis will raise an error because *Benign* uses a constant key to encrypt data when the user clicks on *submit* button in the *form* screen.

## Building Secure with SeMA
We refine the storyboard in *Benign* to create *Secure*. The storyboard (see *main_nav.xml*) of *Secure* is as follows:

- When the app is started, *form* screen is displayed to the user. Before loading the *form* screen, the app generates a random *secret key* and stores it in a KeyStore with restricted access.
- In the *form* screen, a user enters a student's name and grade. When the user clicks on the *Submit* button, the student's name and grade are stored in a file in external storage. The grade is encrypted with the key in the KeyStore before storing it.
- The user navigates to the *msg* screen if the student name and grade is saved successfully.
- The app has a fragment *share*, which can be triggered from outside the app via a *deep link*.
- In *share*, a user enters a student's name and clicks on the *Get Grade* button, which retrieves the student's grade, decrypts it and sends it to the requesting app.

The analysis will report no errors or warnings because the key used to perform cipher operations is stored safely in a KeyStore.

## Discussion
SeMA disallows/prevents the developer from using a constant key or malicious key to perform cipher operations. Additionally, SeMA ensures that the key is stored in a KeyStore.

*Secure* uses a randomly generated secret key to perform cipher operations. This key is saved in a KeyStore. Assuming that the KeyStore cannot be accessed without authorization, *Secure* cannot be exploited to expose the secret key.
