## Summary
Apps that use constant salt for Password-based Encryption are vulnerable to information exposure via dictionary attacks. *Corresponding Ghera benchmark: [PBE-ConstantSalt-InformationExposure-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Crypto/PBE-ConstantSalt-InformationExposure-Lean/)*

## Description of the vulnerability
*Context*: Password-based Encryption (PBE) is an encryption technique where the key used to encrypt/decrypt data is derived from a user-provided password. The password is supplemented by a salt and an integer and is hashed by a secret key. The result is prepended to a salt and an integer and is again hashed. This process can be repeated any number of times to obtain the symmetric key.

*Issue*: If the salt being used is constant, then the symmetric key can be re-created by an attacker if the attacker has access to the password. An attacker can precompute a dictionary of symmetric keys for known passwords and use them to decrypt information.

*Resolution*: SeMA inhibits developers from specifying a salt for PBE-based cipher operations. The required salt is generated randomly by SeMA's code generation algorithm.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, it takes a user-provided text and generates a secret key based on the text and a random salt, and stores it in a KeyStore with restricted access.
- In the *form* screen, a user enters a student's name and her grade. When the user clicks on the *Submit* button, the student's name and her grade are stored in a file in external storage. The grade is encrypted before it is stored in the file.
- The user navigates to the *msg* screen if the student name and grade is saved successfully.
- The app has a fragment *share* that can be triggered from outside the app via a *deep link*.
- In *share*, a user enters a student's name and clicks on the *Get Grade* button, which retrieves the student's grade, decrypts it, and sends it to the requesting app.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags no errors or warnings because a constant salt is not used to perform PBE-based cipher operations.

Click on the Gradle plugin *generateCodeFromNav* or use the following command to generate code from the navigation graph:

`$ ./gradlew generateCodeFromNav`

The generated source code for random salt generation is in the *encryptGrade* method in *app/build/generated/source/FormFragment.java*

## Discussion
The generated source code uses a random salt to generate the key used for PBE. In addition to the random salt, SeMA selects *AES/GCM* by default as the algorithm for performing cipher operations. Finally, SeMA stores the PBE-based key in a KeyStore, which can only be accessed by *Secure*. Due to these reasons, the password-based key cannot be inferred by a malicious app.

The decision of not allowing developers to choose the salt is valid because safe PBE-based encryption should use a random salt. Hence, there is no need to burden developers with making that choice at the storyboard level.

The *Secure* app cannot be exploited by a malicious app to infer the secret key used in PBE based encryption. However, if an attacker gets access to the physical device, then she can access the KeyStore of *Secure* and extract the secret key.
