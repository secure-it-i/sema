## Summary
Apps that store encryption keys without any protection parameter in a `KeyStore` accessible by other apps are vulnerable to information exposure. *Corresponding Ghera benchmark: [ExposedCredentials-InformationExposure-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Crypto/ExposedCredentials-InformationExposure-Lean/)*

## Description of the vulnerability
*Context*: Android apps can use `KeyStore` to store encryption keys. `KeyStore` provides `setEntry` API to store a key entry. Along with the alias and the key, `setEntry` API takes an instance of `ProtectionParameter` as an argument to protect the contents of the entry.

*Issue*: If an entry is stored in a `KeyStore` with `null` as the `ProtectionParameter` argument to `setEntry` API, then any app with access to the `KeyStore` and aware of the alias can retrieve the entry. This issue can be mitigated by using a password-based implementation of `ProtectionParameter` or restricting access to the `KeyStore`.

*Resolution*: SeMA places the `KeyStore` of an app in the app's internal storage by default.  Hence, the `KeyStore` of an app created with SeMA cannot be accessed by other apps.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *form* screen is displayed to the user. Before loading the *form* screen, the app generates a random *secret key* and stores it in a `KeyStore`, which can be accessed only by *Secure*.
- In the *form* screen, a user enters a student's name and her grade. When the user clicks on the *Submit* button, the student's name and her grade are stored in a file in external storage. The grade is encrypted with the key in the `KeyStore` before it is stored.
- The user navigates to the *msg* screen if the student name and grade is saved successfully.
- The app has a fragment *share* that can be triggered from outside the app via a *deep link*.
- In *share*, a user enters a student's name and clicks on the *Get Grade* button, which retrieves the student's grade, decrypts it (using the `KeyStore` key), and sends it to the requesting app.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags no errors or warnings because the `KeyStore` that stores the *Secure* app's secret key cannot be accessed by other apps.

Click on the Gradle plugin *generateCodeFromNav* or use the following command to generate code from the navigation graph:

`$ ./gradlew generateCodeFromNav`

The generated source code for `KeyStore` is in the `saveKey` method in *app/build/generated/source/../FormFragment.java*

## Discussion
Android suggests that that the `KeyStore` should be placed in a secure space (e.g. internal storage) so it is not accessible by other apps. However, there might be instances where the `KeyStore` needs to be shared between apps. In such cases, the app will have to designed such that the `KeyStore` is shared in a safe manner (e.g. via a protected Content Provider).

*Secure* cannot be exploited by a malicious app to access the credentials used by *Secure*. However, if the device is physically compromised then the attacker will be able to access the credentials stored in the KeyStore.
