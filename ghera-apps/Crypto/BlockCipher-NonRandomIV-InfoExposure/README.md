## Summary
Apps that use a block cipher algorithm in CBC mode with a non-random Initialization Vector to encrypt sensitive information are vulnerable to leaking sensitive information. *Corresponding Ghera Benchmark: [BlockCipher-NonRandomIV-InformationExposure](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Crypto/BlockCipher-NonRandomIV-InformationExposure-Lean/)*

## Description of the vulnerability
*Context*: Android apps can use block cipher algorithms to encrypt sensitive information. The `Cipher` API enables developers to specify which block cipher algorithm to use and in which mode.

*Issue*: If the app uses the block cipher algorithm AES in CBC mode, then the app needs to provide an Initialization Vector (IV), which will be XOR-ed with the data that is to be encrypted. Reusing IV to encrypt identical data with a symmetric key will yield identical encrypted results. An attacker with access to the encrypted data can then infer the actual data by just analyzing and comparing the encrypted data.

*Resolution*: In SeMA, users cannot specify IVs in storyboards. Instead, SeMA generates code to use *AES/GCM* as the default algorithm for block cipher operations. *AES/GCM* uses random IV by default.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *form* screen is displayed to the user. Before loading the *form* screen, the app generates a random *secret key* and stores it in a KeyStore, which can be accessed only by *Secure*.
- In the *form* screen, a user enters a student's name and her grade. When the user clicks on the *Submit* button, the student's name and her grade are encrypted using the key from the KeyStore and stored in a file in external storage.
- The user navigates to the *msg* screen if the student name and grade is saved successfully.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags no errors or warnings because a random IV is used to perform cipher operations, which is safe.

Click on the gradle plugin *generateCodeFromNav* or use the following command to generate code from the navigation graph:

`$ ./gradlew generateCodeFromNav`

The generated source code for random IV generation is in the *encryptGrade* method in *app/build/generated/source/../FormFragment.java*.

## Discussion
SeMA chooses *AES/GCM* for cipher operations, which uses a random IV by default. While *AES/GCM* with random IV is a safe choice, it might not be the ideal choice due to a variety of reasons (e.g., performance). However, SeMA can be extended to include all safe choices that use a random IV to perform cipher operations.

*Secure* does not allow a malicious app to infer the secret key used in block cipher operations due to the use of non-random IV in *AES/GCM*.
