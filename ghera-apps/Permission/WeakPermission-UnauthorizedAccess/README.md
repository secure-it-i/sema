## Summary
Apps that use weak permissions to protect exported components are akin to apps that do not protect exported components at all.  *Corresponding Ghera Benchmark: [WeakPermission-UnauthorizedAccess-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Permission/WeakPermission-UnauthorizedAccess-Lean/)*

## Description of the  Vulnerability
*Context*: Android allows developers to create/define a permission and use it to protect exported components. A permission can have four protection levels.

1. `normal`. A permission with protection level normal is granted by Android to the requesting application at install time without explicitly asking the user.
2. `dangerous`. A permission with protection level dangerous is granted by Android to the requesting application only if the user has explicitly approved of the request.
3. `signature`. A permission with protection level signature is granted by Android to the requesting application only if the requesting application is signed with the same certificate that the application, where the permission is defined, is signed with.
4. `signatureOrSystem`. A permission with protection level signatureOrSystem is granted by Android to the requesting application only if the requesting application is in the Android system image or if it is signed with the same certificate that the application, where the permission is defined, is signed with.

*Issue*: If an exported component is protected with a `normal` permission, then any app specifying to use this permission will be granted the permission by Android when the app is installed into the device.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* app has a `BroadcastRreceiver` *recv* that performs privileged operations and is protected with a *normal* permission P. A malicious app using P and installed in the device can use *recv* to perform privileged operations without the user's knowledge or consent.

## Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *home_id* is displayed to the user.
- In the *form_id* fragment, the user clicks on the *Send Email* button to send a broadcast message to the receiver *recv*.
- BroadcaseReceiver *recv* is defined in the app. It is protected with a normal permission and registered to receive two events, X and Y. If event X is received, then it sends an email. If event Y is received, then it sends an SMS.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis raises an error because the receiver *recv* can be accessed by a malicious app that uses the normal permission protecting *recv*. The malicious app can either trigger event X or Y, access *recv*, and use *recv* to send an email or an SMS without the user's knowledge or consent.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *home_id* is displayed to the user.
- In the *form_id* fragment, the user clicks on the *Send Email* button to send a broadcast message to the receiver *recv*.
- BroadcaseReceiver *recv* is defined in the app. It is protected with a *dangerous* permission, and registered two receive two events, X and Y. If event X is received, then it sends an email. If event Y is received, then it sends an SMS.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis raises no error because the receiver *recv* is accessed only by those entities that have been explicitly granted access by the app's user. Hence, such entities have the required permission to perform privileged operations on the user's behalf.

# Discussion
SeMA disallows apps to define components that perform privileged operations, i.e., operations that use Android's protected capabilities (e.g., storage), without protecting them with strong permissions.

The *Secure* app does not have components that perform privileged operations and are protected by *normal* or no permissions; hence, the app is secure.
