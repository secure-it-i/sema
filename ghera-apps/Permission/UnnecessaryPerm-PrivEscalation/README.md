## Summary
Apps that use more permissions than they need are vulnerable to privilege escalation attacks. *Corresponding Ghera benchmark: [UnnecesaryPerms-PrivEscalation-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Permission/UnnecesaryPerms-PrivEscalation-Lean/)*

## What is the Vulnerability
*Context*: The use of protected features on Android devices requires explicit permissions, and developers occasionally ask for more permissions than necessary.

*Issue*: If an app asks for more permissions than necessary, then the permissions can be used by malicious apps that do not have those permissions to invoke protected APIs.

*Resolution*: SeMA generates the permission requests for the permissions required for the app based on the resources used by the app. Therefore, SeMA inhibits apps from requesting unnecessary permissions.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *home_id* is displayed to the user.

- In the *form_id* fragment, the user clicks on the *Send Email* button to send a broadcast message to the receiver *recv*.

- Receiver *recv* is defined in the app. It is protected with a "normal" permission and registered two receive two events, X and Y. If event X is received, then it sends an email. If event Y is received, then it sends an SMS.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis raises no errors because unnecessary permissions are not requested.

The code generation generates permission requests for sending a broadcast to *recv*, sending an email, and sending an SMS. No additional permissions are requested.

# Discussion
SeMA allows apps to use a pre-defined list of resources based on the capabilities provided by the Android platform (e.g., network). The permissions are requested based on the resources used by the app.

The *Secure* app does not request unnecessary permissions. However, the developer can add more permission requests in the *manifest* file, which may not be necessary for the app. 
