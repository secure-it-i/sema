## Summary
Apps that allow copying of sensitive information to clipboard may leak information. *Corresponding Ghera benchmark: [ClipboardUse-InformationExposure-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/System/ClipboardUse-InformationExposure-Lean/)*

## Description of the vulnerability
*Context*: Android clipboard is accessible by every app on the device. Widgets like EditText by default allow users to copy their contents onto the clipboard. For example, to copy the data, users can long-click on EditText and then select copy from dialog-box. By doing so, the contents of EditText widget will be copied onto clipboard. When contents of the clipboard changes, apps that have registered for notifications about changes to the clipboard (via addPrimaryClipChangedListener) will be notified and they will have access to the contents of the clipboard.

*Issue*: Since every app has access to the content of the clipboard independent of the source (app) of the content, malicious apps can steal information by merely accessing the clipboard.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* app writes the device's unique ID to the clipboard. A malicious app with access to the clipboard can extract the device ID from it.

# Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, when the user clicks on the *Copy Device ID* button, the device's unique ID is written to the clipboard.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because sensitive information (device ID) is written to the clipboard, which can be accessed by  a malicious app to extract the device ID.

# Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, when the user clicks on the *Copy Device ID* button, the device's unique ID is first encrypted and then written to the clipboard.

The analysis reports no errors because *Secure* encrypts the sensitive information before writing to the clipboard. Therefore, a malicious app with access to the clipboard will have to break the encryption to extract the device ID.



## Discussion
SeMA disallows apps to write sensitive information to the clipboard. If sensitive information has to be written to the clipboard, then it will have to be encrypted and declared as not sensitive.

The *Secure* app does not write sensitive information to the clipboard without encrypting it. So, a malicious app will not be able to extract sensitive information from *Secure* via the clipboard.
