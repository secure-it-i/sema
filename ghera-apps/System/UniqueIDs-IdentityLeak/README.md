## Summary
Apps that use device identifiers to uniquely identify app instances are vulnerable to exposing device-specific sensitive information. *Corresponding Ghera benchmark: [UniqueIDs-IdentityLeak-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/System/UniqueIDs-IdentityLeak-Lean/)*

## Description of the vulnerability
*Context*: An app can collect device identifiers through APIs like TelephonyManager and BluetoothAdapter. Such identifiers are often used by app developers to uniquely identify app instances.

*Issue*: Collecting device identifiers could lead to exposing sensitive data related to the device which can be used by malicious actors to track the device.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* app saves the device's unique ID to a file in external storage. A malicious app with access to external storage can extract the device ID.

# Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, when the user clicks on the *Save Device ID* button, the device's unique ID is saved in external storage.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because sensitive information (device ID) is saved to external storage, which can be accessed by a malicious app to extract the device ID.

# Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, when the user clicks on the *Copy Device ID* button, the device's unique ID is first encrypted and then saved to external storage.

The analysis reports no errors because *Secure* encrypts the sensitive information before saving to external storage. Therefore, a malicious app with access to external storage will have to break the encryption to extract the device ID.


## Discussion
SeMA disallows apps to write sensitive system information to external storage. If such information needs to be shared then it has to be done in a safe manner (e.g., encryption).

The *Secure* app does not write sensitive system information to external storage without first encrypting it. So, a malicious app will not be able to extract such information from *Secure*.
