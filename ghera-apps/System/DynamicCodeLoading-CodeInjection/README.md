## Summary
Apps that rely on dynamic code loading without verifying the integrity and authenticity of the loaded code may be vulnerable to code injection. *Corresponding Ghera benchmark: [DynamicCodeLoading-CodeInjection-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/System/DynamicCodeLoading-CodeInjection-Lean/)*

## Description of the vulnerability
*Context*: Since Android apps are JVM-based, they can use dynamic class loading features of the JVM (e.g., Class.forName(), Class.getDeclaredField()) to dynamically load and execute code that is not packaged with the app.

*Issue*: Since the apps can load classes from local archives (via PathClassLoader) or remote archives (via URLClassLoader), malicious actors can change such archives and affect the behavior of apps that use the archive. Apps can protect themselves from such influences by either avoiding dynamic code loading (by making such code fragments part of the app) or using approriate checks to verify both integrity and authenticity of the archives at loading time.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* app uses dynamic code loading to generate information that is displayed to the user in a *TextView*. The code is loaded from a jar file in external storage. A malicious app with access to external storage can inject code into the jar file.  

# Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, the *disp* widget is loaded with information, using a dynamically loaded fragment of code from external storage.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because *Benign* uses code from external storage to initialize the widget *disp*. Files in external storage can be accessed by a malicious app, which can lead to malicious code injection in this case.

# Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, the *disp* widget is loaded with information, using a *verified* dynamically loaded fragment of code from external storage.

The analysis reports no errors because *Secure* specifies that the code is verified (by setting a flag in *main_nav.xml*) before it is loaded and executed.


## Discussion
SeMA disallows apps to load code from untrusted sources dynamically. However, it allows dynamic code loading from an untrusted source, if the code is signed with a trusted certificate chain. The logic for verifying the certificate chain needs to be implemented by the developer. So, if the verification is implemented incorrectly, then the app can still be vulnerable.

SeMA does not generate the code for verifying the certificate chain because there are multiple ways to verify a certificate chain. For example, a developer might choose to verify the certificate chain by verifying only the root certificate in the chain, or she might choose to verify at least one certificate in the chain.    

The *Secure* app does allow dynamic code loading from untrusted source. If an untrusted source is used to load code, then the code needs to be verified. However, if the verification is incorrect, then the *Secure* app will not be immune to code injection attacks via dynamic loading.
