## Summary
Apps that do not validate resource requests before loading them into a `WebView` are vulnerable to MitM attacks. *Corresponding Ghera benchmark: [WebViewInterceptRequest-MITM-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Web/WebViewInterceptRequest-MITM-Lean/)*

## Description of the vulnerability
*Context*: Android allows apps to load resources (e.g., JavaScript, CSS, and files) in a web page in a `WebView`, and control the resources loaded in a webpage via the `shouldInterceptRequest()` method in `WebViewClient`. If the response object returned by `shouldInterceptRequest()`, then the `WebView` is loaded with the web page containing the resource that was requested. But, if `shouldInterceptRequest()` returns a non-null response, then the `WebView` is loaded with the web page containing the non-null response.

*Issue*: the app does not validate the resource in `shouldInterceptRequest` method of `WebViewClient`. Hence, a `WebView` loads any resource.

*Example*: The *Benign* app has a WebView (that allows JS execution) to display web content from a URL. The WebView does not validate the resources in a URL before loading the URL. A malicious JS URL embedded in a webpage loaded in the WebView can lead to remote code injection attacks.

## Building Benign with SeMA
The storyboard of *Benign (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

- The WebView is configured to execute JS.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags an error because the *Benign* app loads a URL without verifying the resources embedded in the URL. A URL with malicious JS embedded in it can use the WebView to execute arbitrary JS code.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

- The WebView is configured with a whitelist of URL patterns of trusted resources, which is used to verify resources in a URL loaded in the WebView. If a resource URL does not match the trusted URL patterns, then the request to load the web page is rejected by the WebView.

- The WebView is also configured to execute JS.

The analysis does not report any error because the WebView is configured to verify the resources in a URL w.r.t whitelist of trusted resources.

## Discussion
SeMA ensures that apps define WebViews that verify the trustworthiness of a webpage's resources. This verification is valid because WebViews do not have all the safeguards imposed by full-blown browsers. Therefore, every time a WebView is requested to load a web page, it should check if the resources embedded in the URL of the webpage is trusted.

SeMA enables a WebView to verify the resources loaded in the WebView by configuring the WebView with a whitelist of resource URL patterns indicating trusted resources. So, the *Secure* app configures the WebView defined in the storyboard with a whitelist of resource URL patterns. But, if the URL patterns in the whitelist are too broad (e.g., any URL that ends with *.com*), then malicious resource URLs matching these patterns will be loaded.
