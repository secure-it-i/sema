## Summary
Apps that allow Javascript code to execute in a `WebView` without verifying where the JavaScript is coming from can expose the app's resources. *Corresponding Ghera benchmark: [WebViewAllowFileAccess-UnauthorizedFileAccess-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Web/WebViewAllowFileAccess-UnauthorizedFileAccess-Lean/)*

## Description of the vulnerability
*Context*: An Android app can display web pages by loading HTML/JavaScript files in a `WebView`. The loaded HTML/JavaScript file runs in the context of the app i.e., it has access to the same resources and has the same permissions the app has. For example, an HTML file loaded into the app from a server will have access to the app's internal file system because the HTML file loaded from the server runs in the context of the app.

*Issue*: An application can inject and execute malicious JavaScript inside the `WebView` (provided the `WebView` allows JavaScript execution). If the `WebView` has access to the app's internal resources or has permission to perform privileged operations, then the malicious JavaScript inside the `WebView` will also have the same ability.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* app has a WebView (that allows JS execution) to display web content from a URL. The WebView has access to the app's internal files. If the WebView loads URL with malicious JavaScript (JS) embedded, then the malicious JS will have unauthorized access to the app's internal files.

## Building Benign with SeMA
The storyboard of *Benign (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

- The WebView can execute JS and allows access to the app's internal files.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags an error because the *Benign* app loads a URL in its WebView. If a URL with malicious JS code is loaded in the WebView, then it might lead to unauthorized access of files in the *Benign* app's internal file system.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment if the URL contains *sema.com/* or is *google.com*. Otherwise, the WebView refuses to load the URL.

- The WebView can execute JS and allows access to internal files of the app.

The analysis does not report any error because the WebView loads only trusted URLs, and it assumes it is safe for JS embedded in those URLs to access the app's internal files.

## Discussion
SeMA prevents apps from defining WebViews that load all URLs. This choice is valid because WebViews do not have all the safeguards imposed by full-blown browsers. Therefore, SeMA allows a WebView to define a whitelist of URL patterns that it will load. In the absence of a whitelist, SeMA prevents WebViews from accessing the app's internal files.

The *Secure* app defines a WebView with a whitelist of URL patterns to indicate that if the WebView loads a URL that does not match the patterns in the whitelist, then the WebView will not have access to the app's internal files. But, if the URL patterns in the whitelist are too broad (e.g., any URL that ends with *.com*), then malicious URLs matching these patterns will gain unauthorized access to the app's internal files.
