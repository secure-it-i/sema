## Summary
Apps that ignore SSL errors when loading content in a `WebView` are vulnerable to MitM attacks. .*Corresponding Ghera benchmark: [WebViewIgnoreSSLWarning-MITM-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Web/WebViewIgnoreSSLWarning-MITM-Lean/)*

## Description of the vulnerability
*Context*: An Android app can display web pages by loading HTML/JavaScript files in a `WebView`. A `WebView` loading an HTML/JavaScript file from a server using SSL/TLS can throw an SSL exception if the server presents an incorrect certificate or if the app does not trust the Certificate Authority that has signed the certificate for that server.

*Issue*: Android provides the `WebViewClient` API to manage communication between the app and the server. One of the methods in the API `onReceivedSslError()` allows an app to cancel or proceed with the response from the server when an SSL error occurs. If the app chooses to continue with the response, then the app is vulnerable to MITM attacks because a malicious server can create a fake certificate and still communicate with the app.

*Resolution*: SeMA inhibits apps from ignoring SSL warnings. Therefore, SeMA does not allow creating an app with this vulnerability.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags no errors because the WebView configuration rejects loading a URL if loading the URL raises SSL errors.

## Discussion
SeMA prevents WebViews to ignore SSL errors. The code generation configures all WebViews defined in the storyboard to reject a connection if an SSL error occurs. This configuration choice is valid WebViews should never ignore SSL errors.
