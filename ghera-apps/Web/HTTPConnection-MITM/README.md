## Summary
Apps that connect to a server via the HTTP protocol are vulnerable to Man-in-the-Middle (MITM) attacks. Corresponding Ghera Benchmark: *[HttpConnection-MITM-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Web/HttpConnection-MITM-Lean/)*

## Description of the vulnerability
*Context*: Android apps can connect to a web server via the HTTP protocol using the `HttpURLConnection` API.

*Issue*: A MITM can easily intercept traffic between the app and the server and can even spoof the address of the server to masquerade as the server to the app.

*Example*: The *Benign* app collects personal information from the user and sends it to a remote server X over *HTTP*. A malicious server can masquerade as X and intercept the user's personal information collected by the *Benign* app.

## Building Benign with SeMA
The storyboard of *Benign (see main_nav.xml)* is as follows:

- When the app is started, *form* fragment is displayed to the user.

- In the *form* fragment, the user enters her name and email address and clicks on the *submit* button.

- When the user clicks on the *submit* button, the name and email address she entered is sent to http://sema.remote.server.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags an error because the *Benign* app sends the user's personal information to a remote server over HTTP, which is not safe.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *form* fragment is displayed to the user.

- In the *form* fragment, the user enters her name and email address and clicks on the *submit* button.

- When the user clicks on the *submit* button, the name and email address she entered is sent to https://sema.remote.server.

The analysis does not report any error because the *Benign* app sends the user's personal information to a remote server over HTTPS, which is safe.

## Discussion
SeMA prevents apps from interacting with remote servers over HTTP. Therefore, *Secure* does not allow a malicious server to intercept the requests it sends to a remote server.
