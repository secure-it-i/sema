## Summary
Apps that use `loadDataWithBaseUrl()` with a file scheme based baseURL (e.g., file://www.google.com) and allow the execution of JavaScript sourced from an unverified source may expose the app's resources. *Corresponding Ghera benchmark: [WebViewLoadDataWithBaseUrl-UnauthorizedFileAccess-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Web/WebViewLoadDataWithBaseUrl-UnauthorizedFileAccess-Lean/)*

## Description of the vulnerability
*Context*: An Android app can display web pages via the `WebView` API. The loaded HTML/JavaScript files run in the context of the app. Hence, they have the same privilege as the app in terms of access to resources.

*Issue*: An application can load saved HTML web page as a string using `loadDataWithBaseUrl()` with file scheme `baseURL`. Since `WebView` has permission to access all of the app�s resources, JavaScript code executing in the context of `WebView` will also have the same permissions. If the saved web page sources JavaScript code from a malicious server, then these permissions can be abused.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* app has a WebView (JS execution and file access enabled) to display web content from an HTML file in external storage. The WebView has a base URL with *file* protocol to allow the web content to access the app's internal files. A malicious app can inject an HTML file in external storage. Since the WebView has a base URL with *file* protocol, the malicious HTML in the WebView will gain unauthorized access to the *Benign* app's internal files.

## Building Benign with SeMA
The storyboard of *Benign (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters the name of the HTML file and clicks on the *Load Data* button.

- When the user clicks on the *Load Data* button, the contents of the HTML file in external storage are loaded in the WebView of the *disp* fragment.

- The WebView is configured to execute JS and has a base URL with *file* protocol, which means that the WebView can access the app's internal files.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags an error because the *Benign* app uses a WebView that is configured with a base URL that allows arbitrary JS to access the app's internal files.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters the name of the HTML file and clicks on the *Load Data* button.

- When the user clicks on the *Load Data* button, the contents of the HTML file in internal storage are loaded in the WebView of the *disp* fragment.

- The WebView is configured to execute JS and has a base URL with *file* protocol, which means that the WebView can access the app's internal files. Additionally, the WebView defines a whitelist of URL patterns that it trusts.

The analysis does not report any error because the WebView is configured to verify the request to load a webpage or resource w.r.t a whitelist of trusted URL patterns.

## Discussion
SeMA ensures that a WebView (defined in the storyboard) is configured with a base URL. The base URL is used to define the scope of the WebView in terms of the app resources it can access. For example, a WebView that has a base URL with *Http* protocol cannot access files. Additionally, SeMA configures WebViews defined in the storyboard with a whitelist of trusted URL patterns to prevent the WebView from loading web content that is not trusted by the app. Due to these reasons, SeMA protects a WebView from misuse.

The *Secure* app defines a WebView with *file* base URL. It loads web content from an internal file in the app and verifies every request to load content in the WebView via a whitelist. Therefore, the WebView in the *Secure* app cannot be exploited to gain unauthorized access to the app's resources. However, if the whitelist of trusted URL patterns is too broad, then malicious web content can bypass the checks to inject and enable arbitrary execution of JS code.
