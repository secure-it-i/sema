## Summary
Apps that allow websites viewed through `WebView` may enable cookie overwrite attacks. *Corresponding Ghera Benchmark: [WebView-CookieOverwrite-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Web/WebView-CookieOverwrite-Lean/)*

## Description of the vulnerability
*Context*: Android apps can display websites via WebView. Websites store state information on the browser/client sides using cookies. Cookies can be set either by `Set-Cookie` header or client-side javascript by using `document.write()`.

A cookie consists of name, value, and zero or more attributes as name/value pairs. The domain and path are two attributes that define the scope of a cookie.

If a cookie shares the domain scope with a related domain, it can be directly overwritten by that domain using another cookie with the same name/domain/path.

*Issue*: An Android app that uses `WebView` can allow websites to store cookies. A malicious web page MW from the same domain as benign web page BW can overwrite cookies set by BW by specifying the same name/domain/path.

*Example*: The *Benign* app has a WebView to display web content from a URL. The WebView in the *Benign* app can store data in and retrieve data from cookies. Additionally, the WebView allows the execution of JavaScript embedded in the URL. If the WebView loads URL with malicious JavaScript (JS), then the malicious JS can overwrite the WebView cookies.

## Building Benign with SeMA
The storyboard of *Benign (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

- The WebView can execute JS and accept cookies.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags an error because the *Benign* app accepts cookies from a URL loaded in its WebView. If the URL is malicious, then JS code embedded in it could overwrite data stored in the WebView's cookies.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment if the URL ends with *sema.com*. Otherwise, the WebView refuses to load the URL's contents.

- The WebView can execute JS and accepts cookies.

The analysis does not report any error because the WebView is allowed to load only URLs that end with *sema.com* thus restricting the scope of malicious URLs.

## Discussion
SeMA prevents apps from defining WebViews that accept cookies and allow JS execution from all URLs. This choice is valid because WebViews do not have all the safeguards imposed by full-blown browsers. Therefore, SeMA allows a WebView to define a whitelist of URLs that it will load. In the absence of a whitelist, SeMA prevents WebViews from accepting cookies and executing JS.

The *Secure* app does not allow malicious JS to overwrite cookies in its WebView by restricting the URLs loaded in it. This restriction is enforced via a whitelist of URL patterns defined in the storyboard. However, if the defined patterns are so broad that a malicious URL can bypass them, then the cookies in the *Secure* app's WebView can still be overwritten.  For example, if a WebView defines a whitelist pattern that allows any URL that ends with *.com* to be loaded in the WebView, then it is too broad since malicious URLs that end with *.com* will also be loaded into the WebView.
