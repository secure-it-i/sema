## Summary
Apps that allow Javascript code execution in the `WebView` widget may be susceptible to malicious code execution. Corresponding Ghera Benchmark: *[JavaScriptExecution-CodeInjection-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Web/JavaScriptExecution-CodeInjection-Lean/)*

## Description of the vulnerability
*Context*: An Android app can display web pages using `WebView` widget. The loaded web content (HTML/JavaScript) executes in the context of the app with the same privileges as of the app unless instructed otherwise. Since `WebView` is not vetted for security as rigorously as Web Browsers, the execution of JavaScript code loaded into WebView can be disabled via `WebView.setJavaScriptEnabled()` method.

*Issue*: If arbitrary web content is loaded into `WebView` without disabling JavaScript code execution, an adversary could inject malicious code through JavaScript and have it executed by an app to cause harm.

*Example*: The *Benign* app has a WebView to display web content from a URL. The WebView in the *Benign* app allows the execution of JavaScript. If the WebView loads URL with malicious JavaScript (JS) embedded, then the malicious JS is executed by *Benign*.

## Building Benign with SeMA
The storyboard of *Benign (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

- The WebView can execute JS.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags an error because the *Benign* app loads a URL in its WebView. If a URL with malicious JS code is loaded in the WebView, then it will lead to malicious code execution.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment if the URL contains *sema.com/* or is *google.com*. Otherwise, the WebView refuses to load the URL.

- The WebView can execute JS embedded in the URL that it loads.

The analysis does not report any error because the WebView loads only trusted URLs, and it assumes that the JS embedded in those URLs are safe.

## Discussion
SeMA prevents apps from defining WebViews that load all URLs. This choice is valid because WebViews do not have all the safeguards imposed by full-blown browsers. Therefore, SeMA allows a WebView to define a whitelist of URL patterns in the storyboard. In the absence of a whitelist, SeMA prevents WebViews from executing JS.

The *Secure* app does not allow a WebView defined in the app to execute arbitrary JS. It either disallows a WebView from executing JS, or it allows a WebView to execute JS only if the WebView explicitly identifies a whitelist of URL patterns that are safe to load, however, if the URL patterns are too broad (e.g., any URL that ends with *.com*), then malicious URLs matching these patterns will be able to embed JS and make *Secure* execute it.
