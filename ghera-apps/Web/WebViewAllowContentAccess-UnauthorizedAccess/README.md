## Summary
Apps that allow Javascript code to execute in a `WebView` without verifying where the JavaScript is coming from can expose the app's resources. *Corresponding Ghera benchmark: [WebViewAllowContentAccess-UnauthorizedFileAccess-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Web/WebViewAllowContentAccess-UnauthorizedFileAccess-Lean/)*

## Description of the vulnerability
*Context*: An Android app can display web pages via `WebView` API. The loaded HTML/JavaScript files run in the context of the app. Consequently, they have the same privilege as the app in terms of access to content providers.

*Issue*: An application can inject and execute malicious JavaScript inside the `WebView` (provided the `WebView` allows JavaScript execution). The `WebView` has permission to access all content providers the app has access to. Therefore, JavaScript executing in the context of `WebView` will also have permission to access these content providers.

*Example*: The *Benign* app has a WebView (that allows JS execution) to display web content from a URL and a Content Provider that shares files in the app's internal files storage with other components in the app. If the WebView loads URL with malicious JavaScript (JS) embedded, then the malicious JS will have access to the files in the app's internal file system via the Content Provider.

## Building Benign with SeMA
The storyboard of *Benign (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

- The WebView can execute JS and allows access to any Content Providers defined in the app.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags an error because the *Benign* app loads a URL in its WebView. If a URL with malicious JS code is loaded in the WebView, then it might lead to unauthorized access of files in the *Benign* app's internal file system.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment if the URL contains *sema.com/* or is *google.com*. Otherwise, the WebView refuses to load the URL.

- The WebView can execute JS and allows access to any Content Providers defined in the app.

The analysis does not report any error because the WebView loads only trusted URLs, and it trusts the JS embedded in those URLs to access the app's resources.

## Discussion
SeMA prevents apps from defining WebViews that load all URLs. This choice is valid because WebViews do not have all the safeguards imposed by full-blown browsers. Therefore, SeMA allows a WebView to define a whitelist of URLs that it will load. In the absence of a whitelist, SeMA prevents WebViews from accessing the app's Content Providers.

The *Secure* app defines a WebView with a whitelist of URL patterns to indicate that if the WebView loads a URL that does not match the patterns in the whitelist, then the WebView will not have access to the app's Content Providers. But, if the URL patterns in the whitelist are too broad (e.g., any URL that ends with *.com*), then malicious URLs matching these patterns will gain unauthorized access to the app's Content Providers.
