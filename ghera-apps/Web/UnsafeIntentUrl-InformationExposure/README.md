## Summary
Apps that do not safely handle an incoming intent embedded inside a URI are vulnerable to information exposure via intent hijacking. *Corresponding Ghera benchmark: [UnsafeIntentURLImpl-InformationExposure-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Web/UnsafeIntentURLImpl-InformationExposure-Lean/)*

## Description of the vulnerability
*Context*: Android allows a webpage displayed in an Android app to launch an Android component via an intent. A webpage X needs to encode the intent as a valid URI U and load it into a component C that displays X. When the user interacts with the webpage X and takes an action, the resource pointed to by U is loaded.

*Issue*: If an app has a component C that displays web pages and that component does not sanitize the URI before acting on it, then a malicious web page could embed an intent to a malicious component installed in the device. When the user interacts with the URI, she ends up interacting with the malicious component.

*Example*: The *Benign* app has a WebView to display web content from a URL. The URL can contain intents to components installed in the device. The *Benign* app does not validate the intents in an incoming URL. If the intent in an incoming URL is malicious and the *Benign* app acts on the intent (e.g., embed sensitive information in the intent), then the *Benign* app is vulnerable to intent hijacking.

## Building Benign with SeMA
The storyboard of *Benign (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

- The loaded web page has a button. If the user clicks on this button, then a request to load URL with an intent is sent to the WebView.

- The WebView has a whitelist that accepts only URL requests with implicit intents.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags an error because the *Benign* app accepts URL load requests where the URL contains implicit intents. A malicious app can register to receive these implicit intents and hijack the intent.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

- The loaded web page has a button. If the user clicks on this button, then a request to load URL with an intent is sent to the WebView.

- The WebView has a whitelist that accepts only URL requests with explicit intents.

The analysis does not report any error because the WebView is configured to act on intents that have an explicitly defined destination. Hence, such intents cannot be hijacked.

## Discussion
SeMA ensures that if a WebView loads an intent URL, then the intent is explicit, i.e., explicitly identifies the destination component. The choice to allow only explicit intents in URLs is valid because explicit intents cannot be hijacked.  

SeMA enables users to specify a whitelist of URLs with intent destinations. So, the *Secure* app defines a WebView with a whitelist of intent URLs. However, if an intent destination pattern is too broad, then malicious apps containing entities with the same pattern will still be able to hijack the intent.   
