## Summary
Apps that do not validate page requests made from within a `WebView` are vulnerable to loading malicious content in it. *Corresponding Ghera benchmark: [WebViewOverrideUrl-MITM-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Web/WebViewOverrideUrl-MITM-Lean/)*

## Description of the vulnerability
*Context*: Android allows apps to display web content in a `WebView`, and control navigation across webpages via the `shouldOverrideUrlLoading()` method in `WebViewClient`. The `shouldOverrideUrlLoading()` method takes an instance of WebView and the page request as input and returns a boolean. If true is returned, then the host application handles the request; otherwise, the current WebView handles the request.

*Issue*: The app does not validate the page request in `shouldOverridUrlLoading()` before loading it in the WebView. Consequently, any web page provided by the server will be loaded into WebView.

*Example*: The *Benign* app has a WebView (that allows JS execution) to display web content from a URL. The WebView does not validate a request to load a URL. A malicious URL embedded in a webpage loaded in the WebView can execute JS code.

## Building Benign with SeMA
The storyboard of *Benign (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

- The WebView can execute JS.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis flags an error because the *Benign* app loads a URL that may contain malicious URLs. These malicious URLs can inject JS code into the WebView.

## Building Secure with SeMA
The storyboard of *Secure (see main_nav.xml)* is as follows:

- When the app is started, *browser* fragment is displayed to the user.

- In the *browser* fragment, the user enters a URL and clicks on the *Load URL* button.

- When the user clicks on the *Load URL* button, the contents of the URL are loaded in the WebView of the *disp* fragment.

- The WebView is configured with a whitelist of trusted URL patterns, which is used to verify every request to load a URL in the WebView. If the URL to load does not match the trusted URL patterns, then the URL is rejected by the WebView.

- The WebView can execute JS.

The analysis does not report any error because the WebView is configured to verify a URL load request w.r.t a trusted whitelist.

## Discussion
A WebView in SeMA is a widget that helps display web content. Developers need to specify the URL of the content that is to be displayed. However, WebViews do not have all the safeguards imposed by full-blown browsers. Hence, SeMA enforces developers to configure a WebView with a whitelist of trusted URL patterns to indicate that the WebView will not display contents of a URL that does not match the trusted URL patterns. But, if the URL patterns in the whitelist are broad enough to enable a malicious URL to evade the whitelist verification, then the WebView will still be vulnerable to MITM attacks.
