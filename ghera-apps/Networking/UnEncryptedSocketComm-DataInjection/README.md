## Summary
Apps that communicate with a server over TCP/IP without encryption allow Man-in-the-Middle attackers to spoof server IPs by intercepting client-server data streams. *Corresponding Ghera benchmark: [UnEncryptedSocketComm-MITM-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Networking/UnEncryptedSocketComm-MITM-Lean/)*

## What is the vulnerability
*Context*: An app can connect to a server via the Socket API and transmit data to and from it.

*Issue*: A Man-in-the-Middle can intercept traffic between the app and the server and can also redirect the app to a different IP without the app's knowledge.

*Example*: The *Benign* app demonstrates the vulnerability. *Benign* reads information from an open socket and displays it in a widget. A malicious server can inject data into the socket and control the information *Benign* displays.

## Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* with a button is displayed to the user.

- In *homeFrag*, when the user clicks on the *show file* button, then fragment *dispFrag* is displayed.

- In *dispFrag*, the widget *dispArea* is populated with information from an open socket.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because *Benign* reads information from an open socket without verifying the sender. Therefore, a malicious server can masquerade and inject data into the open socket.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* with a button is displayed to the user.

- In *homeFrag*, when the user clicks on the *show file* button, then fragment *dispFrag* is displayed.

- In *dispFrag*, the widget *dispArea* is populated with information from an open socket. The socket only accepts connection from a server that has a public key signed by a particular certificate authority *Secure* trusts.

The analysis does not report any error because verifying the socket connection before reading from the socket is safe.

## Discussion
SeMA prevents apps from reading from an open socket that accepts connections without verifying the identity of the entity requesting the connection.

The *Secure* app does not accept connections via an open socket without first verifying the identity of the entity requesting the connection.
