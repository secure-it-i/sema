## Summary
Apps that use the SSLCertificateSocketFactory.getSocket(InetAddress, ...) method are vulnerable to MITM attacks. *Corresponding Ghera benchmark:[InsecureSSLSocket-MITM-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Networking/InsecureSSLSocket-MITM-Lean/)*

## Description of the vulnerability
*Context*: Android apps can use SSL/TLS for secure communication with a server. *SSLCertificateSocketFactory.createSocket()* is an API that helps create a secure SSL connection with a server identified by an *InetAddress*. By default, this API does not perform hostname checks on the server.

*Issue*: Use of the method *SSLCertificateSocketFactory.getSocket(InetAddress, ...)* opens up a vulnerability to Man in the Middle attacks due to the absence of hostname verification.

*Resolution*: The code generation in SeMA uses the default hostname verification provided by Android before connecting to a server via SSLSocket. Therefore, apps created with SeMA do not have this vulnerability.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In *homeFrag*, the user clicks on the *Take Pic* button, which allows the user to open the camera and take a picture. The picture is sent as an argument to fragment *imageFrag*.

- The picture is displayed in *imageFrag* along with a button to save the picture in a remote server.

- In *imageFrag*, when the user clicks on the *savePic* button, the picture is written to an open socket. The socket only accepts connection from a server that has a public key signed by a particular certificate authority *Secure* trusts.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports no errors since a malicious server cannot bypass hostname verification to connect to the SSL socket opened by the app.

## Discussion
SeMA prevents apps from interacting with a remote server over a socket without verifying the hostname of the server.

The *Secure* app does not accept connections to open sockets without first verifying the hostname of the server making the connection.
