## Summary
Apps that communicate with a remote server over an open port are vulnerable to information leak. *Corresponding Ghera benchmark:[OpenSocket-InformationLeak-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Networking/OpenSocket-InformationLeak-Lean/)*

## Description of the vulnerability
*Context*: An app can listen on a port expecting a connection from a remote server. When the app is successfully connected with the remote server, it performs an action like uploading code/data from the server or downloading code/data form the server.

*Issue*: If the app does not verify the identity of the host it is connected to before performing an action then the app is susceptible to connecting with a malicious remote server instead of connecting with the intended server.

*Example*: The *Benign* app demonstrates the vulnerability. In *Benign*, a user can take a picture and send it to a remote server. So, *Benign* opens a socket and writes the image to it. A malicious server connects with the socket and gets access to the image without the user's knowledge or consent.  

## Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In *homeFrag*, the user clicks on the *Take Pic* button, which allows the user to open the camera and take a picture. The picture is sent as an argument to fragment *imageFrag*.

- The picture is displayed in *imageFrag* along with a button to save the picture in a remote server.

- In *imageFrag*, when the user clicks on the *savePic* button, the picture is written to an open socket. A remote server connects to the open socket and extracts the picture.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because the image/picture taken by the user of *Benign* is written to an open socket. Therefore, a malicious app/server can connect to the socket and get access to the user's picture.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In *homeFrag*, the user clicks on the *Take Pic* button, which allows the user to open the camera and take a picture. The picture is sent as an argument to fragment *imageFrag*.

- The picture is displayed in *imageFrag* along with a button to save the picture in a remote server.

- In *imageFrag*, when the user clicks on the *savePic* button, the picture is written to an open socket. The socket only accepts connection from a server that has a public key signed by a particular certificate authority *Secure* trusts.

The analysis reports no errors since a malicious app/server cannot connect to the open socket where the picture resides.

## Discussion
SeMA prevents apps from writing sensitive information to an open socket that accepts connections from a server without first verifying its identity.

The *Secure* app does not accept connections to open sockets without first verifying the identity of the server making the connection.
