## Summary
Apps that do not check for the validity of a certificate when communicating with a server are vulnerable to information exposure attacks. *Corresponding Ghera benchmark: [CheckValidity-InformationExposure-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Networking/CheckValidity-InformationExposure-Lean/)*

## Description of the vulnerability
*Context*: Android apps can use SSL/TLS to communicate with a web server securely. When an app wants to connect to a web server, it requests the web server for a certificate signed by a certificate authority that the app trusts. Every certificate has a specified start date and end date. If the app tries to connect to the server, which has a certificate with an end date earlier than the time when the app is trying to connect, then the connection should be refused. Android provides checkValidity API to check if the current date and time are within the validity period given in the certificate. Usually, default trustmanager takes care of validating the certificates. If custom trustmanager is implemented, then checkValidity API can be used in checkServerTrusted() method of X509TrustManager to check the validity of all the certificates in the chain. Any improper implementation of checkServerTrusted() can lead to vulnerability.

*Issue*: Apps can be vulnerable if they do not check the expiry date of the server certificates. checkValidity API is a public method of the X509Certificate interface. In the case of custom trustmanager implementation, the checkServerTrusted() method is responsible for deciding which server to trust and validate the certificate of the server through the checkValidity API. The partial or complete certificate chain provided by the peer is given to checkServerTrusted() as arguments. Every element in this certificate chain should be checked for validity. If the checkValidity method is not used correctly in checkServerTrusted(), then the app can end up connecting to a server signed by a certificate that already expired.

*Resolution*: SeMA generates the default trust managers provided by Android to check the validity of the servers the app interacts with. Alternatively, it also offers the option to specify a whitelist of certificates the app will trust. So, an app made with SeMA will not have this vulnerability.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *form* is displayed to the user.

- In the *form* fragment, the user enters her personal information and clicks on the *submit* button.

- When the user clicks the *submit* button, then her personal information is sent to a remote server after connecting to it via certificate pinning.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports no errors.

The code is generated so that *Secure* verifies the server before connecting to it. The code generation uses Android's default trust managers to verify the validity of the server's certificate.

## Discussion
SeMA prevents apps from implementing custom trust managers. Most apps do not need custom trust managers and are known to implement them incorrectly [1]. However, sometimes, an app needs to connect with a server whose certificate is not trusted by the default trust manager (e.g., self-signed certificate). In such cases, SeMA allows app developers to specify a whitelist of certificates it will trust via the certificate pinning feature.

The *Secure* app does not use custom trust managers to check the validity of a server's certificate.

## References
1) [Mitigating Android Application SSL Vulnerabilities using Configuration Policies by Vasant Tendulkar](https://repository.lib.ncsu.edu/handle/1840.16/8840)
