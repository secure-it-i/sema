## Summary
Apps that do not check for the validity of a certificate when communicating with a server are vulnerable to information exposure attacks. *Corresponding Ghera benchmark: [IncorrectHostNameVerification-MITM-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Networking/IncorrectHostNameVerification-MITM-Lean/)*

## Description of the vulnerability
*Context*: Android apps can use SSL/TLS for secure communication with a web server. The communication can fail if the certificate presented by the server does not have the correct hostname in its subject line.

*Issue*: Android allows apps to provide a custom implementation of HostnameVerifier in which custom checks can be performed on the given hostname in verify method. If these custom checks are incorrect or weak, then apps can end up connecting to malicious servers.

*Resolution*: SeMA generates the default implementation of HostnameVerifier provided by Android to verify the hostname in the certificate of the server the app interacts with. Since, it does not allow custom implementations of HostnameVerifier, apps with this vulnerability cannot be built with SeMA.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *form* is displayed to the user.

- In the *form* fragment, the user enters her personal information and clicks on the *submit* button.

- When the user clicks the *submit* button, then her personal information is sent to a remote server after connecting to it via certificate pinning.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports no errors.

The code is generated so that *Secure* verifies the server before connecting to it. The code generation uses Android's default HostnameVerifier to verify the hostname set in the server's certificate.

## Discussion
SeMA prevents apps from implementing custom HostnameVerifier. Most apps do not need custom HostnameVerifier and are known to implement them incorrectly [1]. However, sometimes, an app needs to connect with a server whose certificate is mis-configured. In such cases, the server will have to correctly configured before proceeding further.

The *Secure* app does not use custom HostnameVerifier to check the hostname in a server's certificate.

## References
1) [Mitigating Android Application SSL Vulnerabilities using Configuration Policies by Vasant Tendulkar](https://repository.lib.ncsu.edu/handle/1840.16/8840)
