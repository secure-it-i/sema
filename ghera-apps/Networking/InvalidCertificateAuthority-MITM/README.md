## Summary
Apps that do not check for the validity of a Certificate Authority when communicating with a server are vulnerable to MITM attacks. *Corresponding Ghera benchmark: [InvalidCertificateAuthority-MITM-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Networking/InvalidCertificateAuthority-MITM-Lean/)*

## Description of the vulnerability
*Context*: Android apps can use SSL/TLS to securely communicate with a web server. When an app wants to connect with a web server, it requests the web server for a certificate signed by a certificate authority that the app trusts. How does the app know which Certificate Authority to trust? Every Android device comes with a list of pre-installed Certificate Authorities that it trusts. All apps running on that device inherit the trusted list of Certificate Authorities. If the server the app is trying to connect to is signed by a Certificate Authority not trusted by the Android device in which the app is running then the connection is refused. This becomes a problem if the server is signed with a certificate that is not well known or the server is self-signed. Android provides the TrustManager and SSLContext APIs to workaround this problem. An app can implement its own TrustManager listing the certificates it trusts. It can then set these TrustManagers in an SSLContext and use that SSLContext when communicating with a server using SSL/TLS.

*Issue*: Apps can be vulnerable if they incorrectly implement a TrustManager. A TrustManager can be implemented by implementing the *X509TrustManager* interface and overriding the methods *checkClientTrusted()*, *checkServerTrusted()* and *getAcceptedUsers()*. The method which checks whether a server is signed by a trusted *Certificate Authority* is *checkServerTrusted()*. If this method is not implemented correctly then app the app can end up connecting to a malicious server signed by a fake Certificate Authority.

*Resolution*: SeMA generates the default trust managers provided by Android to check the validity of the servers the app interacts with. Alternatively, it also offers the option to specify a whitelist of certificates the app will trust. So, an app built with SeMA does not have this vulnerability.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *form* is displayed to the user.

- In the *form* fragment, the user enters her personal information and clicks on the *submit* button.

- When the user clicks the *submit* button, then her personal information is sent to a remote server after connecting to it via certificate pinning.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports no errors.

The code is generated so that *Secure* verifies the server before connecting to it. The code generation uses Android's default trust managers to verify the validity of the server's certificate.

## Discussion
SeMA prevents apps from implementing custom trust managers. Most apps do not need custom trust managers and are known to implement them incorrectly [1]. However, sometimes, an app needs to connect with a server whose certificate is not trusted by the default trust manager (e.g., self-signed certificate). In such cases, SeMA allows app developers to specify a whitelist of certificates it will trust via the certificate pinning feature.

The *Secure* app does not use custom trust managers to check the validity of a server's certificate.

## References
1) [Mitigating Android Application SSL Vulnerabilities using Configuration Policies by Vasant Tendulkar](https://repository.lib.ncsu.edu/handle/1840.16/8840)
