package sema.apps.secure.busLogic;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.String;
import java.lang.UnsupportedOperationException;

public class FormFragmentbusLogic {
  public void busLogicPostParamsForsubmitSucc(OutputStream outputStream, String name, String email)
      throws IOException {
    throw new UnsupportedOperationException();
  }

  public boolean busLogicReadStreamForsubmitSucc(InputStream inputStream) throws IOException {
    throw new UnsupportedOperationException();
  }
}
