## Summary
Apps that do not pin the certificates of servers they trust are vulnerable to MITM attacks. *Corresponding Ghera benchmark: [UnpinnedCertificates-MITM-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Networking/UnpinnedCertificates-MITM-Lean/)*

## What is the vulnerability
*Context*: Android apps can use SSL/TLS to communicate with a web server securely. All apps running on that device inherit the trusted list of Certificate Authorities. If an app knows the certificate of server X, the app can store the corresponding certificate for future reference. When the app tries to connect to any server, it will check against stored certificates. This is called certificate pinning.

*Issue*: Apps can be vulnerable if they do not implement a certificate pinning. If a certificate authority mistakenly trusts the Malicious server, then the app which does not implement the pinning will end up trusting the Malicious server.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* collects information from the user and sends it to a remote server without implementing certificate pinning. Therefore, a malicious server can get access to the user information collected by the app.

## Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *form* is displayed to the user.

- In the *form* fragment, the user enters her personal information and clicks on the *submit* button.

- When the user clicks the *submit* button, then her personal information is sent to a remote server.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because *Benign* connects to a remote server without certificate pinning, which means that a malicious server can masquerade as the server and steal the user's personal information collected by *Benign*.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *form* is displayed to the user.

- In the *form* fragment, the user enters her personal information and clicks on the *submit* button.

- When the user clicks the *submit* button, then her personal information is sent to a remote server after connecting to it via certificate pinning.

The analysis reports no errors because certificate pinning ensures that the app connects to a trusted remote server.

## Discussion
SeMA prevents apps from connecting to a remote server without certificate pinning. Certificate pinning allows an app to list the certificate authorities that the app trusts. A trusted certificate authority must sign a remote server connecting with the app.

The *Secure* app does not connect to remote servers without certificate pinning, thus making sure that it does accidentally not connect to a malicious server.
