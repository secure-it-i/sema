## Summary
Apps that contain a broadcast receiver registered to receive ordered broadcasts are vulnerable to receive data from a malicious broadcast receiver with higher priority. *Corresponding Ghera benchmark: [OrderedBroadcast-DataInjection-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/ICC/OrderedBroadcast-DataInjection-Lean/)*

## Description of the vulnerability
*Context*: A Broadcast Receiver can register to receive ordered broadcasts. When an ordered broadcast is sent, then all broadcast receivers registered to receive that broadcast respond to it in the order of priority. The receivers with higher priority (number) respond first and forward it to receivers with lower priority (number), e.g., receiver with priority 1 will receive the request before a receiver with priority 0. If multiple receivers have the same priority, then the order is arbitrary.

*Issue*: Since higher priority receivers respond first and forward it to lower priority receivers, a malicious receiver with high priority can intercept the message change it and forward it to lower priority receivers.

*Example*: *Benign* app demonstrates the vulnerability. *Benign* has two receivers, *CheckEmailRecvr* and *EmailRecvr*, defined in it. Both these receivers respond to event X. However, *CheckEmailRecvr* has a higher priority, so it responds first, sanitizes the inputs, and passes it to *EmailRecvr*. *EmailRecvr* reads the EmailID and the EmailBody that was part of the first broadcast and sends it to an email client app, that sends the actual email. A malicious app can design a receiver to respond to event X but with a higher priority. The malicious receiver can intercept the broadcast, change the email ID embedded in the broadcast to an email ID of its choice and pass it onto *CheckEmailRecvr*.  

## Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app starts, fragment *home_id* is displayed to the user.

- In *home_id*, if the user clicks on the *Send Email* button, then an *ordered broadcast* with the email ID and the email body is broadcasted to all receivers that are registered to receive event X.

- *CheckEmailRecvr* and *EmailRecvr* in *Benign* are registered to receive event X, with *CheckEmailRecvr* having higher priority.

- *CheckEmailRecvr* responds to event X, sanitizes the email ID in the received broadcast, and forwards it to *EmailRecvr*.

- *EmailRecvr* receives the email ID forwarded by *CheckEmailRecvr* along with email Body from the original broadcast and sends the email ID and email Body to an email client, that actually sends the email.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports two errors because a malicious receiver with higher priority can register to receive event X before *CheckEmailRecvr* and *EmailRecvr*. The malicious receiver can change the email ID or email body embedded in the broadcast and forward it to *CheckEmailRecvr* and *EmailRecvr*.

## Building Secure with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app starts, fragment *home_id* is displayed to the user.

- In *home_id*, if the user clicks on the *Send Email* button, then an *ordered broadcast* with the email ID and the email body is broadcasted to all receivers that are registered to receive event X.

- *CheckEmailRecvr* and *EmailRecvr* in *Benign* are registered to receive event X, with *CheckEmailRecvr* having higher priority. *CheckEmailRecvr* and *EmailRecvr* are further protected by permissions.

- *CheckEmailRecvr* responds to event X, checks the email ID in the received broadcast (based on some business logic), and forwards it to *EmailRecvr*.

- *EmailRecvr* receives the email ID forwarded by *CheckEmailRecvr* along with email Body from the original broadcast and sends the email ID and email Body to an email client, that actually sends the email.

The analysis does not report any error because a malicious receiver will not be able to inject any data into *CheckEmailRecvr* and *EmailRecvr* without the required permission.

## Discussion
SeMA allows developers to define a broadcast receiver that accepts input from a previous receiver or send data to the next receiver in the priority chain. However, all such receivers need to be protected adequately.
