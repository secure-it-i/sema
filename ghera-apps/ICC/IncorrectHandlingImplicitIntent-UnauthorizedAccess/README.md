## Summary
Apps that have components that accept implicit intents but do not handle the implicit intents properly are vulnerable to unauthorized access. *Corresponding Ghera benchmark: [IncorrectHandlingImplicitIntent-UnauthorizedAccess-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/ICC/IncorrectHandlingImplicitIntent-UnauthorizedAccess-Lean/)*

## Description of the vulnerability
*Context*: Android allows developers to create components that can be accessed by other apps via implicit intent.

*Issue*:  If a component accepts implicit intents, then it can be accessed by malicious apps. If such components perform sensitive operations, then they can be used to perform sensitive operations by malicious apps without the user's knowledge and consent.

*Example*: The *Benign* app demonstrates the vulnerability. The *frag1* fragment in *Benign* can be accessed by any app (including malicious apps) and returns the value in the widget *info* to the entity that calls *frag1* without the users' consent or knowledge.

## Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started by another app X, the fragment *frag1* initializes widget *info* with data, and then returns the data in *info* X if a constraint *verifyInp* is satisfied.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because data from *Benign* is sent to an app that started *Benign* without the user's consent or knowledge.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started by another app X, the fragment *frag1* initializes widget *info* with data.

- Fragment *frag1* has a button *Share*, which, when clicked, returns the *info* to app X.

The analysis does not report any errors in this case because *info* is sent to app X if the user explicitly clicks on the button *Share*.

## Discussion
In SeMA, a constraint must guard a transition. Further, if the constraint performs a privileged operation (e.g., share sensitive information with another app), then the transition must be guarded by an additional user constraint (e.g., button click). These requirements ensure that a fragment or a resource defined in the app does not accidentally perform privileged operations without the user's consent or knowledge.        
