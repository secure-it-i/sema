## Summary
Apps that use reflection to dynamically load fragments into an activity are vulnerable to fragment injection attacks. Corresponding Ghera benchmark: *[FragmentInjection-PrivEscalation-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/ICC/FragmentInjection-PrivEscalation-Lean/)*

## Description of the vulnerability

*Context*: Fragments can be loaded into an activity dynamically using Java Reflection with just the fragment name.

*Issue*: An activity that accepts fragment names as input from other components, and loads the fragment dynamically into the activity is vulnerable to executing a fragment provided as input by a malicious app on the device.

*Example*: The *Benign* app demonstrates the vulnerability. The *profile* fragment in *Benign* accepts input via a deeplink. The received input is used to determine which fragment to load. A malicious app can control the input in the deeplink and use the input to load a fragment of the its choice.

## Building Benign with SeMA
The storyboard of *Benign* (see *Benign/main_nav.xml*) is as follows:

- When the app is started, the fragment *loginFrag* is displayed to the user. The user enters username and password and clicks on the *Login* button.

- If the login is successful and the *fragAddress* variable is empty then the user navigates to *home*. *fragAddress* is an input variable to the fragment *loginFrag*.

- If the login is successful and the fragment address is *profile* then the user navigates to *profile*.

- The *profile* fragment displays sensitive information about the user. It can also be accessed via a deeplink.

- If the *profile* fragment is accessed via deeplink then it expects an input variable *profileToken*, which is sent to a server for verification.

- If the verification is successful then *profile* fragment is displayed to the user.

- If the verification is not successful, then *fragAddress* is obtained from *profileToken* and sent as a parameter to *loginFrag*.

- In *loginFrag*, the user enters credentials. If the login is successful, then app navigates to the fragment set in *fragAddress*.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because *Benign* relies on *profileToken*, which can be set by a malicious app, to determine the destination fragment from *loginFrag*.

## Building Secure with SeMA
The storyboard of *Secure* (see *Secure/main_nav.xml*) is as follows:

- When the app is started, the fragment *loginFrag* is displayed to the user. The user enters username and password and clicks on the *Login* button.

- If the login is successful and the *fragAddress* variable is empty then the user navigates to *home*. *fragAddress* is an input variable to the fragment *loginFrag*.

- If the login is successful and the fragment address is *profile* then the user navigates to *profile*.

- The *profile* fragment displays sensitive information about the user. It can also be accessed via a deeplink.

- If the *profile* fragment is accessed via deeplink then it expects an input variable *profileToken*, which is sent to a server for verification.

- If the verification is successful then *profile* fragment is displayed

- If the verification is not successful, then *fragAddress* is set to *profile* and sent as a parameter to *loginFrag*.

- In *loginFrag*, the user enters credentials. If the login is successful, then app navigates to the fragment set in *fragAddress*, which is *profile*.

The analysis reports no errors because a malicious app cannot use *profileToken* to navigate to an arbitrary fragment.

## Discussion
SeMA prevents an app from using potentially malicious input to navigate to a fragment. Hence, a malicious app cannot control the app's navigation via malicious input injections.
