package sema.apps.benign.busLogic;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.String;
import java.lang.UnsupportedOperationException;

public class UserprofileFragbusLogic {
  public String busLogicUrlParamsForgetSSN() {
    throw new UnsupportedOperationException();
  }

  public String busLogicReadStreamForgetSSN(InputStream inputStream) throws IOException {
    throw new UnsupportedOperationException();
  }

  public String busLogicUrlParamsForgetName() {
    throw new UnsupportedOperationException();
  }

  public String busLogicReadStreamForgetName(InputStream inputStream) throws IOException {
    throw new UnsupportedOperationException();
  }

  public String busLogicUrlParamsForgetAddr() {
    throw new UnsupportedOperationException();
  }

  public String busLogicReadStreamForgetAddr(InputStream inputStream) throws IOException {
    throw new UnsupportedOperationException();
  }

  public void busLogicPostParamsForverifyToken(OutputStream outputStream, String profileToken)
      throws IOException {
    throw new UnsupportedOperationException();
  }

  public boolean busLogicReadStreamForverifyToken(InputStream inputStream) throws IOException {
    throw new UnsupportedOperationException();
  }
}
