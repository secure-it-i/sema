package sema.apps.benign.busLogic;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.String;
import java.lang.UnsupportedOperationException;

public class LoginFragbusLogic {
  public void busLogicPostParamsForverifyLogin(OutputStream outputStream, String email, String pwd)
      throws IOException {
    throw new UnsupportedOperationException();
  }

  public boolean busLogicReadStreamForverifyLogin(InputStream inputStream) throws IOException {
    throw new UnsupportedOperationException();
  }

  public boolean isFragEmptybusLogic(String fragAddress) {
    throw new UnsupportedOperationException();
  }

  public boolean isFragProfilebusLogic(String fragAddress) {
    throw new UnsupportedOperationException();
  }
}
