## Summary
Apps that use a Pending Intent with implicit base intent are vulnerable to Intent Hijack attacks. *Corresponding Ghera benchmark: [ImplicitPendingIntent-IntentHijack-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/ICC/ImplicitPendingIntent-IntentHijack-Lean/)*

## Description of the Vulnerability
A pending intent is used by an application to enable a third party app to perform an action on its behalf in the future. An example of this is the calendar app which sends a reminder 30 minutes before a meeting starts. In this case the calendar app will create a pending intent with the reminder and the time the reminder needs to be sent. The calendar app will send this pending intent to an app that can send notifications, which will perform the action on behalf of the calendar app, at the time specified in the pending intent. Note that the Android system will grant the app that can send notifications the same permissions as that of the calendar app while it performs the prescribed action.

*Issue*: To create a pending intent one has to specify a base intent which is the action that will be performed in the future. This base intent can be an explicit or implicit intent to an activity, service, broadcast receiver. If this base intent is an implicit intent then when the pending intent is performed, a malicious app can intercept the implicit intent. If the implicit intent has sensitive information in it then it will be leaked.

*Example*: *Benign* demonstrates this vulnerability. *Benign* sends a pending intent, with implicit base intent, to an external activity. This is a vulnerability because the implicit intent can be intercepted by a malicious app registered to receive the implicit intent.

## Building Benign with SeMA
The storyboard of *Benign* (see *main_nav.xml*) is as follows:

- When the app is started, fragment *alarmSetter* is displayed to the user.

- In the *alarmSetter* fragment, the user enters time and clicks on the *Set Alarm* button.

- Clicking the *Set Alarm* button sends an implicit pending intent to the activity *alarmManager*. This activity is not defined in *Benign*.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because a malicious app can register to receive the base implicit intent and intercept it.

## Building Secure with SeMA
The storyboard of *Secure* (see *main_nav.xml*) is as follows:

- When the app is started, fragment *alarmSetter* is displayed to the user.

- In the *alarmSetter* fragment, the user enters time and clicks on the *Set Alarm* button.

- Clicking the *Set Alarm* button sends a pending intent for the fragment *vibrate* to the activity *alarmManager*. This activity is not defined in *Benign*.

When the analysis is run, no errors are reported because the recipient of the pending intent can only trigger the fragment *vibrate*. The analysis assumes that the pending intent is intentionally shared with *alarmManager*, and this sharing is safe because it explicitly states which fragment can be triggered by the recipient of the pending intent.

## Discussion
SeMA allows developers to only create a pending intent for a fragment defined in the storyboard. Since, a pending intent for a fragment can only be used to start that fragment, it cannot be intercepted by a malicious app.
