## Summary
Apps that contain a broadcast receiver registered to receive system broadcast but does not check for validity of the broadcast message before performing corresponding operations are vulnerable to unintended invocation. *Corresponding Ghera benchmark: [NoValidityCheckOnBroadcastMsg-UnintendedInvocation-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/ICC/NoValidityCheckOnBroadcastMsg-UnintendedInvocation-Lean/)*

## Description of the vulnerability
*Context*: In Android, a broadcast receiver can register to receive messages(intents). Typically, a broadcast receiver expresses its interest in messages by specifying corresponding string identifiers in <intent-filter> tags in its manifest file. When the Android system receives a message for broadcasting, it delivers the message to every broadcast receiver who specified a string identifier (via <intent-filter> tags) that matches the action string of the message.

*Issue*:  Since the use of a <intent-filter> tag automatically exports the corresponding broadcast receiver and the message delivery is push-based, the broadcast receiver can be triggered by an appropriate message from any app, including unintended apps. The vulnerability can be avoided if the broadcast receiver is protected by permissions. However, if a broadcast receiver registers to receive intents from the system about system events, then the broadcast receiver cannot protect itself with permissions, because that would prevent the broadcast receiver from receiving the intent. While it is true that a system intent can only be sent by the system, because a system intent has its action field set to a unique string identifier that can only be used by the system, the broadcast receiver is still exported (due to the intent-filter). This means that the broadcast receiver can be triggered by an app, including unintended apps, via an explicit intent. An explicit intent does not need to have its action field set to a string identifier. It can set the action field directly to the class name and the package name within which the class resides. Thus, a malicious app can use an explicit intent to directly trigger the broadcast receiver bypassing the need to use a system-defined string identifier in its action field.

*Example*: The *Benign* app demonstrates the vulnerability. *Benign* contains a receiver *recv*, which responds to an event X. However, a malicious app can trigger event X and use *recv* to perform a privileged operation (e.g., send an email).

## Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, fragment *home_id* is displayed to the user.

- When the user clicks on button *Send Email* in *home_id*, event *my.email.event* is triggered, and an email ID and email body are sent as arguments with the event to *recv*.

- Receiver *recv* receives the event and sends the email ID and the email body to an email client, which sends the actual email.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because the event *my.email.event* can also be triggered by a malicious app, which will result in sending an email to the email ID of the malicious app's choice.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, fragment *home_id* is displayed to the user.

- When the user clicks on button *Send Email* in *home_id*, event *ACTION_BATTERY_LOW* is triggered, and an email ID and email body are sent as arguments with the event to *recv*.

- Receiver *recv* receives the event and sends the email ID and the email body to an email client, which sends the actual email.

The analysis does not report any errors in this case because the event *ACTION_BATTERY_LOW* can only be triggered by Android or by an app that has been granted permission by Android. The rationale is that an app with permission to trigger *ACTION_BATTERY_LOW* is authorized to access *recv*.

## Discussion
SeMA allows developers to define broadcast receivers. A developer needs to specify the following to define a broadcast receiver:

- access control rules,
- events that will trigger the receiver, and
- operations to be performed when the corresponding events are triggered.

SeMA prevents developers from defining a receiver with unrestricted access to respond to events not triggered by Android. Malicious apps are not allowed to trigger Android events. Hence, a receiver with unrestricted access should only respond to Android events.
