## Summary
Apps with low priority activity are vulnerable to Activity Hijack attacks. Corresponding Ghera benchmark: *[HighPriority-ActivityHijack-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/ICC/HighPriority-ActivityHijack-Lean/)*

## Description of the vulnerability
*Context*: An activity in Android can be started by passing the package name and the class name to an intent or by passing a string identifier. This string identifier is not unique. It can resolve to more than one activity. When an activity with a string identifier is started and it is the only activity with that string identifier then the activity is started. However, if the string identifier is used for more than one activity, then the user is asked to choose the activity she wants to start from a list of activities with that string identifier. Activities with higher priority are displayed first.

*Issue*: If a benign activity uses an intent-filter to specify a string identifier then a malicious activity could use the same string identifier in its intent-filter. The problem is exacerbated when the malicious activity has a higher priority than the benign activity. An activity can choose a priority by specifying a positive integer in the android:priority field in the manifest file. If an activity does not set the priority explicitly then the Android system assigns it a priority of 0. When a string identifier resolves to multiple activities, a list of activities is displayed to the user for her to choose. If a malicious activity has a higher priority, then it will appear before the benign activity making it highly likely for the user to choose the malicious activity over the benign activity.

*Example*: The *Benign* app demonstrates the vulnerability. The *shareFragId* fragment in *Benign* shares the user's SSN with an activity that responds to an event X. A malicious activity (with the highest priority possible) can register to event X and intercept the user's SSN.

## Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the *shareFragId* is displayed to the user.

- The user enters her SSN in the *ssn* field and clicks on the *Share SSN* button.

- When the user clicks on the *Share SSN* button, the value in the *ssn* field is sent to an activity that responds when an event X is triggered (see *app:action* attribute).

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because sensitive user information, *ssn*, can be intercepted by a malicious activity in the device that also responds to event X.

## Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the *shareFragId* is displayed to the user.

- The user enters her SSN in the *ssn* field and clicks on the *Share SSN* button.

- When the user clicks on the *Share SSN* button, the value in the *ssn* field is sent to an activity identified by a package name P (see *app:targetPackage* attribute).

The analysis does not report any errors because *ssn* is sent to an activity that is explicitly identified. So, a malicious app cannot masquerade as the recipient and intercept *ssn*.

## Discussion
SeMA disallows an app from sharing sensitive information with other apps without explicitly identifying the apps. This constraint is appropriate because sharing sensitive information with an unidentified app will allow a malicious app to intercept the sensitive information.
