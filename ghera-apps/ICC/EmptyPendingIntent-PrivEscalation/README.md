## Summary
Apps that use a Pending Intent with base intent empty are vulnerable to leaking privilege. *Corresponding Ghera benchmark: [EmptyPendingIntent-PrivEscalation-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/ICC/EmptyPendingIntent-PrivEscalation-Lean/)*

## Description of the Vulnerability
A pending intent is used by an application to enable a third party app to act on its behalf in the future. An example of this is the calendar app, which sends a reminder 30 minutes before a meeting starts. In this case, the calendar app will create a pending intent with the reminder and the time the reminder needs to be sent. The calendar app will send this pending intent to an app that can send notifications, which will perform the action on behalf of the calendar app, at the time specified in the pending intent. Note that the Android system will grant the app that can send notifications the same permissions as that of the calendar app while it performs the prescribed action.

*Issue*: If a malicious app receives a pending intent whose base action is empty, then the malicious app can set action and execute it in the context of the benign app that sent the pending intent. For example, if the benign app has permission to write files to some location and it mistakenly sends a pending intent whose base action is empty, then the malicious app can write files into that location without requesting permission from the user.

*Example*: *Benign* demonstrates this vulnerability. *Benign* sends an empty pending intent to an external activity. This is a vulnerability because the external activity could use the pending intent to trigger a fragment in *Benign* without the consent or knowledge of the user.

## Building Benign with SeMA
The storyboard of *Benign* (see *main_nav.xml*) is as follows:

- When the app is started, fragment *alarmSetter* is displayed to the user.

- In the *alarmSetter* fragment, the user enters time and clicks on the *Set Alarm* button.

- Clicking the *Set Alarm* button sends an empty pending intent to the activity *alarmManager*. This activity is not defined in *Benign*.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because an *empty* pending intent is sent. The recipient of an *empty* pending intent can use it to trigger a fragment in *Benign* not intended for public consumption.

## Using SeMA to build Secure
The storyboard of *Secure* (see *main_nav.xml*) is as follows:

- When the app is started, fragment *alarmSetter* is displayed to the user.

- In the *alarmSetter* fragment, the user enters time and clicks on the *Set Alarm* button.

- Clicking the *Set Alarm* button sends a pending intent for the fragment *vibrate* to the activity *alarmManager*. This activity is not defined in *Benign*.

When the analysis is run, no errors are reported because the recipient of the pending intent can only trigger the fragment *vibrate*. The analysis assumes that the pending intent is intentionally shared with *alarmManager*, and this sharing is safe because it explicitly states which fragment can be triggered by the recipient of the pending intent.

## Discussion
SeMA allows developers to only create a pending intent for fragments. Since, a pending intent for a fragment is not empty, it cannot be used for anything else other than starting a fragment.  
