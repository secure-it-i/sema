package sema.apps.benign.busLogic;

import android.content.Intent;
import java.lang.String;
import java.lang.UnsupportedOperationException;

public class HomeFragbusLogic {
  public Intent setBrdcastIntent(Intent intent, String param1, String param2) {
    intent.putExtra("emailId", param1);
    intent.putExtra("emailBody", param2);
    return intent;
  }
}
