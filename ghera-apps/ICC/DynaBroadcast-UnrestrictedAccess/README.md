## Summary
Apps that register a broadcast receiver dynamically are vulnerable to granting unrestricted access to the broadcast receiver. *Corresponding Ghera benchmark: [DynamicRegBroadcastReceiver-UnrestrictedAccess-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/ICC/DynamicRegBroadcastReceiver-UnrestrictedAccess-Lean/)*

## Description of the vulnerability

*Context*: Broadcast Receivers can register to receive messages (intents) statically in the manifest file, or they are registered dynamically in source code. Dynamically registering a broadcast receiver exports the receiver automatically.

*Issue*: Since, dynamically registering a broadcast receiver exports a broadcast receiver, it can be triggered by any app, including unintended apps, via an intent.

*Example:* The *Benign* app demonstrates the vulnerability. In *Benign*, a register *recv* is registered to respond to two events, each of which, when triggered, results in the receiver performing a privileged operation. A malicious app can trigger these events to make the receiver perform the corresponding privileged operation on its behalf.   

## Building *Benign* with SeMA
The storyboard (see *main_nav.xml*) of *Benign* is as follows:

- When the app is started, fragment *home_id* is displayed to the user. Before *home_id* is displayed the receiver *recv* is registered to respond to the events *my.email.event* and *my.sms.event*.

- In *home_id*, the user clicks on the *Send Email* button, which results in triggering the *my.email.event*.

- In the receiver *recv*, the triggering of the event *my.email.event* results in sending an email.

Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because the receiver *recv* responds to events that can be triggered by a malicious app.

## Building *Secure* with SeMA
We refine the storyboard by specifying the constraints required to trigger receiver *recv*. As per the refined storyboard, Receiver *recv* responds to the event *my.email.event*, when the event is triggered by an entity that has been granted permission by the app's user. In the storyboard (see *Secure/main_na.xml*), this aspect is specified by setting the **access** attribute of the *receiver* to **user**.

We analyze the storyboard again, and this time the analysis does not report any errors since *recv* is adequately protected against unauthorized access.

## Discussion
SeMA allows developers to define broadcast receivers that respond to broadcast messages. Developers can specify if the receiver will respond to all messages or only trusted messages. A message is trusted if the message was sent by

- the app's user, or
- the app in which the receiver is defined, or
- an app that has been granted permission to send the message by Android or the user.

SeMA disallows developers to define broadcast receivers that respond to all messages if the receiver performs privileged operations. Hence, SeMA prevents vulnerabilities caused due to unrestricted access of a broadcast receiver.
