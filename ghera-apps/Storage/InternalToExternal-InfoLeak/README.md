## Summary
Apps that allow files in internal storage to be downloaded to external storage are vulnerable to information leak. *Corresponding Ghera benchmark: [InternalToExternalStorage-InformationLeak-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Storage/InternalToExternalStorage-InformationLeak-Lean/)*

## Description of the vulnerability
*Context*: Android allows apps to save data in files. These files can be stored in Internal Storage or in External Storage. Files stored in Internal Storage are private to the app. External Storage is divided into two regions -

1. a shared storage directory, which can be accessed by all apps. However, apps will need to request the user for permission to access ExternalStorage before accessing the shared directory.
2. a private storage directory for each app. An app does not need any permissions to access its own private storage directory. However, other apps can get access to this directory if they have permission to access ExternalStorage.

*Issue*: Consider an app has a component X that accepts requests to copy a file from internal storage to external storage, which is inherently insecure. If such an app does not differentiate between trusted and untrusted sources of requests, then it can be exploited to leak information.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* app saves its users' personal information safely in internal storage. But it also allows users of the app to share personal information by writing the saved personal information to external storage. Malicious apps with access to external storage can get access to the user's personal information.

# Building Benign with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, the user clicks on the *Take Pic* button and taken to the camera app.

- In the camera pp the user takes an image and returns to *homeFrag*, where the image is displayed in the widget *pic*.

- In the *homeFrag* fragment, the user clicks on the *Save Pic* button to save the displayed image in external storage.


Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because sensitive information (SSN) is written to  file in external storage. Malicious apps can access the file to get access to the user's SSN.

# Building Secure with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, the user enters her SSN and clicks on the *Save SSN* button. The app saves SSN in a file in internal storage and navigates to *shareFrag* fragment.

- In *shareFrag* fragment, the user clicks on *Share SSN* button. The app reads SSN from the file in internal storage and sends to Android's share client which shares the SSN in a safe manner.

The analysis reports no errors because *Secure* shares the user's personal information (SSN) with Android's share client, which is trusted by *Secure*.



## Discussion
Files in external storage are accessible to all apps in the device. Therefore, apps must be careful before writing sensitive information to files in external storage. SeMA disallows apps from writing sensitive information to external storage.

The *Secure* app releases sensitive information to only entities it trusts.
