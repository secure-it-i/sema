## Summary
Apps that read files stored in External Storage are vulnerable to malicious data injection attacks. *Corresponding Ghera benchmark: [ExternalStorage-DataInjection-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Storage/ExternalStorage-DataInjection-Lean/)*

## Description of the vulnerability
*Context*: Android allows apps to save data in files. These files can be stored in Internal Storage or in External Storage. Files stored in Internal Storage are private to the app. External Storage is divided into two regions -

1. a shared storage directory, which can be accessed by all apps. However, apps will need to request the user for permission to access ExternalStorage before accessing the shared directory.
2. a private storage directory for each app. An app does not need any permissions to access its own private storage directory. However, other apps can get access to this directory if they have permission to access ExternalStorage.

*Issue*: If an app reads from any file stored in ExternalStorage, even if the file is in the private storage directory, then the app has no control over the file it is reading. If a malicious app changes the file the app is reading, the app will unknowingly read malicious content.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* app reads data from a file in the device's external storage and displays it to the user. The file in  external storage can be accessed by a malicious app to control the information displayed to the user.

# Building Benign with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, the user clicks on the *Show File* button. If *file.txt* exists in external storage of the device, then the *dispFrag* fragment is shown to the user, where the contents of *file.txt* are displayed. However, if *file.txt* does not exist, then *file.txt* is created and the user is stays in *homeFrag*.


Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis raises an error because the data from a file in external storage is displayed to the user of the app. Files in external storage can be accessed by all app in the device, including malicious apps.

# Building Secure with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, the user clicks on the *Show File* button. If *file.txt* exists in internal storage of the device, then the *dispFrag* fragment is shown to the user, where the contents of *file.txt* are displayed. However, if *file.txt* does not exist, then *file.txt* is created and the user is stays in *homeFrag*.


Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports no errors because the app reads data from a file in internal storage. Files in internal storage can only be accessed by the app.

## Discussion
Files in external storage are accessible to all apps in the device. Therefore, apps must be careful to use data from these files. SeMA prevents using data from external storage files without sanitizing it.

The *Secure* app does not use data from files in external storage. Therefore, a malicious app cannot control the *Secure* app via files in external storage.
