## Summary
Apps that write files into internal storage are vulnerable to directory traversal attack if sanitization checks for file paths are absent. *Corresponding Ghera benchmark: [InternalStorage-DirectoryTraversal-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Storage/InternalStorage-DirectoryTraversal-Lean/)*

## Description of the vulnerability
*Context*: Every Android app has its own internal filesystem. Only the app and no one else has read, write, execute permissions over files in this filesystem.

*Issue*: If an app accepts a file path as input from an external source like the user, or another app, or the web, and does not sanitize the input then it is possible to craft a malicious input to obtain access to the app's internal file-system and read, write, execute files in it.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* app has a fragment X, which accesses the app's internal storage for crash reports. It then sends the crash reports to a remote server. The fragment X expects the path where the crash report is saved and the remote server address where the report will be sent. Any app, including malicious apps, can access fragment X. Therefore, a malicious app can exploit the *Benign* app by sending a file path and a server address to access any file in the *Benign* app's internal storage and send the contents of that file to a server of its choice.

# Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *browser* is displayed to the user.

- In the *browser* fragment, the user enters a URL.

- If the URL is valid, then the *disp* fragment shows the contents of the URL to the user.

- In *disp* fragment, if an error occurs due to loading the contents of the URL, then a crash reporting is generated, saved in a file in the app's internal storage, and the user is shown the *crash* fragment.

- In the *crash* fragment the user is shown a checkbox and a *close* button. If the checkbox is checked and the user clicks on *close*, then the crash report in the app's internal storage is sent to a remote sever. If this operation is successful, then the user is shown the *browser* fragment.


Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports an error because the *crash* fragment uses the *input* variable to generate the path of a file in *Benign's* internal storage and the remote server where the file will be sent. The *input* variable can be propagated to *crash* via the *disp* fragment or a malicious app installed in the device.

# Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *browser* is displayed to the user.

- In the *browser* fragment, the user enters a URL.

- If the URL is valid, then the *disp* fragment shows the contents of the URL to the user.

- In *disp* fragment, if an error occurs due to loading the contents of the URL, then a crash reporting is generated, saved in a file in the app's internal storage, and the user is shown the *crash* fragment.

- In the *crash* fragment the user is shown a checkbox and a *close* button. If the checkbox is checked and the user clicks on *close*, then the crash report in the app's internal storage is sent to a remote sever. If this operation is successful, then the user is shown the *browser* fragment.

The analysis does not report any errors because the developer specifies that the *input* variable used to generate the file path and the remote server address is sanitized before being used.  

## Discussion
SeMA disallows apps from accepting file paths from other apps (running in the device or remotely) to its' internal storage without first performing sanitization checks on the input file paths. However, SeMA does not verify if the sanitization checks are correct. Its purpose is to warn the developer that she has missed a required sanitization check.

The *Secure* app is designed to ensure that sanitized checks are performed before using a file path received as input from another app.
