## Summary
Apps that write sensitive information to files stored in External Storage can potentially leak sensitive information. *Corresponding Ghera benchmark: [ExternalStorage-InformationLeak-Lean](https://bitbucket.org/secure-it-i/android-app-vulnerability-benchmarks/src/master/Storage/ExternalStorage-InformationLeak-Lean/)*

## Description of the vulnerability
*Context*: Android allows apps to save data in files. These files can be stored in Internal Storage or in External Storage. Files stored in Internal Storage are private to the app. External Storage is divided into two regions -

1. a shared storage directory, which can be accessed by all apps. However, apps will need to request the user for permission to access ExternalStorage before accessing the shared directory.
2. a private storage directory for each app. An app does not need any permissions to access its own private storage directory. However, other apps can get access to this directory if they have permission to access ExternalStorage.

*Issue*: If an app writes sensitive information to any file stored in ExternalStorage, even if the file is in the private storage directory, then the app has no control over who can read the file. A malicious app with read access to ExternalStorage can get access to sensitive information stored in the file.

*Example*: The *Benign* app demonstrates the vulnerability. The *Benign* app helps users take images and saves those images in external storage. A malicious app with access to external storage can retrieve the images without the user's knowledge.

# Building Benign with SeMA
The storyboard of *Benign (see Benign/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, the user clicks on the *Take Pic* button and taken to the camera app.

- In the camera pp the user takes an image and returns to *homeFrag*, where the image is displayed in the widget *pic*.

- In the *homeFrag* fragment, the user clicks on the *Save Pic* button to save the displayed image in external storage.


Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis raises an error because the image taken by the app's user is saved in external storage. A malicious app with permission to read files in external storage will be able to access the user's images.

# Building Secure with SeMA
The storyboard of *Secure (see Secure/../main_nav.xml)* is as follows:

- When the app is started, the fragment *homeFrag* is displayed to the user.

- In the *homeFrag* fragment, the user clicks on the *Take Pic* button and taken to the camera app.

- In the camera pp the user takes an image and returns to *homeFrag*, where the image is displayed in the widget *pic*.

- In the *homeFrag* fragment, the user clicks on the *Save Pic* button to save the displayed image in internal storage.


Click the Gradle plugin *lintDebug* in Android Studio or run the plugin from the command line as follows to check for security violations:

`$ ./gradlew lintDebug`

The analysis reports no errors because the app saves images in internal storage. Files stored in internal storage can only be accessed by the app.

## Discussion
Files in external storage are accessible to all apps in the device. Therefore, apps must be careful before writing sensitive information to files in external storage. SeMA disallows apps from writing sensitive information to external storage.

The *Secure* app does not write sensitive information to external storage.
