# How to design an app with SeMA

This document describes each step required to make an Android app with SeMA in Android Studio.

Making an Android app with SeMA requires the following steps. The document assumes that you have downloaded the current version of **Android Studio** and have configured **SeMA**. If you haven't, then follow the instructions to install Android Studio and configure SeMA [here](README.md).


## Create Project

Open **Android Studio**. You will be prompted with a bunch of options. Select *Start a new Android Studio project* to start a new project. Next, select the *Empty Activity* option. Enter the name of your app, a package name which will uniquely identify your app, select a location where the project will be saved, and click *finish*.

## Layout Screens
The first step is to identify the screens in your app. Each screen will be specified in the *res/layout* folder of the project. So, to specify a screen right click on the *layout* folder and select *New->Layout resource file*. Enter a file name, enter a *Root element*, and click on *ok*. This will create an *xml* file in *res/layout/<file-name>.xml*. Enter *Relative Layout* if you are not sure about the *Root element*.  

After creating a file in *layout*, add widgets to it. The following snippet shows an example of a screen specified in *layout*. This screen has 3 `EditText` widgets and a `Button`. Every widget must have a unique ID (e.g., `@+id/student`).

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent" android:layout_height="match_parent">

    <EditText
        android:id="@+id/student"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:ems="10"
        android:hint="Student Name"
        android:inputType="text" />

    <EditText
        android:id="@+id/grade"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:ems="10"
        android:layout_below="@id/student"
        android:hint="Grade"
        android:inputType="text" />

    <Button
        android:id="@+id/submit"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_below="@id/grade"
        android:text="Submit" />
</RelativeLayout>
```

## Iteratively build a Storyboard

The storyboard defines the app's behavior (e.g., which screen is triggered when the user clicks on a button) via a navigation graph.

The first step is to create a navigation graph. right click on the *res* folder and select *New->Android Resource File*. Next, select *Navigation* as *Resource type* and click *ok*. This creates an *xml* file, which is the navigation graph. We will use this navigation graph to design our app's behavior as follows:

### List all the screens specified in layout

The snippet below shows how to list a screen in a navigation graph. As shown in the snippet, a screen listing must have a unique ID and the name of the layout file.
```xml
<fragment
        android:id="@+id/form"
        tools:layout="@layout/form_fragment"/>
```  

### Choose one of the screens listed out to be the start screen.

The start screen indicates the screen that will be shown to the user, when the app is launched from apps menu. The `startDestination` attribute shown below is used for this purpose.

```xml
<navigation ...
    app:startDestination="@id/form">

    <fragment ...> ... </fragment>
    ...
</navigation>
```

### Declare and initialize widgets in a screen

The widgets of a screen outlined in *layout* can be initialized with values. These values can be one of the following:

  - *user-provided* as shown below

```xml
<fragment android:id="@+id/form"
      tools:layout="@layout/form_fragment">

      <widget wid="@id/student"/>
      ...
</fragment>
```

  - a *variable* as shown below

```xml
<fragment android:id="@+id/form"
    tools:layout="@layout/form_fragment">

    <widget wid="@id/student" value="var/x"/>
    ...
</fragment>
```

  - a *constant* as shown below

```xml
<fragment android:id="@+id/form"
      tools:layout="@layout/form_fragment">

      <widget wid="@id/student" value="Jane Doe"/>
      ...
</fragment>
```

  - a value from a *operation* as shown below

```xml
<fragment android:id="@+id/form"
      tools:layout="@layout/form_fragment">

      <widget wid="@id/student" value="fun/showName">
        <fun android:name="showName"/>
      </widget>
      ...
</fragment>
```

A variable used to initialize a widget must be in the *scope* of the screen containing the widget. All widgets in a screen belong to the scope of that screen. Further, variables provided as arguments on incoming transitions into a screen are part of the scope of that screen. We will talk more about arguments later.

### Specify operations

Operations indicate a task to be performed (e.g., read from a file, get contents from a web server, etc.). An operation must have a name, may have input parameters, and returns a boolean or non-boolean value. The input parameter of an operation can be a *constant*, *widget ID*, *variable*, or another operation. The following snippet shows how an operation is used to initialize a widget. This operation has a name `getSSN`, two input parameters from variables `name` and `address`, and returns a value that will assigned to the widget `ssn`.

```xml
<fragment android:id="@+id/form"
      tools:layout="@layout/form_fragment">

      <widget wid="@id/ssn" value="fun/getSSN">
        <fun android:name="getSSN">
          <parm arg="var/name"/>
          <parm arg="var/address"/>
        </fun>
      </widget>
      ...
</fragment>
```

An operation's parameter can be one of the following:

- a *constant*

```xml
<fun android:name="getSSN">
  <parm arg="5"/>
  ...
</fun>
```

- a *variable*

```xml
<fun android:name="getSSN">
  <parm arg="var/x"/>
  ...
</fun>
```

- a *widget*

```xml
<fun android:name="getSSN">
  <parm arg="@id/x"/>
  ...
</fun>
```

- another *operation*

```xml
<fun android:name="getSSN">
  <parm arg="fun/getName">
    <fun android:name="getName"/>
  </parm>
  ...
</fun>
```

### Specify an operation's resource

Android provides app developers with pre-defined tasks (e.g., perform a network operation). In storyboards, we call these pre-defined tasks *resources*. An operation can use a *resource* to perform a certain task. For example, the snippet below shows that the operation `getSSN` uses the method `get` of the resource `INTERNET`. The `get` method of resource `INTERNET` indicates the task of getting data from a remote server.

```xml
<fragment android:id="@+id/form"
      tools:layout="@layout/form_fragment">

      <widget wid="@id/ssn" value="fun/getSSN">
        <fun android:name="getSSN" resource="INTERNET" method="get">
          <parm arg="http"/>
          <parm arg="test.server.com"/>
          <parm arg="var/name"/>
          <parm arg="var/address"/>
        </fun>
      </widget>
      ...
</fragment>
```

SeMA supports a pre-defined list of such operations. Following are the rules to use these pre-defined resources.

  - **INTERNET resource**. The INTERNET resource is used to connect to a remote server. It offers two methods -- `get` and `post`. The `get` method is used to retrieve data from a server and `post` is used to send data to a remote server. An operation using this resource must provide the protocol and the host name of the server as the first two input parameters.

```xml
<fun android:name="getSSN" resource="INTERNET" method="get">
  <parm arg="http"/>
  <parm arg="test.server.com"/>
  ...
</fun>
```

  - **SOCKET resource**. The SOCKET resource is used to open a socket in the device to communicate with another (local or remote) process. It offers two methods -- `get` and `post`. The `get` method is used to retrieve data from the socket and `post` is used to send data to the socket. An operation using this resource must provide the host and port of the server as the first two input parameters.

```xml
<fun android:name="getSSN" resource="SOCKET" method="get">
  <parm arg="localhost"/>
  <parm arg="8080"/>
  ...
</fun>
```

  - **SSLSOCKET resource**. The SSLSOCKET resource is similar to the SOCKET resource only it uses the SSL protocol to connect to a socket.

```xml
<fun android:name="sendSSN" resource="SSLSOCKET" method="post">
  <parm arg="localhost"/>
  <parm arg="8080"/>
  ...
</fun>
```

  - **EXT_STORE resource**. The EXT_STORE resource is used to open a file in the device's external storage (e.g., SDCARD). It offers two methods -- `get` and `post`. The `get` method is used to read data from the file and `post` is used to write data to a file. An operation using this resource must provide a file name as the first input parameter and an optional directory name as the second parameter.

```xml
<fun android:name="getSSN" resource="EXT_STORE" method="get">
  <parm arg="myFile.txt"/>
  <parm arg="info"/>
  ...
</fun>
```

  - **INT_STORE resource**. The INT_STORE resource is used to open a file in the app's storage. Android provides storage space to every app. It offers two methods -- `get` and `post`. The `get` method is used to read data from the file and `post` is used to write data to a file. An operation using this resource must provide a file name as the first input parameter and an optional directory name as the second parameter.

```xml
<fun android:name="getSSN" resource="INT_STORE" method="get">
  <parm arg="myFile.txt"/>
  <parm arg="info"/>
  ...
</fun>
```

  - **CIPHER**. The CIPHER resource is used to encrypt/decrypt data. It offers two methods -- `encrypt` and `decrypt`. An operation using CIPHER must provide a key as the first input parameter.

```xml
<fun android:name="encryptSSN" resource="CIPHER" method="encrypt">
  <parm arg="myKey"/>
  ...
</fun>
```

  - **KEYSTORE**. The KEYSTORE resource is used to safely store a cipher key. It offers two methods -- `get` and `post`. The `get` method retrieves a key from the keystore based on an existing alias. The `post` method saves a randomly generated key for a given alias in the keystore.

```xml
<fun android:name="getKey" resource="KEYSTORE" method="get">
  <parm arg="myAlias"/>
  ...
</fun>
```

```xml
<fun android:name="getKey" resource="KEYSTORE" method="post">
  <parm arg="myAlias"/>
  ...
</fun>
```

  - **KEYSTORE PBE**. The `post` method in KEYSTORE can be used to save a password-based key for a given alias.  

```xml
<fun android:name="getKey" resource="KEYSTORE" method="post" pbe="true">
  <parm arg="@id/pass"/>
  <parm arg="myAlias"/>
  ...
</fun>
```

  - **CLIPBOARD_SERVICE**. The CLIPBOARD_SERVICE is used to copy and paste data. It offers two methods - `get` and `post`. The `get` method copies data pasted int he clipboard and the `post` method paste data in the clipboard.

```xml
<fun android:name="copy" resource="CLIPBOARD_SERVICE" method="get"/>
```

```xml
<fun android:name="paste" resource="CLIPBOARD_SERVICE" method="post">
  <parm arg="@id/username"/>
</fun>
```

### Specify the possible transitions between screens

A transition from a source screen S to a destination screen D indicates that a user can move from S to D. The following snippet shows how a transition can be specified in a navigation graph. As shown in the snippet, a transition is specified with an `action` tag. The `action` tag must have a  unique `id` and `destination` attributes. The id in the `destination` attribute must refer to a screen listed in the navigation graph. Further, an `action` tag must have a parent `fragment` tag to indicate the source screen.  

```xml
<fragment ...>
  <action android:id="@+id/action1"
      app:destination="@id/form">
          ...
  </action>
</fragment>
```

### Pass arguments to destination screen

Variables can be passed in transitions as arguments to destination screens. An argument must have a *name* and a *value*. The argument *name* becomes part of the scope of the destination screen when the transition is taken and can be used in the destination screen or on a transition from that screen. The following snippets show how an argument can be passed to a destination screen:

 - pass a variable as argument

```xml
<fragment ...>
 <action android:id="@+id/action1"
     app:destination="@id/form">
         <argument android:name="x" value="var/y"/>
         ...
 </action>
</fragment>
```

  - pass a widget *w* as argument. Notice that the widget must be in the scope of the source screen.

```xml
<fragment ...>
 <widget wid="w"/>
 <action android:id="@+id/action1"
     app:destination="@id/form">
         <argument android:name="x" value="@id/w"/>
         ...
 </action>
</fragment>  
```

 - pass a constant as argument.

```xml
<fragment ...>
 <action android:id="@+id/action1"
     app:destination="@id/form">
         <argument android:name="x" value="2"/>
         ...
 </action>
</fragment>  
```

 - pass value from a operation as argument.

```xml
<fragment ...>
 <action android:id="@+id/action1"
     app:destination="@id/form">
         <argument android:name="x" value="fun/getX">
           <fun android:name="getX"/>
         </argument>
         ...
 </action>
</fragment>  
```

### Add user-controlled constraints to transitions

A transition from a source screen to a destination screen can be guarded with user-controlled constraints. A user-controlled constraint indicates that the corresponding transition is taken if a user performed a gesture on a widget in the source screen. For example, in the snippet shown below the transition with a unique ID `action1` is taken if a user performs the `click` gesture on a widget in the source screen `form` with ID `submit`. Currently, only the `click` gesture is allowed in SeMA.

```xml
<action
        android:id="@+id/action1"
        app:destination="@id/form"
        widgetOn="@id/submit"
        gesture="click">
        ...
</action>
```

### Add boolean constraints to transitions

A transition from a source screen to a destination screen can be guarded with operations that return a boolean value. These constraints do not depend on the user. The following snippet shows how to specify a boolean operation as a constraint. In this snippet, the constraint is an operation `save` that returns true if the data in the `password` widget is saved in the file `myFile.txt`; false otherwise.

```xml
<action
        android:id="@+id/action1"
        app:destination="@id/form"
        widgetOn="@id/submit"
        gesture="click">
        <constraint>
          <fun android:name="save" resource="EXT_STORE" method="post">
            <parm arg="myFile.txt"/>
            <parm arg="@id/password"/>
          </fun>
        </constraint>
</action>
```

A boolean constraint can be a *negation*. A negation constraint is specified with the `<not>` tag as follows:

```xml
<constraint>
  <not>
    <fun android:name="save" resource="EXT_STORE" method="post">
      <parm arg="myFile.txt"/>
      <parm arg="@id/password"/>
    </fun>
  </not>
</constraint>
```

A boolean constraint can be a *conjunction* of two operations. A conjunction constraint is specified with the `<and>` tag as follows:

```xml
<constraint>
  <and>
    <fun android:name="op1" >
      <parm arg="@id/password"/>
    </fun>
    <fun android:name="op2">
      <parm arg="var/x"/>
    </fun>
  </and>
</constraint>
```

A boolean constraint can be a *disjunction* of two operations. A disjunction constraint is specified with the `<or>` tag as follows:

```xml
<constraint>
  <or>
    <fun android:name="op1" >
      <parm arg="@id/password"/>
    </fun>
    <fun android:name="op2">
      <parm arg="var/x"/>
    </fun>
  </or>
</constraint>
```

### Navigate to external apps

Android apps communicate with each other. For example, an app can help its user call a phone number by using the phone app installed in the device. An external app is specified in the storyboard with the `activity` tag and a mandatory attribute `app:action` and an optional attribute `app:targetPackage`. Following is an example:

```xml
<activity android:id="@+id/client" app:action="app.client.activity" app:targetPackage="app.client"/>
```

The value in `app:action` helps identify all the apps with that identifier. The `app:targetPackage` attribute uniquely identifies an app. Not specifying the `targetPackage` triggers all apps with the same `app:action`. However, specifying both the attributes uniquely identifies exactly 1 app. When multiple apps are triggered, the user will be asked to choose the app she wants her app to communicate with.

Navigation to an external app specified in the storyboard is specified via the `<action>` tag. For example, the following snippet shows a transition/navigation from a `fragment` defined in the storyboard to an external app. The transitions is triggered when the `button` in the `fragment` is clicked and when the transition is triggered the argument `arg1` with value from the widget `ssn` is passed to the external app `client`.

```xml
<fragment
    android:id="@+id/shareFragId"
    tools:layout="@layout/share_frag">
    <widget wid="@id/ssn" />
    <action
        android:id="@+id/shareAction"
        app:destination="@id/client"
        widgetOn="@id/button"
        gesture="click">
        <argument android:name="arg1" value="@id/ssn"/>
    </action>
</fragment>    
```

### Deep Links

A screen may have one or more *deep links*. A deep link associated with a screen is a URI that can be used by external apps to communicate with the screen. The following snippet shows how to associate deep links with a screen:

```xml
<fragment android:id="@+id/someFragId"
  tools:layout="@layout/some_frag">
  <deepLink app:uri="app//example.myapp.edu/{x}"/>
  <action
      android:id="@+id/shareAction"
      app:destination="@id/client"
      widgetOn="@id/button"
      gesture="click">
      <argument android:name="arg1" value="var/x"/>
  </action>
</fragment>
```  

Every deep link must have a `uri` attribute. The value of `uri` attribute must be of the form `scheme//host/{variableName}`. As shown in the snippet, the `fragment` with ID `someFragId` has a *deep link* with a `uri` attribute, where `scheme := app`, `host := example.myapp.edu`, and `variableName := x`. The `variableName` is optional. So, `<deepLink app:uri="app//example.myapp.edu/"/>` is an example of a deep link with no `variableName`. A variable is required if the screen expects input from an external app. If a variable is provided in a deep link, then it can be used in the fragment with a `var/` prefix as shown in the snippet -- `<argument android:name="arg1" value="var/x"/>`.

### Display Web Content

Web content is displayed in an app via the `WebView` widget. The value in a `WebView` widget must be an operation that uses the the `loadUrl` method of the INTERNET resource. The following snippet shows an example:

```xml
<fragment ...>
  ...
  <widget wid="@id/wv" value="fun/getWebContent">
    <fun android:name="getWebContent" resource="INTERNET" method="loadUrl">
      <parm arg="https://google.com"/>
    </fun>
  </widget>
  ...
</fragment>
```

Web contents may have JavaScript embedded in them. JavaScript execution is disabled in a `WebView` by default. To enable it set the the `allowJsExec` attribute to `true` as follows:

```xml
<fragment ...>
  ...
  <widget wid="@id/wv" value="fun/getWebContent">
    <fun android:name="getWebContent" resource="INTERNET" method="loadUrl" allowJsExec="true">
      <parm arg="https://google.com"/>
    </fun>
  </widget>
  ...
</fragment>
```

A `WebView` widget *may* specify a whitelist. A whitelist helps a `WebView` limit the URLs from which web content will be displayed in it. A whitelist consists of one more `patterns`. Each `pattern` is a regular expression that specifies the URL pattern that will be accepted by the `WebView`. The following snippet shows how a whitelist is specified for a WebView:

```xml
<fragment ...>
  <widget wid="@id/url"/>
  <widget wid="@id/wv" value="fun/getWebContent">
    <fun android:name="getWebContent" resource="INTERNET" method="loadUrl" allowJsExec="true">
      <parm arg="@id/url"/>
      <whitelist>
        <pattern case="*sema.org/*"/>
        <pattern case="*google.com/*"/>
      </whitelist>
    </fun>
  </widget>
  ...
</fragment>
```

### Add Broadcast Receiver

In Android, a broadcast receiver responds to events triggered by the underlying system or by other apps installed in the system. An email notification is an example of an event triggered by an email app. A broadcast receiver can register to respond to events and perform operations when the registered event is triggered.

A receiver must be added to the *res/xml/custom_res.xml* file in the project. A receiver must have a unique ID, an optional *access* attribute, an optional *permission* attribute, and an optional *permission-name* attribute. A receiver can have one or more *event* tags. An *event* tag has a unique ID and *name* and exactly one operation. The snippet below shows how a receiver is defined. The *access* attribute can one of three constants - "all", "own", and "user".

```xml
<event-receiver
  android:id="@+id/recv"
  access="user"
  permission="custom"
  permission-name="sema.recv.event1">
  <!--receiver is protected by a custom dangerous permission-->

  <event android:id="@+id/event1" android:name="my.email.notification">
      <fun android:name="sendEmail">
          <parm arg="input/emailId"/>
          <parm arg="input/emailBody"/>
      </fun>
  </event>

  <event android:id="@+id/event2" android:name="my.sms.notification">
      <fun android:name="sendSMS">
          <parm arg="input/phnNo"/>
          <parm arg="input/body"/>
      </fun>
  </event>
</event-receiver>
```

## Verify Storyboard

Click *lintDebug* under *app/verification* in the *Gradle* window in Android Studio to check the correctness of the storyboard. Fix any errors that the verifier reports. It is advisable to verify the storyboard every time it is refined significantly.

## Generate Code

Click *genCodeFromNav* under *app/other* in the *Gradle* window in Android Studio to generate an implementation for the specified storyboard. The generated code is placed in the *generatedJava* folder of the project. It is advisable not to modify this code. The files placed in *java/../busLogic* can be modified to add business logic. The functions in these files correspond to operations that do not use a pre-defined operation.

## Examples

The *ghera-apps* folder contains 60 sample apps. Each app captures a feature explained in this document.
